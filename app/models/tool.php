<?php
class Tool extends AppModel {
	var $name = 'Tool';
	
	
	
	
	var $validate = array(
		'title' => array(           
			'empty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Enter Tools Title',
			)
		)
		
	);
	
	
	var $belongsTo = array('ToolCategory');
}
?>