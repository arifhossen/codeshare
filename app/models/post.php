<?php
class Post extends AppModel {
	var $name = 'Post';
	
	
	var $belongsTo = array('Category');
	var $hasMany = array('PostComment','PostTag');
	
	
	var $validate = array(
		'title' => array(           
			'empty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Enter Post Title',
			)
		),
		'details' => array(
			'empty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Enter Post Description',
			)
		)
	);
}
?>