<?php
class PostComment extends AppModel {
	var $name = 'PostComment';
	
	var $validate = array(
		'email' => array(           
			'empty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Enter Email Address',
			)
		),
		'comments' => array(
			'empty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Enter Post Comments',
			)
		)
	);	

}
?>