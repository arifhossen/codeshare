<?php
class Content extends AppModel {
	var $name = 'Content';
	
	
	
	var $validate = array(
		'page_name' => array(           
			'empty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Enter Page Title',
			)
		),
		'page_title' => array(
			'empty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Eneter Page Title',
			)
		)
	);
	
}
?>