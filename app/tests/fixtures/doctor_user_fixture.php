<?php
/* DoctorUser Fixture generated on: 2011-09-28 08:14:58 : 1317197698 */
class DoctorUserFixture extends CakeTestFixture {
	var $name = 'DoctorUser';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'doctor_specialty_id' => array('type' => 'integer', 'null' => false),
		'diagnostic_institute_id' => array('type' => 'integer', 'null' => false),
		'medical_institute_id' => array('type' => 'integer', 'null' => false),
		'country_id' => array('type' => 'integer', 'null' => false),
		'username' => array('type' => 'string', 'null' => false, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'password' => array('type' => 'string', 'null' => false, 'length' => 40, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'clear_password' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'first_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'last_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'email' => array('type' => 'string', 'null' => false, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'status' => array('type' => 'string', 'null' => false, 'default' => 'Active', 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'delflag' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'last_login' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'last_access' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'MyISAM')
	);

	var $records = array(
		array(
			'id' => 1,
			'doctor_specialty_id' => 1,
			'diagnostic_institute_id' => 1,
			'medical_institute_id' => 1,
			'country_id' => 1,
			'username' => 'Lorem ipsum dolor ',
			'password' => 'Lorem ipsum dolor sit amet',
			'clear_password' => 'Lorem ipsum dolor ',
			'first_name' => 'Lorem ipsum dolor ',
			'last_name' => 'Lorem ipsum dolor ',
			'email' => 'Lorem ipsum dolor sit amet',
			'status' => 'Lorem ipsum dolor sit amet',
			'delflag' => 1,
			'last_login' => '2011-09-28 08:14:58',
			'last_access' => '2011-09-28 08:14:58',
			'created' => '2011-09-28 08:14:58',
			'modified' => '2011-09-28 08:14:58'
		),
	);
}
