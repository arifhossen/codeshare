<?php
/* DoctorUser Test cases generated on: 2011-09-28 08:14:58 : 1317197698*/
App::import('Model', 'DoctorUser');

class DoctorUserTestCase extends CakeTestCase {
	var $fixtures = array('app.doctor_user', 'app.doctor_specialty', 'app.user', 'app.role', 'app.user_role', 'app.medical_institute', 'app.medical_institute_service', 'app.diagnostic_institute', 'app.diagnostic_institute_service', 'app.country');

	function startTest() {
		$this->DoctorUser =& ClassRegistry::init('DoctorUser');
	}

	function endTest() {
		unset($this->DoctorUser);
		ClassRegistry::flush();
	}

}
