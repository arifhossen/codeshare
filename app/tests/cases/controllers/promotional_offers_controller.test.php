<?php
/* PromotionalOffers Test cases generated on: 2011-12-01 12:01:15 : 1322740875*/
App::import('Controller', 'PromotionalOffers');

class TestPromotionalOffersController extends PromotionalOffersController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PromotionalOffersControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.promotional_offer', 'app.role', 'app.user', 'app.user_role', 'app.country', 'app.generalsetting', 'app.category', 'app.product', 'app.all_file', 'app.productpreview', 'app.product_category', 'app.related_tag', 'app.product_tag', 'app.product_attribute', 'app.product_type_attribute', 'app.product_type', 'app.product_attribute_value', 'app.currency_value');

	function startTest() {
		$this->PromotionalOffers =& new TestPromotionalOffersController();
		$this->PromotionalOffers->constructClasses();
	}

	function endTest() {
		unset($this->PromotionalOffers);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

}
