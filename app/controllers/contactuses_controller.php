<?php
class ContactusesController extends AppController {

	var $name = 'Contactuses';
	var $uses = array('Contactus');
	
	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('add');
		$this->Auth->autoRedirect = false;
		
	}

	function index() {
		$this->layout = 'default';		
		
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid contactus', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('contactus', $this->Contactus->read(null, $id));
	}

	function add() {
		$this->layout = 'default';


		
		if (!empty($this->data)) {
			$this->Contactus->create();
			if ($this->Contactus->save($this->data)) {
			
						$message_details = "Dear admin you have a cutomer enquery from Deshi Chain.Please follow on...\r\n\n";
						$message_details .= $this->data['Contactus']['name']."\r\n";
						$message_details .= $this->data['Contactus']['details']."\r\n";
						
						$fromemailaddress = $this->data['Contactus']['emailaddress'];
						
						//admin email address set here
						$to = 	'info@smartactic.com';
						
						$subject = 	"Smartactic Customer Comments";
						$headers = 	"From: $fromemailaddress\r\n";
						$headers .= "MIME-Version: 1.0\r\n"
							. "Content-Type: text/html; charset=\"iso-8859-1\"\r\n"
							. "Content-Transfer-Encoding: 7bit\r\n";
					
						if(mail($to, $subject, $message_details, $headers))
						{
							$this->Session->setFlash(__('Your enquery has been received by us and a confirmnation email send to your mail please check.', true));
						} else {
							$this->Session->setFlash(__('Feedback send problem', true));
						} 
			
			
			
				$this->Session->setFlash(__('Your Feedback has been sent to smartactic ', true));
				$this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('Your Feedback not sent , Please try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid contactus', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Contactus->save($this->data)) {
				$this->Session->setFlash(__('The contactus has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contactus could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Contactus->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for contactus', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Contactus->delete($id)) {
			$this->Session->setFlash(__('Contactus deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Contactus was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->layout = 'admin_inner';
		$this->Contactus->recursive = 0;
		$this->set('contactuses', $this->paginate());
	}

	function admin_view($id = null) {
		$this->layout = 'admin_inner';
		if (!$id) {
			$this->Session->setFlash(__('Invalid contactus', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('contactus', $this->Contactus->read(null, $id));
	}

	function admin_add() {
		$this->layout = 'admin_inner';
		if (!empty($this->data)) {
			$this->Contactus->create();
			if ($this->Contactus->save($this->data)) {
				$this->Session->setFlash(__('The contactus has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contactus could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		$this->layout = 'admin_inner';
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid contactus', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Contactus->save($this->data)) {
				$this->Session->setFlash(__('The contactus has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contactus could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Contactus->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		$this->layout = 'admin_inner';
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for contactus', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Contactus->delete($id)) {
			$this->Session->setFlash(__('Contactus deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Contactus was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>