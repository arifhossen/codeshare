<?php
class SettingsController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    var $name = 'Settings';
    // var $components = array();
    /**
     * Default helper
     *
     * @var array
     * @access public
     */  
	var $helpers = array('Html', 'Form', 'Time');
    /**
     * This controller does not use a model
     *
     * @var array
     * @access public
     */
    var $uses = array('User','Sitemodule','Template');

    /**
     * Displays a view
     *
     * @param mixed What page to display
     * @access public
     */

    function beforeFilter() {
        parent::beforeFilter();
		$this->Auth->allow('admin_index');
		
		$this->layout = 'admin_dashboard';

    }
	
	
	function admin_manage_template()
	{
	

		
	    $templates = $this->Template->find('all');	
		
		$this->set('templates',$templates);
		
		
		if(!empty($this->data))
		{
		    $where = array('Template.status'=>'0');
			$this->Template->updateAll($where);
			
			
			$this->data['Template']['id'] = $this->data['Template']['id'];
			$this->data['Template']['status'] = 1;
			$this->Template->save($this->data);

            $this->redirect(array('action'=>'manage_template'));			
		
		}
		
	}
	
	function getCurrentTemplateName()
	{
	
	 $where = array('Template.status'=>1);
	 $templatesInfo = $this->Template->find($where);
	 $name = strtolower($templatesInfo['Template']['name']);	 
	 return $name;
	
	
	}
	
	function admin_payment_gateway_integration()
	{
	
	
	
	}
	

}

?>