<?php
class TagsController extends AppController {

    var $name = 'Tags';  
	var $uses = array('Tag');
   var $components = array('FileUpload','RequestHandler');

  function beforeFilter()
	{
		parent::beforeFilter();		
		
		$this->Auth->allow('*');
		if(!empty($this->params['admin']))
			$this->layout = "inner_common_layout";
	}
  

	
    function admin_index() {     
       
	    $header_title = 'Manage Tag';
	    $module_title = 'Tag';
	    $this->set(compact('header_title','module_title'));	
	   
	    $this->Tag->recursive = 0;
        $this->paginate = array('limit'=>'10','order'=>'Tag.id DESC'); 
        $this->set('tags', $this->paginate());
        
	}	



    function admin_add() {
       
		$header_title = 'Add New Tag';
		$module_title = 'Tags';
		$tags = $this->Tag->find('list');	
		
		$this->set(compact('header_title','module_title','tags'));	
		
	   
        if (!empty($this->data)) {
            $this->Tag->create();
           
            if ($this->Tag->save($this->data)) {
                $this->Session->setFlash(__('The Tag has been saved', true), 'message/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Tag could not be saved. Please, try again.', true));
            }
        }
       
    }

    function admin_edit($id = null) {
      
	  
	  	$header_title = 'Edit Tag';
		$module_title = 'Tags';
		
		
		$this->set(compact('header_title','module_title','tags'));	
	  
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid tag', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
           
           
            if ($this->Tag->save($this->data)) {
                $this->Session->setFlash(__('The tag has been saved', true), 'message/success');
                $this->redirect('index');
            } else {
                $this->Session->setFlash(__('The tag could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Tag->read(null, $id);
        }
     
    }

    function admin_delete($id = null) {
        $this->layout='default_admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for content', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Tag->delete($id)) {
            $this->Session->setFlash(__('Tag deleted', true), 'message/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Tag was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }
	
	
	function getTagList()
	{	
		return $this->Tag->find('all');
	
	}

    
	
	

}

?>