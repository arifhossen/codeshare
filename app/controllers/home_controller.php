<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of home_controller
 *
 * @author Md.Moniruzzaman
 */
class HomeController extends AppController {
    //put your code here

    var $uses = array('SupportedPlatform');
	
	var $helpers = array('Ajax');
	var $components = array('FileUpload','RequestHandler','Email');
	
	function beforeFilter(){	  
	  	parent::beforeFilter();
		$this->Auth->allow('index');
	}
	
	function  __construct() {
        parent::__construct();
        $this->layout = 'default';
    }

    function index(){	
	
	    $where = array('SupportedPlatform.identifier'=>'iPhone');
	    $supportedPlatformsInfo = $this->SupportedPlatform->find($where);	
				
		$this->set('supportedPlatformsInfo',$supportedPlatformsInfo);
		
		
		
	}
	
	
	
}
?>
