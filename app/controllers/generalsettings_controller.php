<?php
class GeneralsettingsController extends AppController {

	var $name = 'Generalsettings';
	var $uses = array('GeneralSetting','Template');
	
	var $components = array('FileUpload');
	
	function index() {
		$this->Generalsetting->recursive = 0;
		$this->set('generalsettings', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid generalsetting', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('generalsetting', $this->Generalsetting->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Generalsetting->create();
			if ($this->Generalsetting->save($this->data)) {
				$this->Session->setFlash(__('The generalsetting has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The generalsetting could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid generalsetting', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Generalsetting->save($this->data)) {
				$this->Session->setFlash(__('The generalsetting has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The generalsetting could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Generalsetting->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for generalsetting', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Generalsetting->delete($id)) {
			$this->Session->setFlash(__('Generalsetting deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Generalsetting was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->layout = 'admin_inner';
		$this->Generalsetting->recursive = 0;
		$this->set('generalsettings', $this->paginate());
	}

	function admin_view($id = null) {
		$this->layout = 'admin_inner';
		if (!$id) {
			$this->Session->setFlash(__('Invalid generalsetting', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('generalsetting', $this->Generalsetting->read(null, $id));
	}

	function admin_add() {
		$this->layout = 'admin_inner';
		if (!empty($this->data)) {
			$this->Generalsetting->create();
			if ($this->Generalsetting->save($this->data)) {
				$this->Session->setFlash(__('The generalsetting has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The generalsetting could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		$this->layout = 'admin_inner';
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid generalsetting', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			
			/*========= Upload File Start=============*/
				$filearray_category_image = $this->data['Generalsetting']['logo'];	
						
				//Check here the category image array is empty or not
				if($this->data['Generalsetting']['logo']['name'] != null){				
				$target_path = WWW_ROOT."files".DS."category_image";
				$target_path_category ="files"."/"."category_image/";
				$numOfFile = 1;						
						$filename = $this->data['Generalsetting']['logo']['name'];
						$tmpname  = $this->data['Generalsetting']['logo']['tmp_name'];
						$filesize = $this->data['Generalsetting']['logo']['size'];
						$filetype = $this->data['Generalsetting']['logo']['type'];							
									
					if(strlen($filename) <> 0){
						$newFileName = $this->FileUpload->upload($filename, $tmpname, $filesize, $target_path, 'jpg,png,gif,doc,docx,pdf,txt,html,htm');						
						$this->data['Generalsetting']['logo'] = $target_path_category.$newFileName;						
					}
					else{
						$this->Session->setFlash("The file type is not valid please enter the valid file type",true);
						$this->redirect(array('action'=>'add'));
					}					
					
				}//End
				else{
					$this->data['Generalsetting']['logo'] = '';
				}
			
			
			if ($this->Generalsetting->save($this->data)) {
				$this->Session->setFlash(__('The generalsetting has been updated', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The generalsetting could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Generalsetting->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		$this->layout = 'admin_inner';
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for generalsetting', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Generalsetting->delete($id)) {
			$this->Session->setFlash(__('Generalsetting deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Generalsetting was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	
	
	function manage_admin_templates()
	{
	
	
	
	
	}
	
	
	
}
?>