<?php
class PagesController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    var $name = 'Pages';
    //   var $components = array('Authake');
    /**
     * Default helper
     *
     * @var array
     * @access public
     */
    var $helpers = array();
    /**
     * This controller does not use a model
     *
     * @var array
     * @access public
     */
    var $uses = array('Content');

    /**
     * Displays a view
     *
     * @param mixed What page to display
     * @access public
     */
    function index() {
        $this->layout = 'default';
        $this->pageTitle = '';
        // $slug =   explode('', $this->params['slug']);
        if (isset($this->params['slug'])) {
            $page_name = $this->params['slug'];
            $content = $this->Content->find('first', array('conditions' => "Content.page_name='" . $page_name . "' and Content.published=1"));
        } else {
            $content = '';
        }

        $this->set('content', $content);

    }
	
}

?>