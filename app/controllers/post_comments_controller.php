<?php
class PostCommentsController extends AppController {

    var $name = 'PostComments';  
	var $uses = array('PostComment');
    var $components = array('FileUpload','RequestHandler');

  function beforeFilter()
	{
		parent::beforeFilter();		
		$this->Auth->allow('*');
		if(!empty($this->params['admin']))
			$this->layout = "inner_common_layout";
	}


    
	
    function admin_index() {     
       
	    $header_title = 'Manage Post Comment';
	    $module_title = 'Post Comments';
	    $this->set(compact('header_title','module_title'));	
	   
	    $this->PostComment->recursive = 0;
        $this->paginate = array('limit'=>'10'); 
        $this->set('post_comments', $this->paginate());
        
	}	

    function admin_view($id = null) {
       $this->layout='default_admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid content', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('content', $this->PostComment->read(null, $id));
    }
	
	
	
	 function add() {
       
		   
        if (!empty($this->data)) {
        
        /*
            $this->PostComment->create();           
		    $post_id = $this->data['PostComment']['post_id'];
			$post_title = $this->data['Post']['title'];
				
            if ($this->PostComment->save($this->data)) {
			    
				$comment_id = $this->PostComment->id;
				
				
				
				//******************** Mail Send To Blog Admin *****************	
				  //$this->send_mail($comment_id);
				//******************** End Mail Send ***************************				
				
				
                $this->Session->setFlash(__('Your Comments  has been post', true), 'message/success');
                $this->redirect(array('controller'=>'posts','action' => 'details',$post_title,'#post_comments_form'));
            } else {
                $this->Session->setFlash(__('Your Comments could not be post. Please, try again.', true));
				$this->redirect(array('controller'=>'posts','action' => 'details',$post_title));
            }
            
            */
        }
       
    }

    function admin_add() {
       
		$header_title = 'Add New Post Comment';
		$module_title = 'PostComments';
		$categories = $this->Category->find('list');	
		
		$this->set(compact('header_title','module_title','categories'));	
		
	   
        if (!empty($this->data)) {
            $this->PostComment->create();
           
            if ($this->PostComment->save($this->data)) {
                $this->Session->setFlash(__('The Post Comment has been saved', true), 'message/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Post Comment could not be saved. Please, try again.', true));
            }
        }
       
    }

    function admin_edit($id = null) {
      
	  
	  	$header_title = 'Edit PostComment';
		$module_title = 'PostComments';
	
		
		$this->set(compact('header_title','module_title'));	
	  
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid post', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
           
           
            if ($this->PostComment->save($this->data)) {
                $this->Session->setFlash(__('The post has been saved', true), 'message/success');
                $this->redirect('index');
            } else {
                $this->Session->setFlash(__('The post could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->PostComment->read(null, $id);
        }
     
    }

    function admin_delete($id = null) {
        $this->layout='default_admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for content', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->PostComment->delete($id)) {
            $this->Session->setFlash(__('PostComment deleted', true), 'message/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('PostComment was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }
	
	
	function send_mail($id)
	{
			
		$where = array('PostComment.id'=>$id);
		$post_comments = $this->PostComment->find($where);
		$message = $post_comments['PostComment']['comments'];	
			
		$to      = 'arifhossen2010@gmail.com';
		$subject = 'CodeShare Post Comments Email Notification';		
		
		$headers = 'From: info@codeshare.info' . "\r\n" .
		'Reply-To: arifhossen2010@gmail.com' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();
				
		mail($to, $subject, $message, $headers);		
	
	}

    
	
	

}

?>