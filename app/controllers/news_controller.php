<?php
class NewsController extends AppController {

	var $name = 'News';
	
	var $components = array('FileUpload');

	
	 function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('index','view');
	}
	
	function index() {
		$this->News->recursive = 0;
		$this->set('news', $this->paginate());
	}

	function view($id = null) {
		$this->layout = 'page-inner-staticcontent';
		if (!$id) {
			$this->Session->setFlash(__('Invalid news', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('news', $this->News->read(null, $id));
	}

	


	function admin_index() {
		$this->layout = 'default_admin';	
		$this->News->recursive = 0;
		$this->set('newsLists', $this->paginate());
	}

	function admin_view($id = null) {
		$this->layout = 'default_admin';	
		if (!$id) {
			$this->Session->setFlash(__('Invalid news', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('news', $this->News->read(null, $id));
	}

	function admin_add() {
		$this->layout = 'default_admin';	
		if (!empty($this->data)) {
		
		  /*========= Upload File Start=============*/
								
				//Check here the category image array is empty or not
				if($this->data['News']['picture']['name'] != null){				
				$target_path = WWW_ROOT."images".DS."news";
				$target_path_profile ="images"."/"."news/";		        
						
					$filename = $this->data['News']['picture']['name'];
					$tmpname  = $this->data['News']['picture']['tmp_name'];
					$filesize = $this->data['News']['picture']['size'];
					$filetype = $this->data['News']['picture']['type'];					
					$filenameStore = $this->data['News']['picture']['name'];
					
						
								
					if(strlen($filename) <> 0){
						$newFileName = $this->FileUpload->upload($filename, $tmpname, $filesize, $target_path, 'jpeg,jpg,png,gif');						
						$this->data['News']['image'] = $target_path_profile.$filenameStore;						
										
					}
					else{
						$this->Session->setFlash("The file type is not valid please enter the valid file type",true);
						$this->redirect(array('action'=>'add'));
					}					
					
				}
				else
				{
					$this->data['News']['image'] = '';
				}
				
			
			
				
		
			$this->News->create();
			if ($this->News->save($this->data)) {
				$this->Session->setFlash(__('The news has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The news could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		$this->layout = 'default_admin';	
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid news', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
		
		    
		    /*========= Upload File Start=============*/
								
				//Check here the category image array is empty or not
				if($this->data['News']['picture']['name'] != null){				
				$target_path = WWW_ROOT."images".DS."news";
				$target_path_profile ="images"."/"."news/";		        
						
					$filename = $this->data['News']['picture']['name'];
					$tmpname  = $this->data['News']['picture']['tmp_name'];
					$filesize = $this->data['News']['picture']['size'];
					$filetype = $this->data['News']['picture']['type'];	
					
					$filenameStore = $this->data['News']['picture']['name'];
					
						
								
					if(strlen($filename) <> 0){
						$newFileName = $this->FileUpload->upload($filename, $tmpname, $filesize, $target_path, 'jpeg,jpg,png,gif');						
						$this->data['News']['image'] = $target_path_profile.$filenameStore;						
										
					}
					else{
						$this->Session->setFlash("The file type is not valid please enter the valid file type",true);
						$this->redirect(array('action'=>'add'));
					}					
					
				}
				else
				{
					$this->data['News']['image'] = '';
				}
				
			
				
		 
			if ($this->News->save($this->data)) {
				$this->Session->setFlash(__('The news has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The news could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->News->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for news', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->News->delete($id)) {
			$this->Session->setFlash(__('News deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('News was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
