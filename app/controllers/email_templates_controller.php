<?php
class EmailTemplatesController extends AppController {

	var $name = 'EmailTemplates';

	
	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('index','view');
	}
	
	
	function admin_index() {
		$this->layout = 'default_admin';	
		$this->EmailTemplate->recursive = 0;
		$this->set('emailtemplates', $this->paginate());
	}

	function admin_view($id = null) {
		$this->layout = 'default_admin';	
		if (!$id) {
			$this->Session->setFlash(__('Invalid news', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('news', $this->News->read(null, $id));
	}

	function admin_add() {
		$this->layout = 'default_admin';	
		if (!empty($this->data)) {
			$this->EmailTemplate->create();
			if ($this->EmailTemplate->save($this->data)) {
				$this->Session->setFlash(__('The email template has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email template could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		$this->layout = 'default_admin';	
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid news', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->EmailTemplate->save($this->data)) {
				$this->Session->setFlash(__('The Email Template has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Email Template could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->EmailTemplate->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for news', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->EmailTemplate->delete($id)) {
			$this->Session->setFlash(__('Email Template deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Email Template was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
