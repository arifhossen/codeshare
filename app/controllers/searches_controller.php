<?php

/**
 * Description of Dashboard
 *
 * @author SkyTechBD
 */
class SearchesController extends AppController {

    var $name = 'Searches';
    var $uses=array('Category');

     function beforeFilter() {
        parent::beforeFilter();
		$this->Auth->allow('admin_index','index');
       
    }

    function admin_index() {
        $this->layout = 'admin_inner';
    }
	
	 function index(){
		
		$this->layout = 'default';
		
		if(!empty($this->data)){
		//	debug($this->data);			
			$category_id = $this->data['Search']['category_id'];		
		    $this->redirect(array('controller'=>'products','action'=>'index',$category_id));
		}
		
		
		$label=0;		
		$categoryArray = array();
		$where=array();
		$where = array(array('Category.parent_id' => 0));				
		$categories = $this->paginate($this->Category, $where);		
		$i=0;
		foreach($categories as $key=>$value):			
			$parent_id = $value['Category']['id'];
			//debug($subid);	
			$categoryarray[$parent_id]['label'] =  $label;
			$categoryarray[$parent_id]['category_name'] =  $value['Category']['category_name'];
			$categoryarray[$parent_id]['category_description'] =  $value['Category']['category_description'];
			$categoryarray[$parent_id]['category_image'] =  $value['Category']['category_image'];
			$categoryarray[$parent_id]['category_icon'] =  $value['Category']['category_icon'];
			$categoryarray[$parent_id]['Subcategory'] =  $this->Subcategory($parent_id,$label+1);	
		$i++;
		endforeach;			
		$this->set('categories', $categoryarray);
    }
	
	function Subcategory($parent_id = null,$label=Null){
		$categoryArray = array();
		$activearray=array();			
		$where=array();
		$where = array(array('Category.parent_id' => $parent_id));			
		$categories = $this->paginate($this->Category, $where);				
		
		$subcategoryarray = array();
		foreach($categories as $key=>$value):
			$subid = $value['Category']['id'];
				$subcategoryarray[$subid]['active_id'] =  $parent_id;
				$subcategoryarray[$subid]['label'] =  $label;
				$subcategoryarray[$subid]['category_name'] =  $value['Category']['category_name'];
				$subcategoryarray[$subid]['category_description'] =  $value['Category']['category_description'];
				$subcategoryarray[$subid]['category_image'] =  $value['Category']['category_image'];
				$subcategoryarray[$subid]['category_icon'] =  $value['Category']['category_icon'];
				$subcategoryarray[$subid]['Subcategory'] =  $this->Subcategory($subid,$label+1);
				//$activearray[$subid]['active_id'] =  $this->Subcategory($subid,$label+1);
		endforeach;
		return $subcategoryarray;
	}
}

?>
