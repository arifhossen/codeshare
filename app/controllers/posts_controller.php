<?php
class PostsController extends AppController {

    var $name = 'Posts';  
	var $uses = array('Post','Category','Tag','PostTag');
   var $components = array('FileUpload','RequestHandler');

  function beforeFilter()
	{
	
		parent::beforeFilter();	
		$this->Auth->allow('*');	
		if(!empty($this->params['admin']))
			$this->layout = "inner_common_layout";
			
			
			
	}


    
	function index() {     
       
	  
	    $this->Post->recursive = 2;
		
		/***************** Search By Keyword ***************/
		
		if(!empty($this->data))
		{
		
			$keyword = $this->data['Post']['search_keyword'];			
			
			$post_ids_by_search = $this->search_posts($keyword);
			
		
			$this->paginate = array('conditions'=>array('Post.id'=>$post_ids_by_search,'Post.status'=>1),'limit'=>'3','order'=>'Post.id DESC');		
			
			
		
		}else
		{
		
			if(isset($this->params['pass']['0']))
			{
			     $where_category_name = array('Category.name'=>$this->params['pass']['0']);
				 $category_info = $this->Category->find($where_category_name);
				 $category_id = $category_info['Category']['id'];				
				 $this->paginate = array('conditions'=>array('Post.category_id'=>$category_id,'Post.status'=>1),'limit'=>'3','order'=>'Post.id DESC');		
			}
			else
			{
			
				 $this->paginate = array('conditions'=>array('Post.status'=>1),'limit'=>'3','order'=>'Post.id DESC'); 
			
			}
		
		
		
		}
		
		
		
		/************** Search By Keyword *****************/
		
        $this->set('posts', $this->paginate());
        
	}
	
	function search_posts($keyword)
	{
	
	  
	   $this->Post->recursive = 0;
	   $whereInTitle = array('Post.title LIKE'=>"%$keyword%",'Post.status'=>1);
	   $postIdsByTitle = $this->Post->find('list',array('conditions'=>$whereInTitle,'fields'=>'Post.id'));
	   
	   $whereInDetails = array('Post.details LIKE'=>"%$keyword%",'Post.status'=>1);
	   $postIdsByDetails = $this->Post->find('list',array('conditions'=>$whereInDetails,'fields'=>'Post.id'));
	   
	   
	   $post_ids_list = array_merge($postIdsByTitle,$postIdsByDetails);
	  
	  return $post_ids_list;
	}
	
	function details($title = null) {         
	    
		
		$this->Post->recursive = 2;
		$where = array('Post.title LIKE'=>"%$title%");
		$posts = $this->Post->find($where);
		$this->set('posts',$posts);        
	}
	
	function get_recent_posts() {        
	    	   
	   $postLists = $this->Post->find('all',array('LIMIT'=>'10','order'=>'Post.id DESC'));	  
	   return $postLists;		
        
	}
	
	function get_category_list()
	{
		
		$category_list = $this->Category->find('all');

       return $category_list;	
	
	}		
	
    function admin_index() {     
       
	    $header_title = 'Manage Post';
	    $module_title = 'Posts';
	    $this->set(compact('header_title','module_title'));	
	   
	    $this->Post->recursive = 2;
        $this->paginate = array('limit'=>'10','order'=>'Post.id DESC'); 
        $this->set('posts', $this->paginate());
        
	}	

    function admin_view($id = null) {
       $this->layout='default_admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid content', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('content', $this->Post->read(null, $id));
    }

    function admin_add() {
       
		$header_title = 'Add New Post';
		$module_title = 'Posts';
		$categories = $this->Category->find('list');
		$tags = $this->Tag->find('list');		
		
		$this->set(compact('header_title','module_title','categories','tags'));	
		
	   
        if (!empty($this->data)) {
            $this->Post->create();
           
            if ($this->Post->save($this->data)) {
			
			  
			  /****************** SAVE TAG ID ***************/
			   $this->data['PostTag']['post_id'] = $this->Post->getLastInsertId();			   
			   
			   foreach($this->data['Post']['tag_id'] as $key=>$id)
			   {			   
			   	   $this->data['PostTag']['tag_id'] = $id;
				   $this->PostTag->create();
				   $this->PostTag->save($this->data['PostTag']);	 			   
			   }
			 
			   /********* END SAVE TAG DATA ****************/
			     
			    
                $this->Session->setFlash(__('The Post has been saved', true), 'message/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Post could not be saved. Please, try again.', true));
            }
        }
       
    }

    function admin_edit($id = null) {
      
	  
	  	$header_title = 'Edit Post';
		$module_title = 'Posts';
		$categories = $this->Category->find('list');	
		
		$categories = $this->Category->find('list');
		$tags = $this->Tag->find('list');		
		
		$this->set(compact('header_title','module_title','categories','tags'));	
	  
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid post', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
           
           
            if ($this->Post->save($this->data)) {
			
			
			   
			   $this->delete_old_tag($this->data['Post']['id']);  
			    /****************** SAVE TAG ID ***************/
			   $this->data['PostTag']['post_id'] = $this->data['Post']['id'];			   
			   
			   
			  if(isset($this->data['Post']['tag_id']))
			  {
			   
				   foreach($this->data['Post']['tag_id'] as $key=>$id)
				   {			   
					   $this->data['PostTag']['tag_id'] = $id;
					   $this->PostTag->create();
					   $this->PostTag->save($this->data['PostTag']);	 			   
				   }
				   
			   }
			 
			   /********* END SAVE TAG DATA ****************/
			
			
			
                $this->Session->setFlash(__('The post has been saved', true), 'message/success');
                $this->redirect('index');
            } else {
                $this->Session->setFlash(__('The post could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Post->read(null, $id);
			
			$tag_array = array();
			foreach($this->data['PostTag'] as $key=>$value)
			{
			
				$tag_array[] = $value['tag_id'];
			}			
			
			$this->set('tag_array',$tag_array);
			
        }
     
    }
	
	
	function delete_old_tag($post_id)
	{
	
	    $where = array('PostTag.post_id'=>$post_id);
		$this->PostTag->deleteAll($where);
	}

    function admin_delete($id = null) {
        $this->layout='default_admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for content', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Post->delete($id)) {
            $this->Session->setFlash(__('Post deleted', true), 'message/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Post was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }

    
	
	

}

?>