<?php
class PaymentGatewaysController extends AppController {
    var $name = 'PaymentGateways';	

	var $uses = array('PaymentGateway');		
		
	var $helpers = array('Ajax');
	var $components = array('AuthorizeDotNet','Paypal','FirstDataGlobalGateway');

	
	function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('*');		
		$this->layout = 'inner_common_layout';
    }
	
	
	
	function sample_data()
	{
				
		$amount = '3.99';
		$x_card_num = '6011000000000012';
		$x_exp_date = '04/15';
		$x_card_code = '782';		
		$x_first_name = 'John';
		$x_last_name = 'Doe';
		$x_address = '123 Four Street';
		$x_city = 'San Francisco';
		$x_state = 'CA';
		$x_country = '';
		$x_zip = '94133';
		$x_email = 'US';
		
		
		$field_data_array = array(				
				'amount' => $amount, 
				'card_num' => $x_card_num, 
				'exp_date' => $x_exp_date,
				'first_name' => $x_first_name,
				'last_name' => $x_last_name,
				'address' => $x_address,
				'city' => $x_city,
				'state' => $x_state,
				'country' => $x_country,
				'zip' => $x_zip,
				'email' => $x_email,
				'card_code' => $x_card_code
				);
		
		return $field_data_array;
	
	}	
	function admin_authorize_net_trasaction() {
	
	    $header_title = 'Authoriz.net Test Store';
	    $module_title = 'Payment Gateway';
	    $this->set(compact('header_title','module_title'));	
		
	
		
		
		if(!empty($this->data))
		{		
		
			/******************* Call Trasaction Module ************/
			$transaction_result = $this->AuthorizeDotNet->aim_trasaction_process($this->sample_data());
			$this->set('transaction_result',$transaction_result);
			/********** End Transaction ******************/
			
		}
		
		
		/************** Refund Process ************/		
		//$this->AuthorizeDotNet->refund_process('2177979100');	
        //	$this->AuthorizeDotNet->getTransactionHistory();		
		/************END REFUND PROCESS ***********/
		
		
				
	}
	
	
	
	function admin_paypal_transaction_payment_pro()
	{
	
	    $header_title = 'Paypal Payment Pro';
	    $module_title = 'Payment Gateway';
	    $this->set(compact('header_title','module_title'));	
		
	    if(!empty($this->data))
		{
		  
		 
		  if($this->data['PaymentGateway']['paymentIndenty'] == 'do_direct_payment')
		  {
		  
			  $result = $this->Paypal->direct_payment_process($this->data['PaymentGateway']);
			 
			  $this->set('transaction_result',$result);
		  
		  }
		  else if($this->data['PaymentGateway']['paymentIndenty'] == 'express_checkout_payment')
		  {	  
		    
			$result = $this->Paypal->expressCheckout_payment_process($this->data['PaymentGateway']);
		 
			$this->set('expressCheckout_result',$result);
		  }	
		  else
		  {
		  
		  }
		
		}
	
	
	
	}
	
	
	
    function admin_first_data_global_gateway_transaction()
	{
	
		$header_title = 'First Data Global Gateway';
	    $module_title = 'Payment Gateway';
	    $this->set(compact('header_title','module_title'));	
		
	    if(!empty($this->data))
		{
		  
			  $result = $this->FirstDataGlobalGateway->payment_process($this->data['PaymentGateway']);
			 
			  $this->set('transaction_result',$result);
		  
		 
		
		}
	
	
	}
	
	
	
		
	
}
?>