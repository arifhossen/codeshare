<?php
class SitemodulesController extends AppController {

	var $name = 'Sitemodules';
	
	var $uses = array('Sitemodule','User');
	//var $uses = array('User','UserItem','Industrysector','EmailTemplate', 'Registeredaddress', 'Otheremail','Item','Category','Emailnotification');

	
	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('*');
		$this->Auth->autoRedirect = false;
		
	}
	
	function index($id = null) {
		$this->layout = "pageinner";
				
		$where=array();
		$where = array('Sitemodule.delflag'=>'0','AND'=>array('Sitemodule.is_active'=>'1'));		
		$this->paginate = array('limit' => 10);
		$this->paginate = array('order' => 'id DESC');	
		$Sitemodules =  $this->paginate($this->Sitemodule, $where);	
		$this->set('Sitemodules',$Sitemodules);		
		
	}
	
	function menulink()
	{
		$where = array('Sitemodule.delflag'=>'0',array('AND'=>array('Sitemodule.is_active'=>'1')));
		$order = 'id DESC';
		$Sitemodules = $this->Sitemodule->findAll($where, '', $order, 3);
		
		return $Sitemodules;
	}		
			
		
	
	function admin_permission($id = null) {
		$this->layout = "admin_inner";

		$where=array();
		$where = array('Sitemodule.delflag'=>'0','AND'=>array('Sitemodule.is_active'=>'1'));		
		$Sitemodules = $this->Sitemodule->find('all',$where);	
		$this->set('modules',$Sitemodules);		
				
		$roleArrayList = array();
		$roleList = $this->Role->find('all');

		$this->set('roleLists',$roleList);
		
		foreach($roleList  as $key=>$value){		
			$role_id = $value['Role']['id'];
			$roleName = $value['Role']['name'];
			$roleArrayList[$role_id] = $roleName;
		}	
		$this->set('roleArrayList',$roleArrayList);
		
		if(!empty($this->data)){
		    $moduleIdArray ='';
			$selected_role_id = $this->data['Sitemodule']['role_selected'];
			$modules = $this->data['Module'];			
			
			foreach($modules as $key=>$value){				
				if($value !=0){					
					$moduleIdArray[] = $value;				
				}		
			}
					
			$module_id = implode(',',$moduleIdArray);			
			
			$this->data['Role']['id'] = $selected_role_id;
			$this->data['Role']['module_id'] = $module_id;
			
			//if($this->User->query('update users set module_id  = "'.$module_id.'" where role_id = '.$selected_role_id.'')){
			if($this->Role->save($this->data)){
				$this->Session->setFlash(__('Module permission Successfully', true));
				$this->redirect(array('controller'=>'sitemodules','action'=>'admin_permission'));
			} else {
				$this->Session->setFlash(__('Module Permission could not assigned', true));
			}
		}
	}
	
	function admin_index($id = null) {
		$this->layout = "admin_inner";
				
		$where=array();
		$where = array('Sitemodule.delflag'=>'0','AND'=>array('Sitemodule.is_active'=>'1'));	
		
		$this->paginate = array('order' => 'id ASC');
		$this->paginate = array('limit' => 100);	
		$Sitemodules =  $this->paginate($this->Sitemodule, $where);	
		$this->set('Sitemodules',$Sitemodules);		
	}
	
	function admin_add() {
		$this->pageTitle = 'Add new Sitemodule';
		$this->layout ='admin_inner';		
		
		if (!empty($this->data)) {		
		   
					
			$this->Sitemodule->create();
			if ($this->Sitemodule->save($this->data)) {
				$this->Session->setFlash(__('Sitemodule has been saved', true));
				$this->redirect(array('action'=>'admin_index'));
			} else {
				$this->Session->setFlash(__('The Sitemodule could not be saved. please, try again.', true));
			}
		}
	}
	
	function admin_edit($id = null) {
		$this->pageTitle = 'Add newsletter';
		$this->layout ='admin_inner';		
		
		if(!empty($this->data))
		{
					
			if ($this->Sitemodule->save($this->data)) {
				$this->Session->setFlash(__('Sitemodule has been saved', true));
				$this->redirect(array('action'=>'admin_index'));
			} else {
				$this->Session->setFlash(__('The Sitemodule could not be saved. please, try again.', true));
			}
		
		}
		
		if(empty($this->data))
		{
				
			$this->data = $this->Sitemodule->read(null, $id);
		}	
	}  	
	
	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Sitemodule', true));
			$this->redirect(array('action'=>'admin_index'));
		}
		if ($this->Sitemodule->delete($id)) {
			$this->Session->setFlash(__('Sitemodule has been deleted', true));
			$this->redirect(array('action'=>'admin_index'));
		}
	}	
}
?>