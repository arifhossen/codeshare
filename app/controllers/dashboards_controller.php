<?php

/**
 * Description of Dashboard
 *
 * @author Mohammad ARIF Hossen
 */
class DashboardsController  extends AppController {

    var $name = 'Dashboards'; 
	
	var $uses=array('Dashboard','User','News','UserBrokerInformation','UserAgentInformation','UserOrderInformation','Alert','State');
	var $components = array('FileUpload');
	

   function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('change_password','index','view','managewarranty','changepassword','alerts','profile_edit','reward_history','referral_summary','contactinfo_edit');
		$this->Auth->autoRedirect = false;
		$this->layout = 'default';
	}
	
	
	
	function app()
	{
		 $this->layout = 'default';		
	
	}
	
	function mobile()
	{
		 $this->layout = 'default';		
	
	}
	
	
	function web()
	{
		 $this->layout = 'default';		
	
	}

    function index() {       
        $this->layout = 'default';		
		
		$categoryfooterArray = array();		
		$user_id = $this->Session->read('user_id');			
		
		if($user_id =='')
		{
			$this->Session->setFlash(__('Your have access un authorize', true));
			$this->redirect(array('controller'=>'users','action'=>'login'));
		}
		$this->User->recursive = 2;
		$userInfo = $this->User->read(null,$user_id);
		$this->set('userInfo', $userInfo);
		
		//pr($userInfo);
		//die();
		
		$this->Session->write('userInfo',$userInfo);	
		
		
		$this->paginate = array('limit' => 1);
		$alerts = $this->paginate($this->Alert);	
		
		$this->set('alerts',$alerts);		
		
		
		$this->paginate = array('limit' => 100);
		$newsLists = $this->paginate($this->News);	
		
		$this->set('newsLists',$newsLists);		
		
		
		/****************** Get Referral Summary ******************/
		
		$member_id = $userInfo['User']['member_id'];
		$where = array('User.referral_member_id'=>$member_id);	
		
		$referralSummaryLists = $this->User->find('all',array('conditions'=>$where));
		$this->set('referralSummaryLists',$referralSummaryLists);	
				
		/***************** End Get Referral Summary **************/
		
		
    }
	
	

	function alerts()
	{
	
		$this->layout = 'default';	
		$newsLists = $this->News->find('all');
		$this->set('newsLists',$newsLists);		
	
	}
	
	function news()
	{
	
		$this->layout = 'default';	
		$newsLists = $this->News->find('all');
		$this->set('newsLists',$newsLists);		
	
	}
	
	function news_details()
	{
	
		$this->layout = 'default';	
		
		$where = array('News.id'=>$this->params['pass']['0']);
		$newsLists = $this->News->find($where);
		$this->set('newsLists',$newsLists);		
	
	}
	
	
	
	function billinginfo_edit()
	{
	
		$user_id = $this->Session->read('user_id');
		$this->User->recursive = 2;		 
		$userInfo = $this->User->read(null,$user_id);
		$this->set('userInfo', $userInfo);	
		 
		$stateLists = $this->State->find('all');
		$this->set('stateLists',$stateLists);
		
		if(!empty($this->data))
		{
			$this->data['UserOrderInformation']['user_id'] = $user_id;			
			$where = array('UserOrderInformation.user_id'=>$user_id);
			$userOrderInfo = $this->UserOrderInformation->find($where);
			
			
			$this->data['UserOrderInformation']['id'] = $userOrderInfo['UserOrderInformation']['id'];
			
			$this->data['UserOrderInformation']['type'] = $this->data['UserOrderInformation']['type'];
			
			
			$this->UserOrderInformation->save($this->data);
			$this->Session->setFlash("Payment Information Updated Successfully",true);
			$this->redirect(array('action'=>'billinginfo_edit'));
		   		
		
		
		}
		
		//pr($userInfo);
		
	
	
	}
	
	function paybill()
	{
	
		 $user_id = $this->Session->read('user_id');
		 $this->User->recursive = 2;		 
		 $userInfo = $this->User->read(null,$user_id);
		 $this->set('userInfo', $userInfo);	
		 
		 
		$stateLists = $this->State->find('all');
		$this->set('stateLists',$stateLists);
		
	
	
	}
	
	function reward_history()
	{
	
		$this->layout = 'default';	
		$newsLists = $this->News->find('all');
		$this->set('newsLists',$newsLists);		
	
	}
	
	
	function referral_summary()
	{
	
		$this->layout = 'default';	
		
		/****************** Get Referral Summary ******************/
		$user_id = $this->Session->read('user_id');
		$userInfo = $this->User->read(null,$user_id);	
		
		$member_id = $userInfo['User']['member_id'];
		$where = array('User.referral_member_id'=>$member_id);		
		$referralSummaryLists = $this->User->find('all',array('conditions'=>$where));
		$this->set('referralSummaryLists',$referralSummaryLists);	
				
		/***************** End Get Referral Summary **************/	
	
	}
	
	function profile_edit()
	{
		
		 if (!empty($this->data)) 
		 {
		 
		 	
			
		 
		 /*========= Upload File Start=============*/
								
				//Check here the category image array is empty or not
				if($this->data['User']['picture']['name'] != null){				
				$target_path = WWW_ROOT."images".DS."profile";
				$target_path_profile ="images"."/"."profile/";
				
			
			        
						
					$filename = $this->data['User']['picture']['name'];
					$tmpname  = $this->data['User']['picture']['tmp_name'];
					$filesize = $this->data['User']['picture']['size'];
					$filetype = $this->data['User']['picture']['type'];							
								
					if(strlen($filename) <> 0){
						$newFileName = $this->FileUpload->upload($filename, $tmpname, $filesize, $target_path, 'jpeg,jpg,png,gif');						
						$this->data['User']['picture'] = $target_path_profile.$newFileName;		
						
						$this->data['User']['id'] = $this->Session->read('user_id');
						
						//pr($this->data);
						//die();				
						
						if($this->User->save($this->data,array('validate' => false)))
						{
							$this->Session->setFlash("Profile Picture Uploaded Successfully",true);
						    $this->redirect(array('action'=>'profile_edit'));
							
						}
										
					}
					else{
						$this->Session->setFlash("The file type is not valid please enter the valid file type",true);
						$this->redirect(array('action'=>'profile_edit'));
					}					
					
				}//End
				else{
					$this->Session->setFlash("Please Choose a profile picture.",true);
					$this->redirect(array('action'=>'profile_edit'));
				}
		 
		 }
		 
		 $user_id = $this->Session->read('user_id');		 
		 $userInfo = $this->User->read(null,$user_id);
		 $this->set('userInfo', $userInfo);	
		 
	
	
	
	}
	
	function contactinfo_edit()
	{
	
	
		$this->User->recursive = 2;
		
		$stateLists = $this->State->find('all');
		$this->set('stateLists',$stateLists);
		
		
		$user_id = $this->Session->read('user_id');	
		
		
		$userInfo = $this->User->read(null,$user_id);
		$signedup_date = $userInfo['User']['signup_date'];
		$this->set('signedup_date',$signedup_date);
		
		
		if (!empty($this->data)) 
		{
		 
		 	$this->UserAgentInformation->save($this->data);	
			//die();
			$this->UserBrokerInformation->save($this->data);						
			$this->Session->setFlash("Contact Info Updated Successfully.",true);	
			
			
		 
		}
		else
		{
			$this->data = $this->User->read(null,$user_id);
		
		}
		
		$this->data = $this->User->read(null,$user_id);
		
		
		
		
		//pr($this->data);
		//die();
		
	
	
	}
	
	function change_password(){
			$this->layout = 'default';
			
			
			if(!empty($this->data))
			{
			
			
			$user_id = $this->Session->read('user_id');			
			$oldpassword = $this->Auth->hashPasswords ($this->data['User']['password']);
			$newpassword = $this->Auth->hashPasswords($this->data['User']['newpassword']);
		    
			
			$where = array('User.id'=>$user_id,'AND'=>array('User.password'=>$oldpassword));	
			$userData = $this->User->find($where);
			
							
			if(!empty($userData)){						

				if($this->User->query("UPDATE users SET password = '$newpassword' WHERE id = '$user_id'")){
					$this->Session->setFlash(__('Your password has been changed', true));
					$this->redirect(array('controller'=>'dashboards','action'=>'change_password'));
				}else{
					$this->Session->setFlash(__('Your old password is not correct', true));
					$this->redirect(array('controller'=>'dashboards','action'=>'change_password'));
				}
			
			}else{
				$this->Session->setFlash(__('Your old password is not correct', true));
				$this->redirect(array('controller'=>'dashboards','action'=>'change_password'));
			}
			
		}
		
		}
	
	
	
   
}

?>