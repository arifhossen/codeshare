<?php
class UsersController extends AppController {
    var $name = 'Users';
	
	
	var $paginate = array(
        'User' => array(
            'limit' => 20,
            'order' => array(
                'User.id' => 'asc',
            ),
        ),
    );
    
    /**
     * Set this to false if you don't want to store clear passwords in the database
     * @var bool
     * @access private
     */
	 
    var $_store_clear_password = true;

	var $uses=array('User','UserRole');
	
	var $helpers = array('Ajax');
	var $components = array('RequestHandler');
	
     function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('add','login','signup','upgrade_login','registrationconfirmation','changeuserinformation','check_duplicate_user','change_password','useractivation');
		
	}
	
	
	function admin_dashboard()
	{
		$this->layout = 'admin_dashboard';		
	
	}
	
	
	
	function admin_userlist()
	{
	
		$this->layout = 'default_admin';	
		
		$userList = $this->User->find('all');
		
		$this->set('userLists',$userList);		
	
	
	}
	
	
		
	function admin_useradd() {
	    $this->layout = 'default_admin';	
		
		$p = 'Farid';
		$abc = 'test';		
		$this->set(compact('p', 'abc')); 		
		
		$first_name = $this->data['User']['first_name'];
		$last_name = $this->data['User']['last_name'];
		
		
				
        if (!empty($this->data)) {	
		
		
            $this->User->create();			
			        
            if ($this->User->save($this->data)) {			
			
				$user_id = $this->User->getLastInsertID();	
				
			    $this->Session->setFlash('New Users Create Succefully.', true);			
				
			}
			
			
			
		}
		
	}			
	

	
	function check_duplicate_user() {
      
	   $status ='0';
	   $username = $this->params['pass']['0'];
	   
	   $where = array('User.username'=>$username);
	   $userInfo = $this->User->find($where);
	   
	   if(!empty($userInfo))
	   {   	
			$status = 1;	   
	   }
	   else
	   {
	   		$status = 0;   
	   }
	   
	   $this->set('status',$status);
	   
    }
	
	function admin_update_recurring_payment()
	{
	
		if(!empty($this->data))
		{
		
		    $date = $this->data['RecurringPayment']['recurring_year'].'-'.$this->data['RecurringPayment']['recurring_month'].'-'.$this->data['RecurringPayment']['recurring_day'];
			
			
			$this->data['RecurringPayment']['date'] = date('Y-m-d',strtotime($date));
			
			
		    if(isset($this->data['RecurringPayment']['enable']))
			{
				$this->data['RecurringPayment']['enable'] = 1;
			}
			else
			{
				$this->data['RecurringPayment']['enable'] = 0;
			}
			
			
			$this->RecurringPayment->save($this->data);
			 $this->redirect(array('controller'=>'users','action' => 'index'));
			$this->Session->setFlash(__('The User could not be saved. Please, try again.', true),'message/fail');
		
		}
		
	
	
	
	}
	
	function getYearList()
	{
	
		$yearLists = array();
		
		for($i = date('Y'); $i<=3000; $i++)
		{	
		
			$yearLists[] = $i;
		
		}
		
		
		return $yearLists;
	
	
	}
	
	function getMonthList()
	{
	
		$monthLists = array('1'=>'01',
							'2'=>'02',
							'3'=>'03',
							'4'=>'04',
							'5'=>'05',
							'6'=>'06',
							'7'=>'07',
							'8'=>'08',
							'9'=>'09',
							'10'=>'10',
							'11'=>'11',
							'12'=>'12');
		
		
		
	
		
		return $monthLists;
	
	
	}
	
	function getDaysList()
	{
	
		$dayLists = array();
		
		for($i = 1; $i<=31; $i++)
		{	
		
			$dayLists[] = $i;
		
		}
		
		
		return $dayLists;
	
	
	}
	
	function admin_index($id = null) {
       $this->layout = 'admin_dashboard';		
		
		
		$this->paginate = array('limit' => 2); 
			
		if(!empty($this->params['pass'][0])){
			$params_id = $this->params['pass'][0];
			$this->Session->write('params_id',$params_id);			
			
			$where =     array('User.role_id'=> $params_id);
			$users = $this->paginate('User',$where);
        	$this->set(compact('users'));
		}else{			
			$users = $this->paginate('User');	
			
		
			//pr($users);
			
       		 $this->set(compact('users'));
		}		
    }
	
	

    function add() {
        
		$this->layout = 'login';
        $this->pageTitle = 'Create a New Account or Log In';
        $this->set('registration', false);
       
		if (!empty($this->data)) {
            $this->User->set($this->data);
			
			$first_name = $this->data['User']['first_name'];
			$last_name = $this->data['User']['last_name'];
			$full_name = $first_name.''.$last_name;
			
			$this->data['User']['full_name'] = $full_name;
			$this->data['User']['is_active'] = 0;

            //user verification code
			$user_verificationcode = rand(100000,999999);
			$this->data['User']['activation_key'] = $user_verificationcode;
			
			
            if ($this->User->validates()) {
                $this->data['User']['password'] = $this->data['User']['clear_password'];
                $this->data = $this->Auth->hashPasswords($this->data);
                if (!$this->_store_clear_password) {
                    unset($this->data['User']['clear_password']);
                }
                if($this->User->save($this->data, false)){
				
				$user_id = $this->User->getLastInsertID();
				$role_id = $this->data['User']['role_id'];
				
				$this->data['UserRole']['user_id'] = $user_id;
				$this->data['UserRole']['role_id'] = $role_id;
					
				 	$this->UserRole->create();  
					if($this->UserRole->save($this->data['UserRole'])){
					
					 $siteName  = env('HTTP_HOST').'/deshichain';
					 $controller = 'users';
					 $action = 'useractivation';
					 $userid = $user_id;
					 $verificationcode = $user_verificationcode;
					 $userActivation = $siteName.'/'.$controller.'/'.$action.'/'.$userid.'/'.$verificationcode;
					 $userActivationLink ="<a href='$userActivation'>'$userActivation'</a>";
					
					
					$message ='<table width="100%" border="0">
										<tr>
										<td bgcolor="#CCCCCC"><div align="left"><strong>User Information </strong></div></td>
										</tr>
										
										<tr>
										<td>Dear '.$full_name.',</td>
										</tr>
										
										<tr>
										<td>Your account information are as follows</td>
										</tr>
										
										<tr>
										  <td>User name:'.$this->data['User']['username'].'</td>
  </tr>
										<tr>
										  <td>Password:'.$this->data['User']['clear_password'].'</td>
  </tr>
										<tr>
										  <td>Please click on the following link to active your account with deshichain.com.com</td>
  </tr>
										<tr>
										  <td>'.$userActivationLink.'</td>
										  </tr>
										<tr>
										  <td>&nbsp;</td>
										  </tr>
										<tr>
										  <td>&nbsp;</td>
										  </tr>
										</table>';
					
						//user activation link set here
						$to_address = $this->data['User']['email'];
						//$admin_address = 'shaharia@sholoana.com';
						
						$to = 	$to_address;

						$subject = 	"User Registration Confirmation";
						$headers = 	"From: order@deshichain.com\r\n";
						$headers .= "MIME-Version: 1.0\r\n"
							. "Content-Type: text/html; charset=\"iso-8859-1\"\r\n"
							. "Content-Transfer-Encoding: 7bit\r\n";
					
						if(mail($to, $subject, $message, $headers))
						{
							$this->Session->setFlash(__('Your Registration information has been received by us and a confirmnation email send to your mail please check.', true));
						} else {
							$this->Session->setFlash(__('user Registration do not save', true));
						}
					 $this->Session->setFlash(__('Your Registration information has been received please logon to your account.', true));
					 $this->redirect(array('controller'=>'users','action' => 'registrationconfirmation'));
					}
				}else{				
                	$this->Session->setFlash(__('The User could not be saved. Please, try again.', true),'message/fail');
				}
               
            }
        }
    }
	
	function registrationconfirmation(){
		$this->layout='login';
	}
	
	
	function admin_add() {
        $this->layout = 'admin_dashboard';
        if (!empty($this->data)) {
            $this->User->create();          
            if ($this->User->save($this->data)) {
				$user_id = $this->User->getLastInsertID();
				$role_id = $this->data['User']['role_id'];
				
				$this->data['UserRole']['user_id']=$user_id;
				$this->data['UserRole']['role_id']=$role_id;
					
				 	$this->UserRole->create();  
					if($this->UserRole->save($this->data['UserRole'])){
						$this->Session->setFlash(__('The User has been saved', true));
               		 	$this->redirect(array('action' => 'index'));
					}
					else{
						$this->Session->setFlash(__('The User could not be saved. Please, try again.', true),'message/fail');
               			 unset($this->data['User']['password']);
					}
				
            } else {
                $this->Session->setFlash(__('The User could not be saved. Please, try again.', true),'message/fail');
                unset($this->data['User']['password']);
            }
        } else {
            // $this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
        }
		
		$roles = $this->User->Role->find('list');
		$this->set(compact('roles'));
    }
	
	 function admin_edit($id = null) {
        $this->layout = 'default_admin';
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid User', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->User->save($this->data)) {
                $this->Session->setFlash(__('The User has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->User->read(null, $id);
			$roles = $this->User->Role->find('list');
        }
        $this->set('users', $this->User->read(null, $id));
		$this->set(compact('roles'));		
    }
    
    function edit($id = null) {
        if (!empty($this->data)) {
            $fields = array_keys($this->data['User']);
            if (!empty($this->data['User']['clear_password']) || !empty($this->data['User']['confirm_password'])) {
                $fields[] = 'password';
            } else {
                $fields = array_diff($fields, array('clear_password', 'confirm_password'));
            }
            $this->User->set($this->data);
            if ($this->User->validates()) {
                if (!empty($this->data['User']['clear_password'])) {
                    $this->data['User']['password'] = $this->data['User']['clear_password'];
                }
                $this->data = $this->Auth->hashPasswords($this->data);
                if (!$this->_store_clear_password) {
                    unset($this->data['User']['clear_password']);
                }
                $this->User->save($this->data, false, $fields);
                $this->Session->setFlash('User Updated');
                $this->redirect('index');
            }
        } else {
            $user = $this->User->findById($id);
            if (empty($user)) {
                $this->Session->setFlash('Invalid User ID', 'flash_notice');
                $this->redirect('add');
            } else {
                unset($user['User']['clear_password']);
                $this->data = $user;
            }
        }
    }
	
	
   
	
	function admin_delete($id = null) {
        $this->layout = 'admin_inner';
        
		$params_id = $this->Session->read('params_id');
		if(!empty($params_id)){
			$params_id =  $params_id;
		}else{
			$params_id = ''; 
		}
		
		if($params_id == 9999){
		$role_name = 'Admin'; 
		}else if($params_id == '1'){
			$role_name = 'Medical Institute'; 
		}else if($params_id == 2){
			$role_name = 'Medical Corporate'; 
		}else if($params_id == 3){
			$role_name = 'Doctor'; 
		}else if($params_id == 4){
			$role_name = 'Site User'; 
		}else{
			$role_name = ''; 
		}
			
		if (!$id) {
            $this->Session->setFlash(__('Invalid id for '.$role_name.'', true));
            $this->redirect(array('action' => 'index',$params_id));
        }
        if ($this->User->delete($id)) {
            $this->Session->setFlash(__(''.$role_name.' deleted', true));
            $this->redirect(array('action' => 'index',$params_id));
        }
    }
	    
	function admin_login() {
       // $this->layout = "default";
		
		 $this->layout = "admin_login";
		 
        if (empty($this->data) == false) {		
				if ($this->Auth->user('role_id') != null) {					
					$userdetail = $this->validateLogin();
					if(($this->Auth->User('role_id') == '9999') && ($this->Auth->User('is_active') == '1')){ //For Super Admin
						$this->Session->write('roleid','9999');
						$this->Session->write('user_id',$userdetail['User']['id']);						
						$this->Session->write('rolename',$userdetail['User']['full_name']);
						
						$this->User->id = $this->Auth->user('id');
						$this->User->saveField('last_login', date('Y-m-d H:i:s'));
						
						if (isset($this->data['User']['remember_me']) && $this->data['User']['remember_me']) {
								$this->Cookie->write("User.username", $this->data['User']['username'], false, "30 Days");
							} elseif ($this->Cookie->read("User.username")) {
								$this->Cookie->delete("User.username");
							}
							
						$this->redirect(array('controller'=>'users','action'=>'dashboard'));
					}
					
					else if(($this->Auth->User('role_id') == '1') && ($this->Auth->User('is_active') == '1')){ //For Supplier
						$this->Session->write('roleid','1');
						$this->Session->write('user_id',$userdetail['User']['id']);						
						$this->Session->write('rolename',$userdetail['User']['full_name']);
						
						$this->User->id = $this->Auth->user('id');
						$this->User->saveField('last_login', date('Y-m-d H:i:s'));
						if (isset($this->data['User']['remember_me']) && $this->data['User']['remember_me']) {
								$this->Cookie->write("User.username", $this->data['User']['username'], false, "30 Days");
							} elseif ($this->Cookie->read("User.username")) {
								$this->Cookie->delete("User.username");
							}
						$this->redirect(array('controller'=>'users','action'=>'dashboard'));
					}
					
					else if(($this->Auth->User('role_id') == '2') && ($this->Auth->User('is_active') == '1')){ //For Store Manager 
						
						$this->Session->write('roleid','2');
						$this->Session->write('user_id',$userdetail['User']['id']);						
						$this->Session->write('rolename',$userdetail['User']['full_name']);
						
						$this->User->id = $this->Auth->user('id');
						$this->User->saveField('last_login', date('Y-m-d H:i:s'));
						if (isset($this->data['User']['remember_me']) && $this->data['User']['remember_me']) {
								$this->Cookie->write("User.username", $this->data['User']['username'], false, "30 Days");
							} elseif ($this->Cookie->read("User.username")) {
								$this->Cookie->delete("User.username");
							}
						$this->redirect(array('controller'=>'users','action'=>'dashboard'));
					}
					
					else if(($this->Auth->User('role_id') == '3') && ($this->Auth->User('is_active') == '1')){ //For Delivery manager
						
						$this->Session->write('roleid','3');
						$this->Session->write('user_id',$userdetail['User']['id']);						
						$this->Session->write('rolename',$userdetail['User']['full_name']);
						
						$this->User->id = $this->Auth->user('id');
						$this->User->saveField('last_login', date('Y-m-d H:i:s'));
						if (isset($this->data['User']['remember_me']) && $this->data['User']['remember_me']) {
								$this->Cookie->write("User.username", $this->data['User']['username'], false, "30 Days");
							} elseif ($this->Cookie->read("User.username")) {
								$this->Cookie->delete("User.username");
							}
						$this->redirect(array('controller'=>'users','action'=>'dashboard'));
					}
					
					else if(($this->Auth->User('role_id') == '4') && ($this->Auth->User('is_active') == '1')){ //For Accounts Manager
						
						$this->Session->write('roleid','4');
						$this->Session->write('user_id',$userdetail['User']['id']);						
						$this->Session->write('rolename',$userdetail['User']['full_name']);
						
						$this->User->id = $this->Auth->user('id');
						$this->User->saveField('last_login', date('Y-m-d H:i:s'));
						if (isset($this->data['User']['remember_me']) && $this->data['User']['remember_me']) {
								$this->Cookie->write("User.username", $this->data['User']['username'], false, "30 Days");
							} elseif ($this->Cookie->read("User.username")) {
								$this->Cookie->delete("User.username");
							}
						$this->redirect(array('controller'=>'users','action'=>'dashboard'));
					}
					
					else if(($this->Auth->User('role_id') == '6') && ($this->Auth->User('is_active') == '1')){ //For Dashboard user 						
						$this->Session->write('roleid','6');
						$this->Session->write('user_id',$userdetail['User']['id']);						
						$this->Session->write('rolename',$userdetail['User']['full_name']);
						
						$this->User->id = $this->Auth->user('id');
						$this->User->saveField('last_login', date('Y-m-d H:i:s'));
						if (isset($this->data['User']['remember_me']) && $this->data['User']['remember_me']) {
								$this->Cookie->write("User.username", $this->data['User']['username'], false, "30 Days");
							} elseif ($this->Cookie->read("User.username")) {
								$this->Cookie->delete("User.username");
							}
						$this->redirect(array('controller'=>'users','action'=>'dashboard'));
					}
					
					else if(($this->Auth->User('role_id') == '7') && ($this->Auth->User('is_active') == '1')){ //For Customer or free user
						$this->Session->write('roleid','7');
						$this->Session->write('user_id',$userdetail['User']['id']);						
						$this->Session->write('rolename',$userdetail['User']['full_name']);
						
						$this->User->id = $this->Auth->user('id');
						$this->User->saveField('last_login', date('Y-m-d H:i:s'));
						if (isset($this->data['User']['remember_me']) && $this->data['User']['remember_me']) {
								$this->Cookie->write("User.username", $this->data['User']['username'], false, "30 Days");
							} elseif ($this->Cookie->read("User.username")) {
								$this->Cookie->delete("User.username");
							}
						
						$addtocartlogin = $this->Session->read('addtocartlogin');						
						if(!empty($addtocartlogin)){							
							$this->redirect(array('controller'=>'products/billinginformation'));
						}else{
							$this->redirect(array('controller'=>'users','action'=>'dashboard'));
						}
					}					
				
					
				}else{
				
					$this->Session->setFlash(__('Your user/password do not match.', true));
					$this->set('error_message','Your user/password do not match');
				}
			}
	}
	
	
	
    function login() {		
		 $this->layout = "default";	
		
	
		 
		 if (empty($this->data) == false) {		
				if ($this->Auth->user('role_id') != null) {	
				
							
					$userdetail = $this->validateLogin();
					
					
					if(($this->Auth->User('role_id') == '2') && ($this->Auth->User('is_active') == '1')){ //For Supplier
						$this->Session->write('roleid','1');
						$this->Session->write('user_id',$userdetail['User']['id']);						
						$this->Session->write('rolename',$userdetail['User']['full_name']);
						$this->Session->write('fullname',$userdetail['User']['full_name']);
						
						$this->User->id = $this->Auth->user('id');
						$this->User->saveField('last_login', date('Y-m-d H:i:s'));
						if (isset($this->data['User']['remember_me']) && $this->data['User']['remember_me']) {
								$this->Cookie->write("User.username", $this->data['User']['username'], false, "30 Days");
							} elseif ($this->Cookie->read("User.username")) {
								$this->Cookie->delete("User.username");
							}
							
						if($this->Session->read('upgradepackage') == 1)
						{
							$this->redirect(array('controller'=>'packages','action'=>'index'));
						}
						else
						{
							
							$this->redirect(array('controller'=>'dashboards'));
						}	
					}
					
					else if(($this->Auth->User('role_id') == '3') && ($this->Auth->User('is_active') == '1')){ //For Store Manager 
						
						$this->Session->write('roleid','2');
						$this->Session->write('user_id',$userdetail['User']['id']);						
						$this->Session->write('rolename',$userdetail['User']['full_name']);
						
						$this->User->id = $this->Auth->user('id');
						$this->User->saveField('last_login', date('Y-m-d H:i:s'));
						if (isset($this->data['User']['remember_me']) && $this->data['User']['remember_me']) {
								$this->Cookie->write("User.username", $this->data['User']['username'], false, "30 Days");
							} elseif ($this->Cookie->read("User.username")) {
								$this->Cookie->delete("User.username");
							}
						$this->redirect(array('controller'=>'dashboards'));
					}
					
					else if(($this->Auth->User('role_id') == '4') && ($this->Auth->User('is_active') == '1')){ //For Delivery manager
						
						$this->Session->write('roleid','3');
						$this->Session->write('user_id',$userdetail['User']['id']);						
						$this->Session->write('rolename',$userdetail['User']['full_name']);
						
						$this->User->id = $this->Auth->user('id');
						$this->User->saveField('last_login', date('Y-m-d H:i:s'));
						if (isset($this->data['User']['remember_me']) && $this->data['User']['remember_me']) {
								$this->Cookie->write("User.username", $this->data['User']['username'], false, "30 Days");
							} elseif ($this->Cookie->read("User.username")) {
								$this->Cookie->delete("User.username");
							}
						$this->redirect(array('controller'=>'dashboards'));
					}
					
								
				
					
				}else{
					$this->Session->setFlash(__('Your user/password do not match.', true), 'default', array('class' => 'error'));
					
					//$this->redirect(array('action'=>'upgrade_login'));
					$this->redirect(array('action'=>'signup'));
				}
			}
    }
	
	function signup()
	{
		 $this->layout = 'default';
		 
	}	
	
	function upgrade_login()
	{
		 $this->layout = 'default';
		 
		 if(!empty($this->data))	 {		 
		 		
				
				if ($this->Auth->user('role_id') != null) {				
							
					$userdetail = $this->validateLogin();
					
					if(($this->Auth->User('role_id') == '2') && ($this->Auth->User('is_active') == '1')){ //For Store Manager 
						
						$this->Session->write('roleid','2');
						$this->Session->write('user_id',$userdetail['User']['id']);						
						$this->Session->write('rolename',$userdetail['User']['full_name']);
						
						$this->User->id = $this->Auth->user('id');
						$this->User->saveField('last_login', date('Y-m-d H:i:s'));
						if (isset($this->data['User']['remember_me']) && $this->data['User']['remember_me']) {
								$this->Cookie->write("User.username", $this->data['User']['username'], false, "30 Days");
							} elseif ($this->Cookie->read("User.username")) {
								$this->Cookie->delete("User.username");
							}
						$this->redirect(array('controller'=>'dashboards'));
					}
					else
					{
					
					}						
				
					
				}else{
					$this->Session->setFlash(__('Your user/password do not match.', true), 'default', array('class' => 'error'));
					//$this->redirect(array('action'=>'upgrade_login'));
				} 
		 
		 }
		 
	}	
	 
	
	 function validateLogin() {
		$user = $this->User->find(array('username' => $this->data['User']['username'],
                    'password' => $this->data['User']['password']),
                        array('id','role_id', 'full_name','username', 'is_active'));
        if (empty($user) == false)
            return $user;
        return false;
    }
    
    function logout() {
		$this->layout = 'login';  
		$this->Session->destroy('roleid');    
		//$this->Session->destroy('addtocartlogin');     
		$this->Auth->logout();
        $this->Session->setFlash('You\'ve successfully logged out.','message/fail');
		$this->redirect(array('controller' => 'users','action'=>'signup'));
    }
	
    function admin_logout() {
        $this->layout = 'login';  
		$this->Session->destroy('roleid');   
		$this->Session->destroy('params_id');     
		$this->Auth->logout();
        $this->Session->setFlash('You\'ve successfully logged out.','message/fail');
		$this->redirect(array('controller' => 'users','action'=>'login'));
    }
	
	function admin_activeinactive($id = null){
		$user_id = $this->params['pass'][0];		
		$option = $this->params['pass'][1];		
		$params = $this->params['pass'][2];			
		
		if($this->User->query("update users SET is_active = '$option'  WHERE `id` = '".$user_id."'")){
			$this->redirect(array('controller'=>'users','action'=>'index',$params));			
		}else{
			$this->redirect(array('controller'=>'users','action'=>'index',$params));
		}
	}	

	function admin_show_state($id = null){
		$country_id  = $id;
		
		$this->Country->recursive = 0;
		$where =     array(
		'conditions' => array('Country.parent_id' => $country_id),array('Country.level' => 1),array('Country.is_active' => 1),	
		'order' => array('Country.name DESC')		
		);
		$stateinformations = $this->Country->find('all',$where);		
		$this->set('stateinformation',$stateinformations);
		
	}
	
	function admin_show_city($id = null){
		$state_id  = $id;
		
		$this->Country->recursive = 0;
		$where =     array(
		'conditions' => array('Country.parent_id' => $state_id),array('Country.level' => 2),array('Country.is_active' => 1),	
		'order' => array('Country.name DESC')		
		);
		$cityinformations = $this->Country->find('all',$where);		
		$this->set('cityinformation',$cityinformations);
		
	}
	
	function changeuserinformation(){
			$oldpassword = $this->data['User']['password'];
			$clear_password = $this->data['User']['clear_password'];
			$user_id = $this->data['User']['id'];
			
			$where = array('User.id'=>$user_id,'AND'=>array('User.password'=>$oldpassword));	
			$userData = $this->User->find($where);
			
			if(!empty($userData) && !empty($clear_password)){
				$clearpassword =  $this->data['User']['clear_password'];;
				$this->data['User']['password'] = $this->data['User']['clear_password'];
				$this->data['User']['password'] = $this->Auth->hashPasswords($this->data);			
					if(!empty($this->data['User']['password'])){
						$newpassword =  $this->data['User']['password']['User']['password'];					
						if($this->User->query("UPDATE users SET password = '$newpassword', clear_password = '$clearpassword' WHERE id = '$user_id'")){
							$this->Session->setFlash(__('Your password has been changed', true));
							$this->redirect(array('controller'=>'dashboards','action'=>'index'));
					}else{
						$this->Session->setFlash(__('Your old password is not correct', true));
						$this->redirect(array('controller'=>'dashboards','action'=>'index'));
					}					
				}
								
			}									
			else if(empty($userData) && empty($clear_password)){
				$address1 = $this->data['User']['address1'];
				$address2 = $this->data['User']['address2'];
				$contact_no = $this->data['User']['contact_no'];
				$city = $this->data['User']['city'];
				$post_code = $this->data['User']['post_code'];			
				$country = $this->data['User']['country'];
				
					if($this->User->query("UPDATE users SET address1 = '$address1',address2 = '$address2',contact_no = '$contact_no',city = '$city',post_code = '$post_code',country = '$country' WHERE id = '$user_id'")){
						
						$this->Session->setFlash(__('Your billing address Address  has been changed', true));
						$this->redirect(array('controller'=>'dashboards','action'=>'index'));
					
					}else{
						$this->Session->setFlash(__('Your billing address Address  has not there is a problem', true));
						$this->redirect(array('controller'=>'dashboards','action'=>'index'));
					
					}
			}else{
						$this->Session->setFlash(__('Your old password is not correct', true));
						$this->redirect(array('controller'=>'dashboards','action'=>'index'));
			}
			
	}
	
	function admin_changepassword(){
		$this->layout = 'default_admin';
        if (!empty($this->data)) {

		   	$oldpassword = $this->data['User']['password'];
			$clear_password = $this->data['User']['clear_password'];
			$user_id = $this->data['User']['id'];
			
			$where = array('User.id'=>$user_id,'AND'=>array('User.password'=>$oldpassword));	
			$userData = $this->User->find($where);
			
			if(!empty($userData)){
				$clearpassword =  $this->data['User']['clear_password'];;
				$this->data['User']['password'] = $this->data['User']['clear_password'];
				$this->data['User']['password'] = $this->Auth->hashPasswords($this->data);			
					if(!empty($this->data['User']['password'])){
						$newpassword =  $this->data['User']['password']['User']['password'];					
						if($this->User->query("UPDATE users SET password = '$newpassword', clear_password = '$clearpassword' WHERE id = '$user_id'")){
							$this->Session->setFlash(__('Your password has been changed', true));
							$this->redirect(array('controller'=>'users','action'=>'changepassword'));
					}else{
						$this->Session->setFlash(__('Your old password is not correct', true));
						$this->redirect(array('controller'=>'users','action'=>'changepassword'));
					}					
				}
								
			}
		   
		    if ($this->User->save($this->data)) {
                $this->Session->setFlash(__('The Password has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
            }
        } 
		
		$id = $this->Auth->User('id');
		$this->set('users', $this->User->read(null, $id));      
       		
	}
	
	function useractivation()
	{
		if(empty($this->params))
		{
		    $this->Session->setFlash(__('Unauthorised Access'));
			$this->redirect('/');
		}
		if(sizeof($this->params['pass'] == 2))
		{
		  	$this->layout='default';

			$userid = $this->params['pass'][0];
			$activatilinkverifcationcode  = $this->params['pass'][1];

			
			$where = array('User.id'=> $userid);
			$userTable = $this->User->find($where);
			$usersverifcationcode = $userTable['User']['activation_key'];

			if($usersverifcationcode ==  $activatilinkverifcationcode)
			{

				
				if($this->User->updateAll(array('User.is_active' => '1'),array('User.id' => $userid)))
				{
					$this->set('flag','1');
					$this->Session->setFlash(__('Your registration has been sucessfully complete. now you can login ', true));
					//$this->redirect(array('action'=>'useractivation'));
					
				}
				else
				{
					$this->set('flag','0');
					$this->Session->setFlash(__('Your Activation Code Not Match, Please try again',true));
					//$this->redirect('/');
				}
				
			}
			else
			{
				
				$this->set('flag','0');
				$this->Session->setFlash(__('Your Activation Code Not Match, Please try again',true));
				//$this->redirect('/');
			}
		}
	}

function change_password(){
			$this->layout = 'default';
			
			
			if(!empty($this->data))
			{
			
			
			$user_id = $this->Session->read('user_id');			
			$oldpassword = $this->Auth->hashPasswords ($this->data['User']['password']);
			$newpassword = $this->Auth->hashPasswords($this->data['User']['newpassword']);
		   
			
			$where = array('User.id'=>$user_id,'AND'=>array('User.password'=>$oldpassword));	
			$userData = $this->User->find($where);
							
			if(!empty($userData)){						

				if($this->User->query("UPDATE users SET password = '$newpassword' WHERE id = '$user_id'")){
					$this->Session->setFlash(__('Your password has been changed', true));
					$this->redirect(array('controller'=>'dashboards','action'=>'change_password'));
				}else{
					$this->Session->setFlash(__('Your old password is not correct', true));
					$this->redirect(array('controller'=>'dashboards','action'=>'change_password'));
				}
			
			}else{
				$this->Session->setFlash(__('Your old password is not correct', true));
				$this->redirect(array('controller'=>'dashboards','action'=>'change_password'));
			}
			
		}
		
		}	
	
	
}

?>