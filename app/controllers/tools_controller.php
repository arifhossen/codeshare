<?php
class ToolsController extends AppController {

   var $name = 'Tools';  
   var $uses = array('Tool','ToolCategory');
   var $components = array('FileUpload','RequestHandler');

  function beforeFilter()
	{
	
		parent::beforeFilter();	
		$this->Auth->allow('*');	
		if(!empty($this->params['admin']))
			$this->layout = "inner_common_layout";
			
			
			
	}


    
	function index() {     
       
	  
	    $this->Tool->recursive = 2;
		
		/***************** Search By Keyword ***************/
		
		if(!empty($this->data))
		{
		
			$keyword = $this->data['Tool']['search_keyword'];			
			
			$post_ids_by_search = $this->search_posts($keyword);
			
		
			$this->paginate = array('conditions'=>array('Tool.id'=>$post_ids_by_search,'Tool.status'=>1),'limit'=>'3','order'=>'Tool.id DESC');		
			
			
		
		}else
		{
		
			if(isset($this->params['pass']['0']))
			{
			     $where_category_name = array('Category.name'=>$this->params['pass']['0']);
				 $category_info = $this->Category->find($where_category_name);
				 $category_id = $category_info['Category']['id'];				
				 $this->paginate = array('conditions'=>array('Tool.category_id'=>$category_id,'Tool.status'=>1),'limit'=>'3','order'=>'Tool.id DESC');		
			}
			else
			{
			
				 $this->paginate = array('conditions'=>array('Tool.status'=>1),'limit'=>'3','order'=>'Tool.id DESC'); 
			
			}
		
		
		
		}
		
		
		
		/************** Search By Keyword *****************/
		
        $this->set('tools', $this->paginate());
        
	}
	
	function search_posts($keyword)
	{
	
	  
	   $this->Tool->recursive = 0;
	   $whereInTitle = array('Tool.title LIKE'=>"%$keyword%",'Tool.status'=>1);
	   $postIdsByTitle = $this->Tool->find('list',array('conditions'=>$whereInTitle,'fields'=>'Tool.id'));
	   
	   $whereInDetails = array('Tool.details LIKE'=>"%$keyword%",'Tool.status'=>1);
	   $postIdsByDetails = $this->Tool->find('list',array('conditions'=>$whereInDetails,'fields'=>'Tool.id'));
	   
	   
	   $post_ids_list = array_merge($postIdsByTitle,$postIdsByDetails);
	  
	  return $post_ids_list;
	}
	
	function details($title = null) {         
	    
		
		$this->Tool->recursive = 2;
		$where = array('Tool.title LIKE'=>"%$title%");
		$posts = $this->Tool->find($where);
		$this->set('posts',$posts);        
	}
	
	function get_recent_posts() {        
	    	   
	   $postLists = $this->Tool->find('all',array('LIMIT'=>'10','order'=>'Tool.id DESC'));	  
	   return $postLists;		
        
	}
	
		
	
    function admin_index() {     
       
	    $header_title = 'Manage Tool';
	    $module_title = 'Tools';
	    $this->set(compact('header_title','module_title'));	
	   
	    $this->Tool->recursive = 2;
        $this->paginate = array('limit'=>'10','order'=>'Tool.id DESC'); 
        $this->set('tools', $this->paginate());
        
	}	

    function admin_view($id = null) {
       $this->layout='default_admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid content', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('content', $this->Tool->read(null, $id));
    }

    function admin_add() {
       
		$header_title = 'Add New Tool';
		$module_title = 'Tools';
		$categories= $this->ToolCategory->find('list');
		
		
		$this->set(compact('header_title','module_title','categories'));	
		
	   
        if (!empty($this->data)) {
		
		
		
		
		   	/*************** Upload Tools FIle ***************/
			
			if(!empty($this->data['Tool']['upload_source']['name']))
			{
			
				$target_path = WWW_ROOT.'files'.DS.'tools'.DS;
				$file_name = time().'_'.$this->data['Tool']['upload_source']['name'];
				$target_path = $target_path. $file_name;
				if(move_uploaded_file($this->data['Tool']['upload_source']['tmp_name'], $target_path)) 
				{
				   $this->data['Tool']['download_source'] = $file_name;
					
				}
                else
                {
					$this->data['Tool']['download_source'] = '';
                }				
			}
			else
			{
				$this->data['Tool']['download_source'] = '';
			}
			
			
			/************** End Upload Tools FILE **********/
			
		  
		
		
            $this->Tool->create();
           
            if ($this->Tool->save($this->data)) {
			
			  
			    
                $this->Session->setFlash(__('The Tool has been saved', true), 'message/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Tool could not be saved. Please, try again.', true));
            }
        }
       
    }

    function admin_edit($id = null) {
      
	  
	  	$header_title = 'Edit Tool';
		$module_title = 'Tools';
	    $categories= $this->ToolCategory->find('list');
		
		
		$this->set(compact('header_title','module_title','categories'));	
		
		
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Tool', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
           
           
		    
		   	/*************** Upload Tools FIle ***************/
			
			if(!empty($this->data['Tool']['upload_source']['name']))
			{
			
				$target_path = WWW_ROOT.'files'.DS.'tools'.DS;
				$file_name = time().'_'.$this->data['Tool']['upload_source']['name'];
				$target_path = $target_path. $file_name;
				if(move_uploaded_file($this->data['Tool']['upload_source']['tmp_name'], $target_path)) 
				{
				   $this->data['Tool']['download_source'] = $file_name;
					
				}
                else
                {
					//$this->data['Tool']['download_source'] = '';
                }				
			}
			else
			{
				//$this->data['Tool']['download_source'] = '';
			}
			
			
			/************** End Upload Tools FILE **********/
			
		   
		   
		   
            if ($this->Tool->save($this->data)) {
			
			
                $this->Session->setFlash(__('The tools has been saved', true), 'message/success');
                $this->redirect('index');
            } else {
                $this->Session->setFlash(__('The tools could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Tool->read(null, $id);		
			
        }
     
    }
	
	
	function download($id = null)
	{
	
	     
	    $this->autoRender = false;
		
	    $where  = array('Tool.id'=>$id);
		$tools = $this->Tool->find($where);
		
		
		if(!empty($tools))
		{
	   
		
			$file = 'files/tools/'.$tools['Tool']['download_source'];			
						
			if (file_exists($file)) {
			
		
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename='.basename($file));
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');
				header('Content-Length: ' . filesize($file));
				ob_clean();
				flush();
				readfile($file);
				exit;
			}
			
			
	    }
		
	
	
	}


    function admin_delete($id = null) {
        $this->layout='default_admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for content', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Tool->delete($id)) {
            $this->Session->setFlash(__('Tool deleted', true), 'message/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Tool was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }

    
	
	

}

?>