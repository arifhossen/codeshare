<?php

class PaypalComponent extends Object
{
    
    var $controller;
    
    /**
     * component statup
     */
    function startup(&$controller) {

        $this->controller=&$controller;
		
		//App::import('Vendor', 'PaypalApi', array('file' => 'paypal_api'.DS.'CallerService.php'));
		
		$this->paypal_config();	
			

    }//startup()
	
	
	function BasicIdea()
	{
	
		/* In normally paypal have sale and authorization transaction, 
		     many developer try to know the difference between the two transactions.

                 Authorization Transaction:

			When doing paypal transaction if set paymentaction is authorization,
			Then when doing transaction, amount has not credited in merchant account. 
			When checking transaction view payment status would be pending.
			Once merchant has authorized the transaction amount then the amount credited in to merchant account.
			Sample shot as �
			
			
			 Sale Transaction:

			When default the paypal use sale transaction. Paymentaction has only optional one�
			Once the transaction has completed the amount credited to merchant account.
			So there no need to authorize the amount.

			Order transaction:

			if set paymentaction is authorization then use the Authorization & Capture API to authorize and capture the payment payments.
			The Merchant Services on the PayPal website let you capture payments only for authorizations, not for orders.


       */
	
	}
	
	function paypal_config()
	{
	
	   define('API_USERNAME', 'arifho_1316614020_biz_api1.gmail.com');
	   define('API_PASSWORD', '1316614078');
	   define('API_SIGNATURE', 'Ae6GWKmYFxCqrJ7Ir9NhboDSLoPEADTmm8t3Hy3XLIn3D70h0fnvoW.u');
	   define('API_ENDPOINT', 'https://api-3t.sandbox.paypal.com/nvp');
       //define('API_ENDPOINT', 'https://api-3t.paypal.com/nvp');
	   
	   /**
		USE_PROXY: Set this variable to TRUE to route all the API requests through proxy.
		like define('USE_PROXY',TRUE);
		*/
		define('USE_PROXY',FALSE);
		/**
		PROXY_HOST: Set the host name or the IP address of proxy server.
		PROXY_PORT: Set proxy port.

		PROXY_HOST and PROXY_PORT will be read only if USE_PROXY is set to TRUE
		*/
		define('PROXY_HOST', '127.0.0.1');
		define('PROXY_PORT', '808');
		

		/* Define the PayPal URL. This is the URL that the buyer is
		first sent to to authorize payment with their paypal account
		change the URL depending if you are testing on the sandbox
		or going to the live PayPal site
		For the sandbox, the URL is
		https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=
		For the live site, the URL is
		https://www.paypal.com/webscr&cmd=_express-checkout&token=
		*/
		define('PAYPAL_URL', 'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=');
		//define('PAYPAL_URL', 'https://www.paypal.com/webscr&cmd=_express-checkout&token=');
	   
	   
		/**
		# Version: this is the API version in the request.
		# It is a mandatory parameter for each API request.
		# The only supported value at this time is 2.3
		*/

		define('VERSION', '57.0');	



	
	}
	
	function direct_payment_process($field_data_array)
	{
		/**
		* Get required parameters from the web form for the request
		*/
		$paymentType =urlencode( $field_data_array['paymentType']);
		$firstName =urlencode( $field_data_array['firstName']);
		$lastName =urlencode( $field_data_array['lastName']);
		$creditCardType =urlencode( $field_data_array['creditCardType']);
		$creditCardNumber = urlencode($field_data_array['creditCardNumber']);
		$expDateMonth =urlencode( $field_data_array['expDateMonth']);

		// Month must be padded with leading zero
		$padDateMonth = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);

		$expDateYear =urlencode( $field_data_array['expDateYear']);
		$cvv2Number = urlencode($field_data_array['cvv2Number']);
		$address1 = urlencode($field_data_array['address1']);
		$address2 = urlencode($field_data_array['address2']);
		$city = urlencode($field_data_array['city']);
		$state =urlencode( $field_data_array['state']);
		$zip = urlencode($field_data_array['zip']);
		$amount = urlencode($field_data_array['amount']);
		$aw = urlencode('789');
		//$currencyCode=urlencode($field_data_array['currency']);
		$currencyCode="USD";
		$paymentType=urlencode($field_data_array['paymentType']);

		/* Construct the request string that will be sent to PayPal.
		The variable $nvpstr contains all the variables and is a
		name value pair string with & as a delimiter */


		$auto_id = 4;
		$inv_id = 'INV'.'1234';  

		$lastName = $lastName.'_'.$inv_id;


		$nvpstr="&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".$padDateMonth.$expDateYear."&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName&STREET=$address1&CITY=$city&STATE=$state".
		"&ZIP=$zip&COUNTRYCODE=US&CURRENCYCODE=$currencyCode";
		
		

		/* Make the API call to PayPal, using API signature.
		The API response is stored in an associative array called $resArray */
		$resArray=$this->hash_call("doDirectPayment",$nvpstr);

		/* Display the API response back to the browser.
		If the response from PayPal was a success, display the response parameters'
		If the response was an error, display the errors received using APIError.php.
		*/
		$ack = strtoupper($resArray["ACK"]);

		if($ack!="SUCCESS")  {
		$_SESSION['reshash']=$resArray;
		$location = "APIError.php";
		 header("Location: $location");
		}
		
		
		return $resArray;
				
		
	
	}
	
	function expressCheckout_payment_process($field_data_array)
	{
	
				
		//$token = $_REQUEST['token'];
		if(! isset($token)) {

				/* The servername and serverport tells PayPal where the buyer
				   should be directed back to after authorizing payment.
				   In this case, its the local webserver that is running this script
				   Using the servername and serverport, the return URL is the first
				   portion of the URL that buyers will return to after authorizing payment
				   */
				   $serverName = $_SERVER['SERVER_NAME'];
				   $serverPort = $_SERVER['SERVER_PORT'];
				   $url=dirname('http://'.$serverName.':'.$serverPort.$_SERVER['REQUEST_URI']);

				   $paymentAmount=$field_data_array['paymentAmount'];
				   $currencyCodeType=$field_data_array['currencyCodeType'];
				   $paymentType=$field_data_array['paymentType'];
				 

				 /* The returnURL is the location where buyers return when a
					payment has been succesfully authorized.
					The cancelURL is the location buyers are sent to when they hit the
					cancel button during authorization of payment during the PayPal flow
					*/
				   
				   $returnURL =urlencode($url.'/ReviewOrder.php?currencyCodeType='.$currencyCodeType.'&paymentType='.$paymentType.'&paymentAmount='.$paymentAmount);
				   $cancelURL =urlencode("$url/SetExpressCheckout.php?paymentType=$paymentType" );

				 /* Construct the parameter string that describes the PayPal payment
					the varialbes were set in the web form, and the resulting string
					is stored in $nvpstr
					*/
				  
				   $nvpstr="&Amt=".$paymentAmount."&PAYMENTACTION=".$paymentType."&ReturnUrl=".$returnURL."&CANCELURL=".$cancelURL ."&CURRENCYCODE=".$currencyCodeType;

				 /* Make the call to PayPal to set the Express Checkout token
					If the API call succeded, then redirect the buyer to PayPal
					to begin to authorize payment.  If an error occured, show the
					resulting errors
					*/
				   $resArray=$this->hash_call("SetExpressCheckout",$nvpstr);
				   $_SESSION['reshash']=$resArray;
				   
				   

				   $ack = strtoupper($resArray["ACK"]);

				   if($ack=="SUCCESS"){
							// Redirect to paypal.com here
							$token = urldecode($resArray["TOKEN"]);
							$payPalURL = PAYPAL_URL.$token;
														
							header("Location: ".$payPalURL);
						  } else  {
							 //Redirecting to APIError.php to display errors. 
								$location = "APIError.php";
								header("Location: $location");
							}
		} else {
				 /* At this point, the buyer has completed in authorizing payment
					at PayPal.  The script will now call PayPal with the details
					of the authorization, incuding any shipping information of the
					buyer.  Remember, the authorization is not a completed transaction
					at this state - the buyer still needs an additional step to finalize
					the transaction
					*/

				   $token =urlencode( $_REQUEST['token']);

				 /* Build a second API request to PayPal, using the token as the
					ID to get the details on the payment authorization
					*/
				   $nvpstr="&TOKEN=".$token;

				 /* Make the API call and store the results in an array.  If the
					call was a success, show the authorization details, and provide
					an action to complete the payment.  If failed, show the error
					*/
				   $resArray=$this->hash_call("GetExpressCheckoutDetails",$nvpstr);
				   $_SESSION['reshash']=$resArray;
				   $ack = strtoupper($resArray["ACK"]);

				   if($ack=="SUCCESS"){			
							require_once "GetExpressCheckoutDetails.php";				 
					  } else  {
						//Redirecting to APIError.php to display errors. 
						$location = "APIError.php";
						header("Location: $location");
					  }
		}
		
		
		
		return $resArray;
	
	
	
	}
	
	
	
	function hash_call($methodName,$nvpStr)
   {
		//declaring of global variables
		//global $API_Endpoint,$version,$API_UserName,$API_Password,$API_Signature,$nvp_Header;
		
		
       
		//setting the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,API_ENDPOINT);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		//turning off the server and peer verification(TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POST, 1);
		//if USE_PROXY constant set to TRUE in Constants.php, then only proxy will be enabled.
	   //Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php 
		if(USE_PROXY)
		curl_setopt ($ch, CURLOPT_PROXY, PROXY_HOST.":".PROXY_PORT); 

		//NVPRequest for submitting to server
		$nvpreq="METHOD=".urlencode($methodName)."&VERSION=".urlencode(VERSION)."&PWD=".urlencode(API_PASSWORD)."&USER=".urlencode(API_USERNAME)."&SIGNATURE=".urlencode(API_SIGNATURE).$nvpStr;

		//setting the nvpreq as POST FIELD to curl
		curl_setopt($ch,CURLOPT_POSTFIELDS,$nvpreq);

		//getting response from server
		$response = curl_exec($ch);

		//convrting NVPResponse to an Associative Array
		$nvpResArray=$this->deformatNVP($response);
		$nvpReqArray=$this->deformatNVP($nvpreq);
		
	
		//$_SESSION['nvpReqArray']=$nvpReqArray;
		
		

		if (curl_errno($ch)) {
			// moving to display page to display curl errors
			  $_SESSION['curl_error_no']=curl_errno($ch) ;
			  $_SESSION['curl_error_msg']=curl_error($ch);
			  $location = "APIError.php";
			  header("Location: $location");
		 } else {
			 //closing the curl
				curl_close($ch);
		  }

	return $nvpResArray;
  }

/** This function will take NVPString and convert it to an Associative Array and it will decode the response.
  * It is usefull to search for a particular key and displaying arrays.
  * @nvpstr is NVPString.
  * @nvpArray is Associative Array.
  */

	function deformatNVP($nvpstr)
	{

		$intial=0;
		$nvpArray = array();

		while(strlen($nvpstr)){
			//postion of Key
			$keypos= strpos($nvpstr,'=');
			//position of value
			$valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);

			/*getting the Key and Value values and storing in a Associative Array*/
			$keyval=substr($nvpstr,$intial,$keypos);
			$valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
			//decoding the respose
			$nvpArray[urldecode($keyval)] =urldecode( $valval);
			$nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
		 }
		return $nvpArray;
	}
		
	
	    
	
} 

?>