<?php

class FileUploadComponent extends Object
{
    
    var $controller;
    
    /**
     * component statup
     */
    function startup(&$controller) {

        $this->controller=&$controller;

    }//startup()
    
	function upload($filename, $tmpname, $filesize, $folder="", $types="") {
	    $file_title = $filename;
	    //Get file extension
	    $ext_arr = split("\.",basename($file_title));
	    $ext = strtolower($ext_arr[count($ext_arr)-1]); //Get the last extension
	
	    //Not really uniqe - but for all practical reasons, it is
	    $uniqer = substr(md5(uniqid(rand(),1)),0,5);
	    $file_name = $uniqer . '_' . str_replace("'", "", str_replace(" ", "_",$file_title));//Get Unique Name
	
	    $all_types = explode(",",strtolower($types));
	    if($types) {
	        if(in_array($ext,$all_types));
	        else {
	            $result = "'".$filename."' is not a valid file."; //Show error if any.
	            //return array('',$result);
				return 0;
	        }
	    }
	
	    //Where the file must be uploaded to
	    if($folder) $folder .= '/';//Add a '/' at the end of the folder
	    $uploadfile = $folder . $file_name;
	
	    $result = '';
	    //Move the file from the stored location to the new location
	    if (!move_uploaded_file($tmpname, $uploadfile)) {
	        $result = "Cannot upload the file '".$filename."'"; //Show error if any.
	        if(!file_exists($folder)) {
	            $result .= " : Folder don't exist.";
	        } elseif(!is_writable($folder)) {
	            $result .= " : Folder not writable.";
	        } elseif(!is_writable($uploadfile)) {
	            $result .= " : File not writable.";
	        }
	        $file_name = '';
	        
	    } else {
	        if(!$filesize) { //Check if the file is made
	            @unlink($uploadfile);//Delete the Empty file
	            $file_name = '';
	            $result = "Empty file found - please use a valid file."; //Show the error message
	        } else {
	            chmod($uploadfile,0777);//Make it universally writable.
	        }
	    }
	
	    return $file_name;
	}
} 

?>