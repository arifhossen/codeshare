<?php

class AuthorizeDotNetComponent extends Object
{
    
    var $controller;
    
    /**
     * component statup
     */
    function startup(&$controller) {

        $this->controller=&$controller;
		
		App::import('Vendor', 'AnetPhpSdk', array('file' => 'anet_php_sdk'.DS.'AuthorizeNet.php'));
		
		$this->authorizenet_config();	
			

    }//startup()
	
	function authorizenet_config()
	{
	
	   /**** Below Access Information Get From my Test account ******/
	   //https://test.authorize.net/
	   //Username : and password get from arifhossen2010@gmail.com address.
	   
		define("METHOD_TO_USE","AIM");    // Add your API LOGIN ID
		define("AUTHORIZENET_API_LOGIN_ID","8q24p3QHWza");    // Add your API LOGIN ID
		define("AUTHORIZENET_TRANSACTION_KEY","8t26FRvn367eNv2S"); // Add your API transaction key
		define("AUTHORIZENET_SANDBOX",true);       // Set to false to test against production
		define("TEST_REQUEST", "FALSE");           // You may want to set to true if testing against production
		
		
		// $METHOD_TO_USE = "DIRECT_POST";         // Uncomment this line to test DPM
		// You only need to adjust the two variables below if testing DPM
		define("AUTHORIZENET_MD5_SETTING","");                // Add your MD5 Setting.
		$site_root = "http://bytecode-tech.com/authorize_dot_net_samples/your_store/"; // Add the URL to your site
		
		
		/*
		Difference between Authorize SIM and AIM
		
		The AIM module accepts the credit card info directly on your site 
		and then passes it securely to the gateway in the background. 
		It has a tighter handshaking between your store and the gateway service than SIM, 
		and typically has faster response times.
		
        SIM doesn't collect credit cards on your own site. It sends the customer to authorize.net's website to checkout on their secure page.		
		
		The main difference between SIM and AIM is that SIM redirects the user to a page on the Authorize.net server in order to collect the actual credit card number of the user. 
		With AIM no redirection is needed and the credit card number can be collected on the site.

         Read more: Difference Between SIM and AIM | 
		 Difference Between | SIM vs AIM http://www.differencebetween.net/technology/difference-between-sim-and-aim/#ixzz296lccbfw

		
		*/

	
	}
	
	function aim_trasaction_process($field_data_array)
	{
	    				
		if (METHOD_TO_USE == "AIM") {		
		  
			$transaction = new AuthorizeNetAIM;
			
			$transaction->setSandbox(AUTHORIZENET_SANDBOX);
			
			$transaction->setFields($field_data_array);
			
			$response = $transaction->authorizeAndCapture();
			
			return $response;
			
		
			/*
			if ($response->approved) {
				// Transaction approved! Do your logic here.
				header('Location: thank_you_page.php?transaction_id=' . $response->transaction_id);
			} else {
				header('Location: error_page.php?response_reason_code='.$response->response_reason_code.'&response_code='.$response->response_code.'&response_reason_text=' .$response->response_reason_text);
			}
			
			*/
		 
	    }
	
	}
	
	
	function refund_process($transaction_id)
	{
	
				/**
		* Demonstrates how to void a charge using the Authorize.Net SDK.
		*/
		$transaction = new AuthorizeNetAIM;
		$response = $transaction->void($transaction_id);
		
		pr($response);
		die();

		if ($response->approved)
		{
		  // Transaction approved! Do your logic here.
		  header('Location: refund_page.php?transaction_id=' . $response->transaction_id);
		}
		else
		{
		  header('Location: error_page.php?response_reason_code='.$response->response_reason_code.'&response_code='.$response->response_code.'&response_reason_text=' .$response->response_reason_text);
		}
			
	
	
	}
	
	function sim_trasaction_process()
	{
	
		$response = new AuthorizeNetSIM;
			if ($response->isAuthorizeNet()) {
			
			pr($response);
			die();
				if ($response->approved) {
					// Transaction approved! Do your logic here.
					// Redirect the user back to your site.
					$return_url = $site_root . 'thank_you_page.php?transaction_id=' .$response->transaction_id;
				} else {
					// There was a problem. Do your logic here.
					// Redirect the user back to your site.
					$return_url = $site_root . 'error_page.php?response_reason_code='.$response->response_reason_code.'&response_code='.$response->response_code.'&response_reason_text=' .$response->response_reason_text;
				}
				echo AuthorizeNetDPM::getRelayResponseSnippet($return_url);
			} else {
					echo "MD5 Hash failed. Check to make sure your MD5 Setting matches the one in config.php";	
				}
	
	}
	
	
	function getTransactionHistory()
	{
			
				

		
		// Get Settled Batch List
		$request = new AuthorizeNetTD;
		$response = $request->getSettledBatchList();
		echo count($response->xml->batchList->batch) . " batches\n";
		foreach ($response->xml->batchList->batch as $batch) {
			echo "Batch ID: " . $batch->batchId . "\n";
		}

		// Get Transaction Details
		$transactionId = "2177979100";
		$response = $request->getTransactionDetails($transactionId);
		echo $response->xml->transaction->transactionStatus;


			
	
	
	
	}
	
	
	
    
	
} 

?>