<?php

class FirstDataGlobalGatewayComponent extends Object
{
    
    var $controller;
    
    /**
     * component statup
     */
    function startup(&$controller) {

        $this->controller=&$controller;
		
		App::import('Vendor', 'FirstDataGlobalGatewayApi', array('file' => 'first_data_global_gateway_api'.DS.'transaction.php'));		
		$this->obj_api = new lphp;
		
		$this->config();	
			

    }//startup()
	
	
	
	function config()
	{
	
	   define('API_HOST', 'staging.linkpt.net');
	   define('API_PORT', '1129');
	   define('API_KEY_FILE', './1909781181.pem');
	   define('API_CONFIG_FILE', '1909781181');
	
	}
	
	function payment_process($field_data_array)
	{
		
				
		$myorder["host"]       =  API_HOST;	
		$myorder["port"]       = API_PORT;
		$myorder["keyfile"]    = API_KEY_FILE; 
		$myorder["configfile"] =  API_CONFIG_FILE;		
		

		/*************** Card Information ***********************/
		$myorder["ordertype"]  = $field_data_array["order_type"];
		$myorder["cardnumber"]    = $field_data_array['card_number'];       
		$myorder["cardexpmonth"]  = $field_data_array['month'];
		$myorder["cardexpyear"]   = $field_data_array['year'];
		$myorder["subtotal"]      =  $field_data_array['sub_total'];
		$myorder["chargetotal"]   = $field_data_array['order_total'];
		$myorder["tax"]   = $field_data_array['total_tax'].'%';
		
		
		/********************* User Information ***********************/	
		//$myorder["userid"]  = $field_data_array['user_id'];
		$myorder["name"]   = $field_data_array['full_name'];
		$myorder["address1"]   = $field_data_array['billing_address'];
		$myorder["city"]     = $field_data_array['city'];	
		$myorder["email"]     = $field_data_array['email'];
		
		
		$result = $this->obj_api->curl_process($myorder);  # use curl methods
		
		return $result;
				
		
	
	}
		    
	
} 

?>