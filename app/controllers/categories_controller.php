<?php
class CategoriesController extends AppController {

    var $name = 'Categories';  
	var $uses = array('Category');
   var $components = array('FileUpload','RequestHandler');

  function beforeFilter()
	{
		parent::beforeFilter();		
		
		$this->Auth->allow('*');
		if(!empty($this->params['admin']))
			$this->layout = "inner_common_layout";
	}
  

	
    function admin_index() {     
       
	    $header_title = 'Manage Category';
	    $module_title = 'Category';
	    $this->set(compact('header_title','module_title'));	
	   
	    $this->Category->recursive = 0;
        $this->paginate = array('limit'=>'10','order'=>'Category.id DESC'); 
        $this->set('categories', $this->paginate());
        
	}	



    function admin_add() {
       
		$header_title = 'Add New Category';
		$module_title = 'Categorys';
		$categories = $this->Category->find('list');	
		
		$this->set(compact('header_title','module_title','categories'));	
		
	   
        if (!empty($this->data)) {
            $this->Category->create();
           
            if ($this->Category->save($this->data)) {
                $this->Session->setFlash(__('The Category has been saved', true), 'message/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Category could not be saved. Please, try again.', true));
            }
        }
       
    }

    function admin_edit($id = null) {
      
	  
	  	$header_title = 'Edit Category';
		$module_title = 'Categorys';
		
		
		$this->set(compact('header_title','module_title','categories'));	
	  
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid category', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
           
           
            if ($this->Category->save($this->data)) {
                $this->Session->setFlash(__('The category has been saved', true), 'message/success');
                $this->redirect('index');
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Category->read(null, $id);
        }
     
    }

    function admin_delete($id = null) {
        $this->layout='default_admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for content', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Category->delete($id)) {
            $this->Session->setFlash(__('Category deleted', true), 'message/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Category was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }
	
	
	function getCategoryList()
	{	
		return $this->Category->find('all');
	
	}

    
	
	

}

?>