<?php
class ContentsController extends AppController {

    var $name = 'Contents';  
	var $uses = array('Content');
   var $components = array('FileUpload','RequestHandler');

  function beforeFilter()
	{
		parent::beforeFilter();	
		
		$this->Auth->allow('*');
			
		if(!empty($this->params['admin']))
			$this->layout = "inner_common_layout";
	}
	
	
	
	function index() {    
       
	    $indentifier= $this->params['pass'][0];
		$where = array('Content.page_name'=>$indentifier);
		$contents = $this->Content->find($where);		
		$this->set('contents',$contents);
	}	
	
	
	
	function jquery()
	{
	
	
	
	}


    function admin_index() {     
       
	    $header_title = 'Manage Content';
	    $module_title = 'Contents';
	    $this->set(compact('header_title','module_title'));	
	   
	    $this->Content->recursive = 0;
        $this->paginate = array('limit'=>'10'); 
        $this->set('contents', $this->paginate());
        
	}	

    function admin_view($id = null) {
       $this->layout='default_admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid content', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('content', $this->Content->read(null, $id));
    }

    function admin_add() {
       
		$header_title = 'Add New Content';
		$module_title = 'Contents';
		$this->set(compact('header_title','module_title'));	
		
	   
        if (!empty($this->data)) {
            $this->Content->create();
           
            if ($this->Content->save($this->data)) {
                $this->Session->setFlash(__('The Content has been saved', true), 'message/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Content could not be saved. Please, try again.', true));
            }
        }
       
    }

    function admin_edit($id = null) {
      
	  
	  	$header_title = 'Edit Content';
		$module_title = 'Contents';
	
		
		$this->set(compact('header_title','module_title'));	
	  
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid post', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
           
           
            if ($this->Content->save($this->data)) {
                $this->Session->setFlash(__('The Content has been saved', true), 'message/success');
                $this->redirect('index');
            } else {
                $this->Session->setFlash(__('The Content could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Content->read(null, $id);
        }
     
    }

    function admin_delete($id = null) {
        $this->layout='default_admin';
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for content', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Content->delete($id)) {
            $this->Session->setFlash(__('Content deleted', true), 'message/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Content was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }

    
	
	

}

?>