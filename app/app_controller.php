<?php
class AppController extends Controller {
    var $components = array(
        'Auth' => array(
            'autoRedirect' => false,
        ),
        'Session',
		'Cookie',
    );
    var $helpers = array(
        'Html',
        'Form',
        'Session',		
		'Text',
		'Javascript',
		'Fck',
    );
	
	var $uses = array('Role');
	

	
	
	function beforeFilter(){	
	
		
		$this->Auth->allow('index','view');
		$this->Auth->authError = 'Please login to view that page.';
		$this->Auth->loginError = 'Incorrect username/password combination.';
		$this->Auth->logoutRedirect = array('controller'=>'users','action'=>'login');		
		$this->Cookie->name = 'User.username';		
		
		
	}	
	
	
		
	function afterFilter() {
        # Update User last_access datetime
        if ($this->Auth->user()) {
            $this->loadModel('User');
            $this->User->id = $this->Auth->user('id');
            $this->User->saveField('last_access', date('Y-m-d H:i:s'));
        }
    }
	

		
	
}
?>
