<?php echo $javascript->link('/js/tiny_mce/tiny_mce.js'); ?>
<script type="text/javascript">
	tinyMCE.init({
		theme : "advanced",
		mode : "textareas",
		convert_urls : false
	});
</script>



<div class="action_title"><span>Add New Design Code</span></div>
<?php echo $this->Form->create('DesignCode', array('id'=>'addDesignCode','enctype'=>"multipart/form-data")); ?>
<div class="tabitem">
	<div class="common_from-product2">	
		
		 
		 
		 
		 
		 <div class="element">
            <div class="title">Select Design</div>
			 <div class="value">
				<select name="data[DesignCode][design_id]" >
					<?php
					foreach($designLists AS $key=>$value):
					
						$id = $value['Design']['id'];
						$name = $value['Design']['design_name'];
					?>					
					<option value="<?php echo $id; ?>"><?php echo $name; ?></option>
					<?php
					endforeach;
					?>
				</select>
            </div>
        </div>
		
		 
		
		 
		
		 <div class="element">
            <div class="title">Design Code</div>
            <div class="value">
            <?php echo $this->Form->input('design_code',array('id'=>'design_code','label'=>false));?>
            </div>
        </div>
		
		
		  <div class="element">
            <div class="title">Features &  Description:</div>
            <div class="value">
			<?php echo $form->input('DesignCode.features',array('label'=>false,'div'=>false,'type'=>'textarea'));?>
            </div>
        </div>

		<div class="element">
			<div class="title">Design Image:</div>
			<div class="value">       
			<?php echo $this->Form->input("DesignCode.image",array('label'=>false,'div'=>false,'type'=>'file'));?>
			</div>
		</div>
		
		 <div class="element">
            <div class="title">Demo URL</div>
            <div class="value">
            <?php echo $this->Form->input('demo_url',array('id'=>'demo_url','label'=>false));?>
            </div>
        </div>
		
		
	
		
		
	</div>
		
   </div>
</div>
<div class="button-container">
    <button value="signin" name="btnAction" type="submit"><span>Submit</span></button>
    or <?php echo $this->Html->link(__('Back to list', true),array('action' => 'index')); ?>
</div>