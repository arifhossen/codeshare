<div class="action_title"><span style="font-size:18px; font-weight:bold;">Edit Payment Gateway Information</span></div>
<br />
<?php echo $this->Form->create('PaymentGateway', array('controller'=>'payment_gateways','action'=>'edit')); ?>
 <?php echo $this->Form->input('id',array('id'=>'demo_url','label'=>false));
 
 	  $active = '';
	  $inactive = '';
	  
	   if($this->data['PaymentGateway']['active'] == 1)
	   {
	   		$active = 'checked="checked"';
	   }
	   
	   
	  if($this->data['PaymentGateway']['active'] == 0)	  
	   {
	   		$inactive = 'checked="checked"';
	   
	   }
 
 ?>
 
 
 <table width="100%" style="font-weight:bold; font-size:14px;">
 
 	<tr>
		<td> Mode </td> <td> <?php echo $this->Form->input('mode',array('label'=>false));?> </td> 
	</tr>
	
	
	<tr>
		<td> Host </td> <td> <?php echo $this->Form->input('host',array('label'=>false));?></td> 
	</tr>	
	
	<tr>
		<td> Port </td> <td> <?php echo $this->Form->input('port',array('label'=>false));?></td> 
	</tr>	
	
	
	<tr>
		<td> Key File </td> <td> <?php echo $this->Form->input('keyfile',array('label'=>false));?> </td> 
	</tr>	
	
	
	<tr>
		<td> Config File </td> <td> <?php echo $this->Form->input('configfile',array('label'=>false));?> </td> 
	</tr>	
	
	<tr>
		<td> Order Type  </td> <td> <?php echo $this->Form->input('order_type',array('label'=>false));?> </td> 
	</tr>
	
	
	<tr>
		<td> Description  </td> <td> <?php echo $this->Form->input('account_information',array('label'=>false));?> </td> 
	</tr>
	
	<tr>
		<td> Status  </td> <td> Active <input type="radio" <?php echo $active; ?>  name="data[PaymentGateway][active]" value="1" id="active" />  Inactive <input type="radio" name="data[PaymentGateway][active]" <?php echo $inactive; ?>  value="0" id="active" /> </td> 
	</tr>
	
		
	<tr>
		<td style="height:30px;">   </td> <td>  </td> 
	</tr>
	
	
		
	<tr>
		<td>   </td> <td>  <button value="signin" name="btnAction" type="submit"><span>Update</span></button> </td> 
	</tr>
	
	
	
	
	
</table>	


<div class="button-container">
   
	<br />
    or <?php echo $this->Html->link(__('Back to list', true),array('action' => 'index')); ?>
</div>

