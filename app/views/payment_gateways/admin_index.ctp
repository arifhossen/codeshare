
	<script>
	$(document).ready(function () 
	{
		$("#grid").wijgrid({
		allowSorting: true,
		allowPaging: true,
		pageSize:10,
		});

	});

	</script>

	<div style="padding-left:10px; width:750px;">
	<h1>Payment Gateway Configuration : First Data Gateway </h1>

		<table id="grid" width="100%" style="font-size:16px; font-weight:bold;">
			<thead>
				<tr>
					<th style="width:10px;">
					SL No.
					</th>
					<th width="70%">
					Transaction Mode
					</th>
					
					<th width="70%">
					Order Type
					</th>
					
					<th width="70%">
					Status
					</th>
					
					<th width="20%">
					Action 
					</th>
				</tr>
			</thead>

			<tbody>
				<?php 
				
				if(!empty($paymentGatewayInfo))
				{
			
				$serial_no = 1;
				foreach($paymentGatewayInfo as $data)
				   { ?>
					<tr>
						<th>
							<?php echo $serial_no++ ; ?>
						</th>
						
						<td><?php  echo $data['PaymentGateway']['mode'];  ?></td>
						
						<td> <?php echo $data['PaymentGateway']['order_type']; ?></td>
						
					
						
						
						<td> <?php 
							if($data['PaymentGateway']['active'] == 1)
							{
								echo "<p style='color:green; font-weight:bold; font-size:16px;'>Active</p>";
							
							}
							else
							{
							
								echo "<p style='color:red; font-weight:bold; font-size:16px;'>In Active</p>";
							}
						?>	</td>
						       
						        
								<td>
								
								 
<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $data['PaymentGateway']['id'])); ?> | 



            </td>
								
								
									
					</tr>

				<?php 
				  }//EO FOR EACH
				}//EO NOT EMPTY ?>
			</tbody>
		</table>
	</div>















