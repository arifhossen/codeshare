


    <h1>Advanced Integration Module(AIM)</h1>
	
	<?php
	
		if(isset($transaction_result))
		{
		
		    echo "<p style='color:green; font-weight:bold'>";
			echo "Trasaction Success<br>";
			echo "Transaction ID:".$transaction_result->transaction_id;
			echo "</p>";
		
		}
	
	
	
	
	
	?>
	
	
	 <?php echo $this->Form->create('PaymentGateway', array('action'=>'authorize_net_trasaction','enctype'=>"multipart/form-data")); ?>
	 
      <fieldset>
	  
	     <div>
          <label>Amount</label>
          <input type="text" class="text required" size="15" name="data[PaymentGateway][x_amount]" value="<?php echo $amount=20; ?>"></input>
        </div>
        <div>
          <label>Credit Card Number</label>
          <input type="text" class="text required creditcard" size="15" name="data[PaymentGateway][x_card_num]" value="6011000000000012"></input>
        </div>
        <div>
          <label>Exp.</label>
          <input type="text" class="text required" size="4" name="data[PaymentGateway][x_exp_date]" value="04/15"></input>
        </div>
        <div>
          <label>CCV</label>
          <input type="text" class="text required" size="4" name="data[PaymentGateway][x_card_code]" value="782"></input>
        </div>
      </fieldset>
      <fieldset>
        <div>
          <label>First Name</label>
          <input type="text" class="text required" size="15" name="data[PaymentGateway][x_first_name]" value="John"></input>
        </div>
        <div>
          <label>Last Name</label>
          <input type="text" class="text required" size="14" name="data[PaymentGateway][x_last_name]" value="Doe"></input>
        </div>
      </fieldset>
      <fieldset>
        <div>
          <label>Address</label>
          <input type="text" class="text required" size="26" name="data[PaymentGateway][x_address]" value="123 Four Street"></input>
        </div>
        <div>
          <label>City</label>
          <input type="text" class="text required" size="15" name="data[PaymentGateway][x_city]" value="San Francisco"></input>
        </div>
      </fieldset>
      <fieldset>
        <div>
          <label>State</label>
          <input type="text" class="text required" size="4" name="data[PaymentGateway][x_state]" value="CA"></input>
        </div>
        <div>
          <label>Zip Code</label>
          <input type="text" class="text required" size="9" name="data[PaymentGateway][x_zip]" value="94133"></input>
        </div>
        <div>
          <label>Country</label>
          <input type="text" class="text required" size="22" name="data[PaymentGateway][x_country]" value="US"></input>
        </div>
      </fieldset>
      <input type="submit" value="BUY" class="submit buy">
    </form>
	
	
	
	<hr>
	
	<br><br>
							
	<h1>Simple Integration Module(SIM)</h1>						
							

    
<!-- This section generates the "Submit Payment" button using PHP           -->
<?php
// This sample code requires the mhash library for PHP versions older than
// 5.1.2 - http://hmhash.sourceforge.net/
	
// the parameters for the payment can be configured here
// the API Login ID and Transaction Key must be replaced with valid values
$loginID		= "8q24p3QHWza";
$transactionKey = "8t26FRvn367eNv2S";
$amount 		= "19.99";
$description 	= "Sample Transaction";
$label 			= "Submit Payment"; // The is the label on the 'submit' button
$testMode		= "false";
// By default, this sample code is designed to post to our test server for
// developer accounts: https://test.authorize.net/gateway/transact.dll
// for real accounts (even in test mode), please make sure that you are
// posting to: https://secure.authorize.net/gateway/transact.dll
$url			= "https://test.authorize.net/gateway/transact.dll";

// If an amount or description were posted to this page, the defaults are overidden
if (array_key_exists("amount",$_REQUEST))
	{ $amount = $_REQUEST["amount"]; }
if (array_key_exists("amount",$_REQUEST))
	{ $description = $_REQUEST["description"]; }

// an invoice is generated using the date and time
$invoice	= date('YmdHis');
// a sequence number is randomly generated
$sequence	= rand(1, 1000);
// a timestamp is generated
$timeStamp	= time();

// The following lines generate the SIM fingerprint.  PHP versions 5.1.2 and
// newer have the necessary hmac function built in.  For older versions, it
// will try to use the mhash library.
if( phpversion() >= '5.1.2' )
	{ $fingerprint = hash_hmac("md5", $loginID . "^" . $sequence . "^" . $timeStamp . "^" . $amount . "^", $transactionKey); }
else 
	{ $fingerprint = bin2hex(mhash(MHASH_MD5, $loginID . "^" . $sequence . "^" . $timeStamp . "^" . $amount . "^", $transactionKey)); }
?>

<!-- Print the Amount and Description to the screen. -->
Amount: <?php echo $amount; ?> <br />
Description: <?php echo $description; ?> <br />

<!-- Create the HTML form containing necessary SIM post values -->
<form method='post' action='<?php echo $url; ?>' >
<!--  Additional fields can be added here as outlined in the SIM integration
 guide at: http://developer.authorize.net -->
	<input type='hidden' name='x_login' value='<?php echo $loginID; ?>' />
	<input type='hidden' name='x_amount' value='<?php echo $amount; ?>' />
	<input type='hidden' name='x_description' value='<?php echo $description; ?>' />
	<input type='hidden' name='x_invoice_num' value='<?php echo $invoice; ?>' />
	<input type='hidden' name='x_fp_sequence' value='<?php echo $sequence; ?>' />
	<input type='hidden' name='x_fp_timestamp' value='<?php echo $timeStamp; ?>' />
	<input type='hidden' name='x_fp_hash' value='<?php echo $fingerprint; ?>' />
	<input type='hidden' name='x_test_request' value='<?php echo $testMode; ?>' />
	<input type='hidden' name='x_show_form' value='PAYMENT_FORM' />
	<input type='submit' value='<?php echo $label; ?>' />
</form>
							
						