<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="google-site-verification" content="UanJJd6Lv4Inclv3QOXsrhIhn8hNZVIG-RXutOC5xqw" />
<meta name="description" content="Code Share">

<title><?php __('The Code Share ');?><?php echo $title_for_layout; ?> 

</title>



<?php	
echo $this->Html->charset();
echo $this->Html->meta('icon');
echo $this->Html->css('global');
echo $this->Html->css('jquery_menu_css');
echo $this->Html->script('jExpand');
echo $this->Html->script('jquery-1.3.1.min');
echo $this->Html->script('jquery.dropdownPlain');	
echo $scripts_for_layout;
?>
	
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37394247-1']);
  _gaq.push(['_setDomainName', 'codeshare.info']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>	

</head>

<body>
	
	
	
	<div id="container">
		<?php //echo $this->element('layout/header'); ?>
		
		<div id='smartMiddleContent'>


		<div id="header">
			<?php echo $this->element('header'); ?>					
		</div>	
	
			<div id='mainContentArea'>
			
			
		   <div id="left-content">
			
			<?php echo $this->Session->flash(); ?>
			<?php echo $content_for_layout; ?>
			</div>
			
			<div id="right-sidebar">
				
				<?php echo $this->element('layout/sidebar1'); ?>			
			
			</div>
			
			
			
			<br>
			
			
			
			</div>	
			
		
	
			<div id='footer'>
			
			   <?php echo $this->element('layout/footer-menu'); ?>
			
			</div>

        </div>

		
			
		
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
