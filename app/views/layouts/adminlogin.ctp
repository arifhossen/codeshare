﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>SMARTACTIC LOGIN

</title>

<?php	
/*All style sheet included here*/ 
echo $this->Html->charset();
echo $this->Html->meta('icon');
echo $this->Html->css('global');

/*All javascript included here*/ 

echo $scripts_for_layout;
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-13197635-4']);
  _gaq.push(['_setDomainName', 'none']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
    <body>
       <div id="container">
		
		<!-- header start here -->
		<?php echo $this->element('layout/header'); ?>
		
		
		<div id='smartMiddleContent'>

			<div id='smartHeaderLogo'>
			
				<?php
				
				    $logoImg = 'images/layouts/Smartactic_Logo.png';
					echo $logo = '<img src="'.$this->webroot.$logoImg.'" alt="Smartactic Logo" width="350" height="75" />';	
				
				?>
			
			
			</div>
	
			<div id='smartTopMenu'>
			
				<?php echo $this->element('layout/top-menu'); ?>
		
			
			</div>	
	
			<div id='content-container'>
			
			<?php echo $this->Session->flash(); ?>
			<?php echo $content_for_layout; ?>
			
			<br>
			
			
			<br><br>
			</div>	
	
			<div id='smartFooter'>
			
			   <?php echo $this->element('layout/footer-menu'); ?>
			
			</div>

        </div>
			
	</div>
    </body>
</html>