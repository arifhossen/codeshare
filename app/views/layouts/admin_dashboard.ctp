<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="imagetoolbar" content="no" />
<title>Admin Dashboard</title>

<?php	

    $template_name = $this->requestAction('settings/getCurrentTemplateName');
	echo $this->Html->meta('icon'); 
	echo $this->Html->css('admin_panel/admin'); 
	echo $this->Html->css('admin_panel/admin-'.$template_name); 
	echo $this->Html->script('admin_panel/behaviour'); 
?>


<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/admin-ie.css" /><![endif]-->
<!-- aurora-theme is default -->






</head>

<body>

	<!--[if !IE]>start wrapper<![endif]-->
	<div id="wrapper">
		
		<!--[if !IE]>start header main menu<![endif]-->
		
		<div id="header_main_menu">
		
		<span id="header_main_menu_bg"></span>
		<!--[if !IE]>start header<![endif]-->
		<div id="header">
			<div class="inner">
				<h1 id="logo"><a href="#">websitename <span>Administration Panel</span></a></h1>
				
				<!--[if !IE]>start user details<![endif]-->
				<div id="user_details">
					<ul id="user_details_menu">
						<li class="welcome">Welcome <strong>Administrator</strong></li>
						
						<li>
							<ul id="user_access">
								<li class="first"><a href="#">My account</a></li>
								<li class="last"><a href="<?php echo $this->webroot;?>admin/users/logout">Log out</a></li>
							</ul>
						</li>
						
						
					</ul>
					
					<div id="server_details">
						<dl>
							<dt>Date :</dt>
							<dd> <?php echo date('d/m/Y'); ?></dd>
						</dl>
						<dl>
							<dt>Last login ip :</dt>
							<dd>192.168.0.25</dd>
						</dl>
					</div>
					
				</div>
				
				<!--[if !IE]>end user details<![endif]-->
			</div>
		</div>
		<!--[if !IE]>end header<![endif]-->
		
		<!--[if !IE]>start main menu<![endif]-->
		<div id="main_menu">
			<div class="inner">
			<ul>
				<li>
					<a href="<?php echo $this->webroot; ?>admin/users/dashboard" class="selected_lk"><span class="l"><span></span></span><span class="m"><em>Dashboard</em><span></span></span><span class="r"><span></span></span></a>
					
				</li>
				<li>
					<a href="<?php echo $this->webroot.'admin/users'; ?>"><span class="l"><span></span></span><span class="m"><em>Users</em><span></span></span><span class="r"><span></span></span></a>
				</li>
				<li><a href="<?php echo $this->webroot.'admin/settings/payment_gateway_integration'; ?>"><span class="l"><span></span></span><span class="m"><em>Manage Payment Gateway</em><span></span></span><span class="r"><span></span></span></a></li>
				
				<li><a href="<?php echo $this->webroot.'admin/settings/manage_template'; ?>"><span class="l"><span></span></span><span class="m"><em>Admin Templates</em><span></span></span><span class="r"><span></span></span></a></li>
				
				
				</ul>
			</div>
			<span class="sub_bg"></span>
		</div>
		<!--[if !IE]>end main menu<![endif]-->
		
		</div>
		
		<!--[if !IE]>end header main menu<![endif]-->
		
		
		
		
		<!--[if !IE]>start content<![endif]-->
		<div id="content">
			<div class="inner">
			
			  <?php echo $this->Session->flash(); ?>
			  <?php echo $content_for_layout;?>
			
								
			</div>
		</div>
		<!--[if !IE]>end content<![endif]-->
		
		
	</div>
	<!--[if !IE]>end wrapper<![endif]-->
	
	<!--[if !IE]>start footer<![endif]-->
	<div id="footer">
		<div id="footer_inner">
			<div class="inner">
				
				<div id="footer_info">
					Design and Developed by : Mohammad Arif Hossen
				</div>
				
				
				<ul id="footer_menu">
					<li class="first"><a href="#">Help</a></li>
					<li><a href="#">Contact Support</a></li>
				</ul>	
				
				
				
			</div>
		</div>
	</div>
	<!--[if !IE]>end footer<![endif]-->

</body>
</html>
