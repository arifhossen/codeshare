<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="imagetoolbar" content="no" />
<title>Administrator Login</title>


<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/login-ie.css" /><![endif]-->
<!-- aurora-theme is default -->



<?php	
	echo $this->Html->meta('icon'); 
	echo $this->Html->css('admin_panel/login.css'); 
	echo $this->Html->css('admin_panel/login-blue'); 
	
?>

</head>

<body>
	<!--[if !IE]>start wrapper<![endif]-->
	<div id="wrapper">
	<div id="wrapper2">
	<div id="wrapper3">
	<div id="wrapper4">
	<span id="login_wrapper_bg"></span>
	
	<div id="stripes">
	
		
		
		
		
		<!--[if !IE]>start login wrapper<![endif]-->
		<div id="login_wrapper">
			
			
			<?php
			if(isset($error_message))
			{
			
			?>
			<div class="error">
				<div class="error_inner">
					<strong>Access Denied</strong> | 
					<span><?php 
					
					
						echo $error_message;
					
					If ($this->Session->check('Message.flash'))
					{
						
									echo $this->Session->flash();
					}				
						?>
		            </span>
				</div>
			</div>
			
			<?php } ?>

			
			
			
			<!--[if !IE]>start login<![endif]-->
			<?php echo $this->Form->create('User', array('action' => 'login')); ?>
	
	
				<fieldset>
					
					
					
					
					
					<h1>Please log in</h1>
					<div class="formular">
						<span class="formular_top"></span>
						
						<div class="formular_inner">
						
						<label>
							<strong>Username:</strong>
							
							<span class='input_wrapper'>
							<?php echo $this->Form->input('username', array('div' => false, 'label' => false)); ?>
                            </span>						
							
						</label>
						
						<label>
							<strong>Password:</strong>
							<span class="input_wrapper">
									<?php echo $this->Form->password('password', array('div' => false, 'label' => false)); ?>	

							</span>
						</label>
						<label class="inline">
							 <input type="checkbox" name="data[User][remember_me]" value="1" <?php if (isset($checked))
        echo 'checked="checked"'; ?> />
							remember me on this computer
						</label>
						
						
						<ul class="form_menu">
							<li><span class="button"><span><span><em>SUBMIT</em></span></span><input type="submit" name=""/></span></li>
							<li><span class="button"><span><span><a href="#">BACK TO SITE</a></span></span></span></li>
						</ul>
						
						</div>
						
						<span class="formular_bottom"></span>
						
					</div>
				</fieldset>
			</form>
			<!--[if !IE]>end login<![endif]-->
			
			<!--[if !IE]>start reflect<![endif]-->
			<span class="reflect"></span>
			<span class="lock"></span>
			<!--[if !IE]>end reflect<![endif]-->
			
			
		</div>

		<!--[if !IE]>end login wrapper<![endif]-->
	    </div>
		</div>
     	</div>
		</div>	
	</div>
	<!--[if !IE]>end wrapper<![endif]-->
</body>
</html>
