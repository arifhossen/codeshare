<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>PRP- Police Reform Programme </title>
  <?php
	echo $this->Html->meta('icon');
	echo $this->Html->css(array('prp', 'ui-lightness/jquery-ui-1.7.3.custom','ui-lightness/jquery.wijmo-complete.0.8.2','ui-lightness/jquery-ui-1.8.4.custom','jquery.wijmo-complete.0.8.2','jquery.wijmo-open.0.8.2'));
	echo $scripts_for_layout;
	echo $this->Html->script(array('jquery-1.3.2.min', 'jquery-ui-1.7.3.custom.min', 'jquery.form', 'jquery.validate', 'jquery.validate', 'additional-methods','jquery-1.4.3.min.js','jquery-ui-1.8.6.custom.min.js','jquery.glob.min.js','jquery.wijmo-open.0.8.2.min.js','jquery.wijmo-complete.0.8.2.min.js','lib/Wijmo-Open/development-bundle/external/jquery-1.4.3.min','lib/Wijmo-Open/development-bundle/external/jquery-ui-1.8.6.custom.min','lib/Wijmo-Open/development-bundle/external/jquery.bgiframe-2.1.3-pre','lib/Wijmo-Open/development-bundle/external/jquery.glob.min','Wijmo-Complete/development-bundle/external/jquery.mousewheel.min','lib/Wijmo-Complete/development-bundle/external/raphael','lib/Wijmo-Complete/development-bundle/external/raphael-popup','lib/Wijmo-Open/js/jquery.wijmo-open.0.8.2.min','lib/Wijmo-Complete/js/jquery.wijmo-complete.0.8.2.min'));
	
	?>
    
	  <script type="text/javascript">
		$(document).ready(function () {
			/*$("#accordion").accordion({ 
			header: "h3"
			
			
			 });*/
			 
			 
			
		});
  
		
</script>
	
</head>
<body>
	<div id="body_wrap">
		<div id="adminbar">
		<p id="top_line">&nbsp;</p>
			<span class="pad_10">You're signed in as <?php  echo $user['first_name'] .'&nbsp'. $user['last_name'] ?></span>
			<span class="float_right">
				<ul id="menu">
					<li><?php echo $this->Html->link('Admin Dashboard', array('controller'=>'users', 'action'=>'dashboard'));?></li>
					<li><?php echo $this->Html->link('Sign Out', array('controller'=>'users', 'action'=>'logout'));?></li>
			</span>
			
		</div>
    	<div id="top_dash">
        </div>
        <div id="header_dash">
            <?php echo $this->element('header'); ?>
        </div>
        <div id="top_dash">
        </div>
        <div id="content">
        	<div id="leftbar">
            	<div class="align_left" id="accordion">
                   <?php echo $this->element('leftbar'); ?>     
                </div>
            </div>
            <div id="content_body">
            	
					<?php echo $this->Session->flash(); ?>
					<?php echo $content_for_layout; ?>
                
            </div>
        </div>
        <div id="bottom">
        	 <?php echo $this->element('footer'); ?>
        </div>
    </div>
</body>
</html>

<?php 
	echo $this->Html->css(array('jquery.alerts'));
	echo $this->Html->script(array('jquery.alerts'));
?>