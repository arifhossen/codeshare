<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="css/prp.css" />
<title>PRP- Police Reform Programme</title>
	<?php
	echo $this->Html->meta('icon');
	echo $this->Html->css(array('prp','jquery.alerts',  'ui-lightness/jquery-ui-1.7.3.custom'));
	echo $scripts_for_layout;
	echo $this->Html->script(array('jquery.alerts', 'jquery-1.3.2.min', 'jquery-ui-1.7.3.custom.min', 'jquery.form', 'jquery.validate', 'jquery.validate', 'additional-methods'));
	
	?>
	
</head>
<body>
	<div id="body_wrap">
    	<div id="top"> </div>
        <div id="content">
           
	    	<?php echo $this->Session->flash(); ?>
			<?php echo $content_for_layout; ?>
		    
        </div>
		
        <div id="bottom">
		
        </div>
		
    </div>
</body>
</html>