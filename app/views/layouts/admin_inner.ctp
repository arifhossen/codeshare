<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php __('Smartactic Admin Panel -'); ?>
		<?php echo $title_for_layout; ?></title>

<?php	
	/*All style sheet included here*/ 
	  
	//echo $this->Html->charset();
	echo $this->Html->meta('icon');
	echo $this->Html->css('content');
	echo $this->Html->css('default');  
	echo $this->Html->css('inner-style'); 
	echo $this->Html->css('calendar'); 
	echo $this->Html->css('datepicker'); 
	echo $this->Html->script('datepicker'); 	
	
	/*All javascript included here*/ 
	//echo $this->Html->script('datepicker'); 	
	echo $this->Html->script('curvycorners'); 	
	echo $this->Html->script('jquery-1.6.2');
	echo $this->Html->script('prototype');
	echo $this->Html->script('jquery.validate');
   
    echo $this->Html->css('/fcbkcomplete/style');
    echo $this->Html->script('/fcbkcomplete/jquery.fcbkcomplete'); 

	
	echo $scripts_for_layout;
?>

<script type="text/JavaScript">

  window.onload = function()
  {

      settings = {
          tl: { radius: 7},
          tr: { radius: 7},
          bl: { radius: 7},
          br: { radius: 7},
          antiAlias: true,
          autoPad: true,
          validTags: ["div"]
      }

     settings2 = {
          tl: { radius: 7},
          tr: { radius: 7},
          bl: { radius: 0},
          br: { radius: 0},
          antiAlias: true,
          autoPad: true,
          validTags: ["div"]
      }

      var myBoxObject = new curvyCorners(settings, "carve-div");
      myBoxObject.applyCornersToAll();

      var myBoxObject2 = new curvyCorners(settings2, "carve-div-tab");
      myBoxObject2.applyCornersToAll();
  }

</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-13197635-4']);
  _gaq.push(['_setDomainName', 'none']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>



</head>

<body>
<!--header start -->

<div align="center">
	<div><?php echo $html->image('innertopbg.png');?></div>
		<div class="innerpagemiddle">
		        <div class="spacer" style="padding-bottom:15px; padding-right:65px; font-weight:bold;"><p align="right"><?php //echo  $fullName; ?> : <?php //echo $logged_in; ?></p></div>
				<div class="carve-div" id="carve-div-banner">
				
				    <table class="banner">
				        <tr>
							<td>
							<?php $img_url = "img/admin/inner-logo.png"; ?>
							 <img src="<?php echo $this->webroot;?>timthumb.php?src=<?php echo $img_url; ?>&w=329&h=68&zc=2" alt='Your Profile Photo' />
							<?php //echo $html->image('admin/inner-logo.png'); ?>	  					</td>
					      <td class="banner-tagline">
						 <?php						  
						 $topmenumodules =  $this->Session->read('top-banner-modules');
						 if(!empty($topmenumodules)){
						
						 foreach($topmenumodules as $key=>$value){	
							$mod_name = $value['Sitemodule']['modulename'];
							$url = $value['Sitemodule']['url'];
							$url = explode('/',$url);
							$controller = $url['0'];
							$action = $url['1'];
							
							if(!empty($url['2'])){
								$params = '/'.$url['2'];
							}else{
							 $params = '';
							}
							
							$controller_action = $this->webroot.'admin/'.$controller.'/'.$action.$params;
							
							echo "<div style='width:75px; font-size:10px; height:55px; float:left; border:0px solid gray;'>";
							$img_url = "img/admin/".$value['Sitemodule']['image']."";
							$imagepath = '<img src="'.$this->webroot.'timthumb.php?src='.$img_url.'&w=20&h=20&zc=1" alt="Product Photo" href="'.$controller_action.'" />';
							echo $x = '<a href='.$controller_action.' style=text-decoration:none;>'.$imagepath.'</a>';
							
							//echo $this->Html->link(($imagepath),  array('controller'=>$controller, 'action'=>$action));
							echo "<br>";
							echo $html->link($mod_name,  array('controller'=>$controller, 'action'=>$action,$params), null, false, false); 
							echo "</div>";	
						}						
						}
						else{
							
						}
						  ?>
						  
						  </td>
						</tr>
					</table>

</div>
				
		         <div class="spacer-banner-bottom">&nbsp;</div>
				<!-- End Header -->
				
				
				<!-- Start Main content -->

				<div class="main-div">
				    <table class="main-table" cellpadding="0">
						<tr>
						<td>&nbsp;</td>
							<td valign="top">
							
							</td>
						
							<td  valign="top">
							            
								<div class="carve-div" style='width:99%;'>
								
        						<div style="border: 0px solid red;  #float: left; padding:12px 12px 12px 12px;">
									<?php echo $this->Session->flash(); ?>
									<?php echo $content_for_layout;?>
									</div>
								</div>
							</td>
						<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				
<!-- 	Start Footer -->

</div>
	<div><?php echo $html->image('innerbottombg.png');?></div>
</div>


<!--End Footer -->
<?php echo $this->element('sql_dump'); ?>
</body>
</html>