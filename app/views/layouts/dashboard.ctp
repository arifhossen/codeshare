﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="msvalidate.01" content="B34CF6F615541671045747127244C8AF" />
<meta name="description" content="Send gift to Bangladesh, Deshichain offers you wonderful gifts from wide range of collection for your beloved in Bd. Order online to send Gifts, Flowers, Cakes & Chocolates, Greeting cards, Books, Perfumes, Foods , Sweets  & many more occasional gifts to Bangladesh.">
<meta name="keywords" content="Send Gift to Bangladesh, Send  gift to BD, Birthday gift package, wedding gift, flower's for birthday, Anneiversary gift, Eid packages, Flower's for her, Birthday Flower's,Flowers, Cakes & Chocolates, Greeting cards, pizza hut birthday package, Gift package, bangladesh online gift shop, gift to Bangladesh, bangladesh online shopping, sending gift to Bangladesh,Deshi Chain,Deshichainbd, DeshiChain gift,bd gift,  gift delivery service in Bangladesh, gift delivery service,  online shopping Bangladesh, gift in Bangladesh, gifts to Bangladesh, send gift to bd, bangladeshi gift sites, bangladesh online shopping website, gift for Bangladesh  ">

<title><?php __('Deshi Chain-');?><?php echo $title_for_layout; ?> <?php echo "Send Gift to Bangladesh - Gift, Flower, Cake Delivery to Bangladesh"; ?>

</title>

<?php	
/*All style sheet included here*/ 
echo $this->Html->charset();
echo $this->Html->meta('icon');
echo $this->Html->css('global');
echo $this->Html->css('sholoanaglobal');
echo $this->Html->script('jquery-1.6.2');
echo $javascript->link('jquery.validate');
echo $this->Html->script('prototype');

echo $this->Html->script('jquery.validation.functions');
/*All javascript included here*/ 

echo $scripts_for_layout;
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-13197635-4']);
  _gaq.push(['_setDomainName', 'none']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
    <body>
       <div id="container">
		
		<!-- header start here -->
		<?php echo $this->element('layout/header'); ?>
		<!-- Header end here -->		
		<div id="content-container">			
			<?php echo $this->Session->flash(); ?>
			<?php echo $content_for_layout; ?>
		</div>
		<!--content container end here-->
		
		<!-- footer start here -->
		<div id="footer">
		<?php echo $this->element('layout/footer'); ?>
		</div>
		<!-- Footer End here -->
		
		<!-- Copy right ingormation start here -->
		
		<!-- Copy right information end here -->
			
	</div>
	<?php echo $this->element('sql_dump'); ?>
    </body>
</html>