<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php __('Smartactic.com - '); ?>
		<?php echo $title_for_layout; ?></title>

<?php	
	echo $this->Html->meta('icon'); 
	echo $this->Html->css('default'); 
	echo $this->Html->css('inner-style'); 
	echo $this->Html->script('curvycorners'); 
?>

<script type="text/JavaScript">

  window.onload = function()
  {

      settings = {
          tl: { radius: 7},
          tr: { radius: 7},
          bl: { radius: 7},
          br: { radius: 7},
          antiAlias: true,
          autoPad: true,
          validTags: ["div"]
      }

     settings2 = {
          tl: { radius: 7},
          tr: { radius: 7},
          bl: { radius: 0},
          br: { radius: 0},
          antiAlias: true,
          autoPad: true,
          validTags: ["div"]
      }
      var myBoxObject = new curvyCorners(settings, "carve-div");
      myBoxObject.applyCornersToAll();

      var myBoxObject2 = new curvyCorners(settings2, "carve-div-tab");
      myBoxObject2.applyCornersToAll();
  }
</script>
<script type="text/javascript">

var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-13197635-4']);
  _gaq.push(['_setDomainName', 'none']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
varga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<style>
td {
	text-align:center;
}
</style>
</head>

<body>
<!--header start -->
<div align="center">
	<div><?php echo $html->image('innertopbg.png');?></div>
		<div class="innerpagemiddle">
		        <div class="spacer" style="padding-bottom:15px; padding-right:65px; font-weight:bold;"><p align="right"><?php //echo  $fullName; ?> : <?php //echo $logged_in; ?></p></div>
				<div class="carve-div" id="carve-div-banner">
				    <table class="banner" width="100%">
				        <tr>
							<td width="2%">
							  <?php echo $html->image('admin/inner-logo.png'); ?> 				</td>
							 <td width="98%" class="banner-tagline">				        <table width="100%" border="0">
                              <tr>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right" style="width:50%">&nbsp;</td>
                                <td align="right"><?php echo $html->image('admin/adminpanel.jpg'); ?></td>
                              </tr>
                              
                            </table></td>
						</tr>
					</table>
				</div>
				<div class="spacer">&nbsp;</div>
		         <div class="spacer-banner-bottom">&nbsp;</div>
				<!-- End Header -->
				<!-- Start Main content -->
				<div class="main-div">
				    <table class="main-table" cellpadding="0">
						<tr>				
							<td valign="top">&nbsp;</td>                            	
							<td  valign="top">
								<div class="carve-div" style='width:99%; height:800px;'>
        						<div style="border: 0px solid red;  #float: left; padding:12px 12px 12px 12px;">
									<?php echo $this->Session->flash(); ?>
									<?php echo $content_for_layout;?>
									</div>
								</div>
							</td>
							<td>&nbsp;</td>				
						</tr>
					</table>
				<!-- End Main content -->
				<div class="spacer">&nbsp;</div>
				</div>
<!-- 	Start Footer -->
</div>
	<div><?php echo $html->image('innerbottombg.png');?></div>
</div>

<!--End Footer -->
<?php echo $this->element('sql_dump'); ?>
</body>
</html>