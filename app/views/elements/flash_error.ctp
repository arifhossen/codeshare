
<div class="flash_error" style="
	width:489px;
	padding:10px;
	vertical-align:center;
	z-index:16;
	left: 25px;
	background-color: #ffa4aa;
	border: 1px solid #800000;
	font-weight:bold;
	margin:0 0 15px;">
     <?php echo $html->image('icons/negative.png', array( 'alt' => 'Error', 'style' => 'vertical-align:middle;')) ?>
     <span style="flash_good"> <?php echo $message; ?> </span> 
</div>  
