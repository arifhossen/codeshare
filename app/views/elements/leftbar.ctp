<?php

/************** This code only for menu selected **************/
	$controller = $this->params['controller'];
	$action = $this->params['action'];
	
	if(isset($this->params['slug'])){
		$slug = $this->params['slug'];
	}
	
	
			
	$selected_id = '';
	$selected = '';
	$selected_user_link ='';
	$selected_user_password_link = '';
	$selected_trainers_link ='';
	$selected_participants_link ='';
	$selected_courses_link ='';
	$selected_institutions_link ='';
	$selected_branches_link ='';
	$selected_sections_link ='';
	$selected_units_link ='';
	$selected_departments_link ='';
	$selected_venues_link ='';
	$selected_designations_link ='';
	$selected_ranks_link ='';
	$selected_reports_link ='';
	$selected_reports_trained_link ='';
	$selected_reports_participant_link ='';
	$selected_reports_ngo_link ='';
	$selected_reports_custom_link ='';
	
	
	if($controller == "users" && $action == "index"){
		$selected_id = '1';
		$selected_user_link = "selected";
	}
	else if($controller == "users" && $action == "change_password"){
		$selected_id = '1';
		$selected_user_password_link = "selected";
	}
	else if($controller == "trainers" && $action == "index"){
		$selected_id = '2';
		$selected_trainers_link = "selected";
	}
	else if($controller == "participants" && $action == "index"){
		$selected_id = '3';
		$selected_participants_link  = "selected";
	}
	else if($controller == "courses" && $action == "index"){
		$selected_id = '4';
		$selected_courses_link  = "selected";
	}
	else if($controller == "institutions" && $action == "index"){
		$selected_id = '5';
		$selected_institutions_link  = "selected";
	}
	
	else if($controller == "branches" && $action == "index"){
		$selected_id = '6';
		$selected_branches_link  = "selected";
	}
	
	else if($controller == "sections" && $action == "index"){
		$selected_id = '7';
		$selected_sections_link  = "selected";
	}
	
	else if($controller == "units" && $action == "index"){
		$selected_id = '8';
		$selected_units_link  = "selected";
	}
	
	else if($controller == "departments" && $action == "index"){
		$selected_id = '9';
		$selected_departments_link  = "selected";
	}
	
	else if($controller == "users" && $action == "statements"){
		$selected_id = '10';
		$selected_venues_link  = "selected";
	}
	
	else if($controller == "designations" && $action == "index"){
		$selected_id = '11';
		$selected_designations_link  = "selected";
	}
	
	else if($controller == "ranks" && $action == "index"){
		$selected_id = '12';
		$selected_ranks_link  = "selected";
	}
	
	else if($controller == "reports" && $action == "index"){
		$selected_id = '13';
		$selected_reports_link  = "selected";
	}
	
	else if($controller == "reports" && $action == "trained_report"){
		$selected_id = '13';
		$selected_reports_trained_link  = "selected";
	}
	
	else if($controller == "reports" && $action == "participant_report"){
		$selected_id = '13';
		$selected_reports_participant_link  = "selected";
	}
	
	else if($controller == "reports" && $action == "ngo_report"){
		$selected_id = '13';
		$selected_reports_ngo_link  = "selected";
	}
	
	else if($controller == "reports" && $action == "custom_report"){
		$selected_id = '13';
		$selected_reports_custom_link  = "selected";
	}
		
?>




<script type="text/javascript">
$(function() {  


	   selectedId = '<?php echo $selected_id; ?>';
	   
       $('#subMenu_'+selectedId).show();
	   $('#view_'+selectedId).hide();
	   $('#close_'+selectedId).show();	




            
		$('.view').click(function(){	
		    var data = $(this).attr("id");
			
			var dataArray = data.split('_');
			var id = dataArray[1];
			
			$('#subMenu_'+id).show();
			$('#view_'+id).hide();
			$('#close_'+id).show();	
					
		});
		
		$(".close").click(function() {
		
		    var data = $(this).attr("id");
			
			var dataArray = data.split('_');
			var id = dataArray[1];
			
			
			$('#subMenu_'+id).hide();
			$('#view_'+id).show();
			$('#close_'+id).hide();			
		});
		
			
		
	});
	
</script>





 <div id="MenuList" class="menu_right_border">
 

		 
		 
		 <div id='menu1'#accordion>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_1'>
					 System User
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_1' style="display:none;">	
					 System User
				</h4>
				
			</div>
			
			<div id="subMenu_1" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('user.png', array('alt'=>'user', 'title'=>'Manage Users')); ?>  <?php echo $this->Html->link('Manage Users', array('controller'=>'users', 'action'=>'index'),array('class'=>"v_align_top $selected_user_link",'escape' => false));?>
					</strong><br/>					
				</div>
			</div>
		
		</div>
		<div style='clear:both;'></div>
		
		
		 <div id='menu2'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_2'>
					Change Password
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_2' style="display:none;">	
					Change Password
				</h4>
				
			</div>
			
			<div id="subMenu_2" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('small_trainer.png', array('alt'=>'user')); ?>  <?php echo $this->Html->link('Change password', array('controller'=>'users', 'action'=>'changepassword'),array('class'=>"v_align_top $selected_trainers_link",'escape' => false));?>
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		 <div id='menu100'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_100'>
					Payment Config
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_100' style="display:none;">	
					Payment Config
				</h4>
				
			</div>
			
			<div id="subMenu_100" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('small_trainer.png', array('alt'=>'Payment Configuration')); ?>  <?php echo $this->Html->link('Payment Config', array('controller'=>'payment_gateways', 'action'=>'index'),array('class'=>"v_align_top $selected_trainers_link",'escape' => false));?>
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		
		<div style='clear:both;'></div>
		
		<div id='menu3'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_3'>
					 Article Pages
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_3' style="display:none;">	
					Article Pages
				</h4>
				
			</div>
			
			<div id="subMenu_3" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('small_user.png', array('alt'=>'user')); ?>  <?php echo $this->Html->link('Articles', array('controller'=>'contents', 'action'=>'index'),array('class'=>"v_align_top $selected_participants_link",'escape' => false));?>
					</strong><br/>
					
				</div>
			</div>
		
		</div>
		<div style='clear:both;'></div>
		
		 <div id='menu4'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_4'>
					Home page
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_4' style="display:none;">	
					Home page
				</h4>
				
			</div>
			
			<div id="subMenu_4" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('small_trainer.png', array('alt'=>'user')); ?>  <?php echo $this->Html->link('Manage Homepage', array('controller'=>'supportedPlatforms', 'action'=>'index'),array('escape' => false));?>
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		
		
		<div style='clear:both;'></div>
		
		 
		
		 <div id='menu5'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_5'>
					Package Details
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_5' style="display:none;">	
					Package Details
				</h4>
				
			</div>
			
			<div id="subMenu_5" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('course.png', array('alt'=>'user')); ?>  <?php echo $this->Html->link('Manage Package', array('controller'=>'packages', 'action'=>'index'),array('class'=>"v_align_top $selected_courses_link",'escape' => false));?>
					</strong><br/>
					
				</div>
				
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('course.png', array('alt'=>'user')); ?>  <?php echo $this->Html->link('Package Pricing', array('controller'=>'package_setups', 'action'=>'index'),array('class'=>"v_align_top $selected_courses_link",'escape' => false));?>
					</strong><br/>
					
				</div>
			</div>
		
		</div>
		<div style='clear:both;'></div>
		
		
		
		 <div id='menu6'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_6'>
					 App Design
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_6' style="display:none;">	
					 App Design
				</h4>
				
			</div>
			
			<div id="subMenu_6" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('small_home.png', array('alt'=>'user')); ?>  <?php echo $this->Html->link('Manage App Design', array('controller'=>'designCodes', 'action'=>'index',1),array('class'=>"v_align_top $selected_branches_link",'escape' => false));?>
					</strong><br/>
				</div>
			</div>
		
		</div>
		<div style='clear:both;'></div>
		
		 <div id='menu7'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_7'>
					 Mobile Design
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_7' style="display:none;">	
					 Mobile Design
				</h4>
				
			</div>
			
			<div id="subMenu_7" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('small_home.png', array('alt'=>'user')); ?>  <?php echo $this->Html->link('Manage Mobile Design', array('controller'=>'designCodes', 'action'=>'index',2),array('class'=>"v_align_top $selected_sections_link",'escape' => false));?>
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		<div style='clear:both;'></div>
		
		 <div id='menu8'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_8'>
					 Web Design
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_8' style="display:none;">	
					 Web Design
				</h4>
				
			</div>
			
			<div id="subMenu_8" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('small_home.png', array('alt'=>'user')); ?>  <?php echo $this->Html->link('Manage Web Design', array('controller'=>'designCodes', 'action'=>'index',3),array('class'=>"v_align_top $selected_units_link",'escape' => false));?>
					</strong><br/>
				</div>
			</div>
		
		</div>
		<div style='clear:both;'></div>
		
		 <div id='menu9'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_9'>
					 Background Design
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_9' style="display:none;">	
					 Background Design
				</h4>
				
			</div>
			
			<div id="subMenu_9" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('small_home.png', array('alt'=>'user')); ?>  <?php echo $this->Html->link('Manage Background Design', array('controller'=>'designCodes', 'action'=>'index',4),array('class'=>"",'escape' => false));?>
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		
		
		
				
			
		<div style='clear:both;'></div>
		
		 <div id='menu10'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_10'>
					Statements/Recipts 
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_10' style="display:none;">	
					Statements/Recipts 
				</h4>
				
			</div>
			
			<div id="subMenu_10" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php echo $this->Html->link('Statements/Recipts', array('controller'=>'users', 'action'=>'admin_statements'),array('class'=>"v_align_top $selected_courses_link",'escape' => false));?>
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		
		
			
		<div style='clear:both;'></div>
		
		 <div id='menu11'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_11'>
					Refunds/Credits
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_11' style="display:none;">	
					Refunds/Credits
				</h4>
				
			</div>
			
			<div id="subMenu_11" style="display:none;">
				<div class="left_menu_link">
					<strong>
						
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		<div style='clear:both;'></div>
		
		 <div id='menu12'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_12'>
					Alert Status
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_12' style="display:none;">	
					Alert Status
				</h4>
				
			</div>
			
			<div id="subMenu_12" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('small_home.png', array('alt'=>'user')); ?>  <?php echo $this->Html->link('Manage Email Templates', array('controller'=>'email_templates', 'action'=>'index'),array('class'=>"v_align_top $selected_units_link",'escape' => false));?>
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		
		<div style='clear:both;'></div>
		
		 <div id='menu13'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_13'>
					Manage Leads
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_13' style="display:none;">	
					Manage Leads
				</h4>
				
			</div>
			
			<div id="subMenu_13" style="display:none;">
				<div class="left_menu_link">
					<strong>
					
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		
		
		<div style='clear:both;'></div>
		
		 <div id='menu14'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_14'>
					Work order Status
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_14' style="display:none;">	
					Work order Status
					
					
				</h4>
				
			</div>
			
			<div id="subMenu_14" style="display:none;">
				<div class="left_menu_link">
					
					
				</div>
			</div>
		
		</div>
		
		
		<div style='clear:both;'></div>
		
		 <div id='menu15'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_15'>
					Declined Credit Cards
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_15' style="display:none;">	
				    Declined Credit Cards
				</h4>
				
			</div>
			
			<div id="subMenu_15" style="display:none;">
				<div class="left_menu_link">
					<strong>
					
					</strong><br/>
				</div>
			</div>
		
		</div>
		
	
		<div style='clear:both;'></div>
		
		 <div id='menu16'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_16'>
					News
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_16' style="display:none;">	
				    News
				</h4>
				
			</div>
			
			<div id="subMenu_16" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php  echo $this->Html->image('small_home.png', array('alt'=>'user')); ?>  <?php echo $this->Html->link('Manage News', array('controller'=>'news', 'action'=>'index'),array('class'=>"v_align_top $selected_units_link",'escape' => false));?>
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		
		<div style='clear:both;'></div>
		
		 <div id='menu17'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_17'>
					Sales Rep Commission
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_17' style="display:none;">	
				    Sales Rep Commission
				</h4>
				
			</div>
			
			<div id="subMenu_17" style="display:none;">
				<div class="left_menu_link">
					<strong>
						
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		
		<div style='clear:both;'></div>
		
		 <div id='menu18'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_18'>
					Developers Commission
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_18' style="display:none;">	
				   Developers Commission
				</h4>
				
			</div>
			
			<div id="subMenu_18" style="display:none;">
				<div class="left_menu_link">
					<strong>
						
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		
		<div style='clear:both;'></div>
		
		 <div id='menu19'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_19'>
					Create Invoice
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_19' style="display:none;">	
				   Create Invoice
				</h4>
				
			</div>
			
			<div id="subMenu_19" style="display:none;">
				<div class="left_menu_link">
					<br/>
				</div>
			</div>
		
		</div>
		
		
		
		<div style='clear:both;'></div>
		
		 <div id='menu20'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_20'>
					POP
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_20' style="display:none;">	
				  POP
				</h4>
				
			</div>
			
			<div id="subMenu_20" style="display:none;">
				<div class="left_menu_link">
					
				</div>
			</div>
		
		</div>
		
		<div style='clear:both;'></div>
		
		<div id='menu21'>
			 
			<div id="menuHeader" class="left_menu_off">			
						
				<h4 class='view menu_arrow' id='view_21'>
					Email Templates
				</h4>
				
				<h4 class='close menu_down_arrow' id='close_21' style="display:none;">	
					Email Templates
				</h4>
				
			</div>
			
			<div id="subMenu_21" style="display:none;">
				<div class="left_menu_link">
					<strong>
						<?php echo $this->Html->link('Email Templates', array('controller'=>'email_templates', 'action'=>'index'),array('class'=>"v_align_top $selected_courses_link",'escape' => false));?>
					</strong><br/>
				</div>
			</div>
		
		</div>
		
		
		
		
		
</div>	

