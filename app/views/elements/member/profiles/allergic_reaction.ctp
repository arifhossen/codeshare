<script type="text/javascript">

    /**************************************************************************************
     *  Dynamic add more option function
     *  return addmore html
     **************************************************************************************/
    function addMoreOption_is_had_any_allergic(name, total){
        var no =   $('.'+name+'_add_more_cancel').length+1;
        var max_add  = parseInt(total) - 1 ;
        var text = '<div id="'+name+'_add_more_container_'+no+'" class="is_had_any_allergic_container" > ';
        text += ' <span class="is_had_any_allergic is_had_any_allergic_name" ><input  type="text" name="data[allergic_reaction][name][]" value="" id="is_had_any_allergic_name_'+no+'" class="add_more_input_is_had_any_allergic" ></span>';
        text += ' <span class="is_had_any_allergic is_had_any_allergic_speciality" ><input  type="text" name="data[allergic_reaction][reaction][]" value="" id="is_had_any_allergic_strength_'+no+'" class="add_more_input_is_had_any_allergic" ></span>';


        text += ' <span class="'+name+'_add_more_cancel add_more_cancel"  id="'+name+'_cancel_'+no+'" onclick="addMoreOptionCancel(\''+name+'_add_more_container_'+no+'\', \''+no+'\', \''+name+'\')" >X</span>';
        text += '</div> <div style="clear: both;"></div>';

        $('#'+name+'_add_more_panel').append(text);
        // i++;
        //var no =   $('.'+name+'_add_more_cancel').length;
        if(no >= max_add) {
            $('#'+name+'_add_more').hide();
            no=1;
        }
    }


</script>

<style>
    .add_more_input_is_had_any_allergic{ width: 95%!important; padding: 2px;}
    .is_had_any_allergic_container_header{ margin: 5px; padding-bottom:7px; font-weight: bold;}
    .is_had_any_allergic_container{ margin: 5px; padding-bottom:7px;}
    .is_had_any_allergic{ float: left;}
    .is_had_any_allergic_name{width:300px;}
    .is_had_any_allergic_speciality{width:300px;}
</style>

<div style="float: left; clear:both;">
    <div class="" id="is_had_any_allergic_add_more_panel">
        <div class="is_had_any_allergic_container_header" style="padding: 0px;" >
            <span class="is_had_any_allergic is_had_any_allergic_name">Name of the Allergies</span>
            <span class="is_had_any_allergic is_had_any_allergic_speciality">Reactions</span>

        </div>
        <div style="clear: both;"></div> <?php //pr($AllergicReaction);
        if(isset($AllergicReaction) && !empty ($AllergicReaction)) {
            $i=0;
            foreach ($AllergicReaction as $list) {
                ?>
        <div class="is_had_any_allergic_container" id="is_had_any_allergic_add_more_container_<?php echo $i; ?>">
            <span class="is_had_any_allergic is_had_any_allergic_name"><input type="text" class="add_more_input_is_had_any_allergic" id="is_had_any_allergic_name_0" value="<?php echo $list['AllergicReaction']['name']; ?>" name="data[allergic_reaction][name][]"></span>
            <span class="is_had_any_allergic is_had_any_allergic_speciality"><input type="text" class="add_more_input_is_had_any_allergic" id="is_had_any_allergic_speciality_0" value="<?php echo $list['AllergicReaction']['reaction']; ?>" name="data[allergic_reaction][reaction][]"></span>
       <?php if($i!=0) {?>
            <span onclick="addMoreOptionCancel('is_had_any_allergic_add_more_container_<?php echo $i; ?>', '<?php echo $i; ?>', 'is_had_any_allergic')" id="is_had_any_allergic_cancel_<?php echo $i; ?>" class="is_had_any_allergic_add_more_cancel add_more_cancel">X</span>
   <?php } ?>
        </div> <div style="clear: both;"></div>
                <?php
                $i++;
            }
        } else {
            ?>
        <div class="is_had_any_allergic_container" id="is_had_any_allergic_add_more_container_0">
            <span class="is_had_any_allergic is_had_any_allergic_name"><input type="text" class="add_more_input_is_had_any_allergic" id="is_had_any_allergic_name_0" value="" name="data[allergic_reaction][name][]"></span>
            <span class="is_had_any_allergic is_had_any_allergic_speciality"><input type="text" class="add_more_input_is_had_any_allergic" id="is_had_any_allergic_speciality_0" value="" name="data[allergic_reaction][reaction][]"></span>
        </div>
        <div style="clear: both;"></div>
            <?php  } ?>
    </div>
    <span id="is_had_any_allergic_add_more" class="add_more" onclick="addMoreOption_is_had_any_allergic('is_had_any_allergic');">Add More</span>

</div>
