<script type="text/javascript">


    /**************************************************************************************
     *  Dynamic add more option function
     *  return addmore html
     **************************************************************************************/
    function addMoreOption_medication(name, total){
        var no =   $('.'+name+'_add_more_cancel').length+1;
        var max_add  = parseInt(total) - 1 ;
        var text = '<div id="'+name+'_add_more_container_'+no+'" class="drug_container" > ';
        text += ' <span class="drug drug_name" ><input  type="text" name="data[Medication][drug_name][]" value="" id="drug_name_'+no+'" class="add_more_input_medication" ></span>';
        text += ' <span class="drug drug_strength" ><input  type="text" name="data[Medication][drug_strength][]" value="" id="drug_strength_'+no+'" class="add_more_input_medication" ></span>';
        text += ' <span class="drug drug_formulation" ><input  type="text" name="data[Medication][drug_formulation][]" value="" id="drug_formulation_'+no+'" class="add_more_input_medication" ></span>';
        text += ' <span class="drug drug_dose" ><input  type="text" name="data[Medication][dose][]" value="" id="dose_'+no+'" class="add_more_input_medication" ></span>';
        text += ' <span class="drug drug_from" ><input  type="text" name="data[Medication][from][]" value="" id="from'+no+'" class="add_more_input_medication" ></span>';
        text += ' <span class="drug drug_to" ><input  type="text" name="data[Medication][to][]" value="" id="to_'+no+'" class="add_more_input_medication" ></span>';
        text += ' <span class="drug drug_comment" ><input  type="text" name="data[Medication][comment][]" value="" id="comment_'+no+'" class="add_more_input_medication" ></span>';


        text += ' <span class="'+name+'_add_more_cancel add_more_cancel"  id="'+name+'_cancel_'+no+'" onclick="addMoreOptionCancel(\''+name+'_add_more_container_'+no+'\', \''+no+'\', \''+name+'\')" >X</span>';
        text += '</div> <div style="clear: both;"></div>';

        $('#'+name+'_add_more_panel').append(text);
        // i++;
        //var no =   $('.'+name+'_add_more_cancel').length;
        if(no >= max_add) {
            $('#'+name+'_add_more').hide();
            no=1;
        }
    }


</script>
<style>
    .add_more_input_medication{ width: 95%!important; padding: 2px;}
    .drug_container_header{ margin: 5px; padding-bottom:7px;}
    .drug_container{ margin: 5px; padding-bottom:7px;}
    .drug{ float: left;}
    .drug_name{width:20%;}
    .drug_strength{width:15%;}
    .drug_formulation{width: 15%;}
    .drug_dose{width: 15%;}
    .drug_from{width:8%;}
    .drug_to{width:8%;}
    .drug_comment{width:15%;}

</style>


<div class="" id="drug_add_more_panel">
    <div class="drug_container_header" style="padding: 0px;" >
        <span class="drug drug_name">Name </span>
        <span class="drug drug_strength">Strength</span>
        <span class="drug drug_formulation">formulation</span>
        <span class="drug drug_dose">Dose</span>
        <span class="drug drug_from">From</span>
        <span class="drug drug_to">To</span>
        <span class="drug drug_comment">Comment</span>

    </div>
    <div style="clear: both;"></div>
    <?php //pr($drugHistory);
    if(isset($drugHistory) && !empty ($drugHistory)) {
        $i=0;
        foreach ($drugHistory as $list) {
            ?>
    <div class="drug_container" id="drug_add_more_container_<?php echo $i; ?>">
        <span class="drug drug_name"><input type="text" class="add_more_input_medication" id="drug_name_0" value="<?php echo $list['Medication']['drug_name']; ?>" name="data[Medication][drug_name][]"></span>
        <span class="drug drug_strength"><input type="text" class="add_more_input_medication" id="drug_strength_0" value="<?php echo $list['Medication']['drug_strength']; ?>" name="data[Medication][drug_strength][]"></span>
        <span class="drug drug_formulation"><input type="text" class="add_more_input_medication" id="drug_formulation_0" value="<?php echo $list['Medication']['drug_formulation']; ?>" name="data[Medication][drug_formulation][]"></span>
        <span class="drug drug_dose"><input type="text" class="add_more_input_medication" id="dose_0" value="<?php echo $list['Medication']['dose']; ?>" name="data[Medication][dose][]"></span>
        <span class="drug drug_from"><input type="text" class="add_more_input_medication" id="duration_0" value="<?php echo $list['Medication']['from']; ?>" name="data[Medication][from][]"></span>
        <span class="drug drug_to"><input type="text" class="add_more_input_medication" id="duration_0" value="<?php echo $list['Medication']['to']; ?>" name="data[Medication][to][]"></span>
        <span class="drug drug_comment"><input type="text" class="add_more_input_medication" id="duration_0" value="<?php echo $list['Medication']['comment']; ?>" name="data[Medication][comment][]"></span>
    <?php if($i!=0) {?>
                    <span onclick="addMoreOptionCancel('drug_add_more_container_<?php echo $i; ?>', '<?php echo $i; ?>', 'drug')" id="drug_cancel_<?php echo $i; ?>" class="drug_add_more_cancel add_more_cancel">X</span>
   <?php } ?>
        </div> <div style="clear: both;"></div>
                <?php
                $i++;
            }
        } else {
            ?>
    <div class="drug_container" id="drug_add_more_container_0">
        <span class="drug drug_name"><input type="text" class="add_more_input_medication" id="drug_name_0" value="" name="data[Medication][drug_name][]"></span>
        <span class="drug drug_strength"><input type="text" class="add_more_input_medication" id="drug_strength_0" value="" name="data[Medication][drug_strength][]"></span>
        <span class="drug drug_formulation"><input type="text" class="add_more_input_medication" id="drug_formulation_0" value="" name="data[Medication][drug_formulation][]"></span>
        <span class="drug drug_dose"><input type="text" class="add_more_input_medication" id="dose_0" value="" name="data[Medication][dose][]"></span>
        <span class="drug drug_from"><input type="text" class="add_more_input_medication" id="duration_0" value="" name="data[Medication][from][]"></span>
        <span class="drug drug_to"><input type="text" class="add_more_input_medication" id="duration_0" value="" name="data[Medication][to][]"></span>
        <span class="drug drug_comment"><input type="text" class="add_more_input_medication" id="duration_0" value="" name="data[Medication][comment][]"></span>
    </div>
    <div style="clear: both;"></div>
        <?php  } ?>
</div>
<span id="drug_add_more" class="add_more" onclick="addMoreOption_medication('drug');">Add More</span>

