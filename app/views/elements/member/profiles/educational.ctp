<div class="element">
    <div class="title">Education Level:</div>
    <div class="value">
        <?php $education_level = array('SSC'=>'SSC','HSC'=>'HSC', 'Bachelors'=>'Bachelors','Masters'=>'Masters','Other'=>'Other');
        $value = 'SSC';
        $other_value = '';
        if(isset($Profile['Profile']['education_level'])) {
            if(in_array($Profile['Profile']['education_level'], $education_level)) {
                $value = $Profile['Profile']['education_level'];
            }
            else {
                $value = 'Other';
                $other_value = $Profile['Profile']['education_level'];
            }
        }
        echo $form->input('education_level',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
        'value'=>$value,
        'options'=>$education_level));
        
        echo $form->input('education_level_other',array('label'=>false,'value'=>$other_value,'div'=>false,'style'=>'width: 100px;'));
        ?>

    </div>

</div>

<div class="element">
    <div class="title">Education Medium:</div>
    <div class="value">
        <?php $education_medium = array('Bangla'=>'Bangla','English'=>'English', 'Arabic'=>'Arabic');
        echo $form->input('education_medium',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio','value'=>'Bangla','options'=>$education_medium));?>

    </div>

</div>