<script type="text/javascript">

    /**************************************************************************************
     *  Dynamic add more option function
     *  return addmore html
     **************************************************************************************/
    function addMoreOption_is_ever_patient(name, total){
        var no =   $('.'+name+'_add_more_cancel').length+1;
        var max_add  = parseInt(total) - 1 ;
        var text = '<div id="'+name+'_add_more_container_'+no+'" class="is_ever_patient_container" > ';
        text += ' <span class="is_ever_patient is_ever_patient_name" ><input  type="text" name="data[hospitalization][treated_for][]" value="" id="treated_for_'+no+'" class="add_more_input_is_ever_patient" ></span>';
        text += ' <span class="is_ever_patient is_ever_patient_speciality" ><input  type="text" name="data[hospitalization][hospital_name][]" value="" id="hospital_name_'+no+'" class="add_more_input_is_ever_patient" ></span>';
        text += ' <span class="is_ever_patient is_ever_patient_from" ><input  type="text" name="data[hospitalization][stay_from][]" value="" id="stay_from_'+no+'" class="add_more_input_medication" ></span>';
        text += ' <span class="is_ever_patient is_ever_patient_to" ><input  type="text" name="data[hospitalization][stay_to][]" value="" id="stay_to_'+no+'" class="add_more_input_medication" ></span>';

        text += ' <span class="'+name+'_add_more_cancel add_more_cancel"  id="'+name+'_cancel_'+no+'" onclick="addMoreOptionCancel(\''+name+'_add_more_container_'+no+'\', \''+no+'\', \''+name+'\')" >X</span>';
        text += '</div> <div style="clear: both;"></div>';

        $('#'+name+'_add_more_panel').append(text);
        // i++;
        //var no =   $('.'+name+'_add_more_cancel').length;
        if(no >= max_add) {
            $('#'+name+'_add_more').hide();
            no=1;
        }
    }


</script>

<style>
    .add_more_input_is_ever_patient{ width: 95%!important; padding: 2px;}
    .is_ever_patient_container_header{ margin: 5px; padding-bottom:7px; font-weight: bold;}
    .is_ever_patient_container{ margin: 5px; padding-bottom:7px;}
    .is_ever_patient{ float: left;}
    .is_ever_patient_name{width: 210px;}
    .is_ever_patient_speciality{width: 250px;}
    .is_ever_patient_from{width:15%;}
    .is_ever_patient_to{width:15%;}
</style>

<div style="float: left; clear:both;">

    <div class="" id="is_ever_patient_add_more_panel">
        <div class="is_ever_patient_container_header" style="padding: 0px;" >
            <span class="is_ever_patient is_ever_patient_name">Treated for</span>
            <span class="is_ever_patient is_ever_patient_speciality">Hospital</span>
            <span class="is_ever_patient is_ever_patient_from">From</span>
            <span class="is_ever_patient is_ever_patient_to">To</span>
        </div>
        <div style="clear: both;"></div>
        <?php //pr($Hospitalization);
        if(isset($Hospitalization) && !empty ($Hospitalization)) {
            $i=0;
            foreach ($Hospitalization as $list) {
                ?>
        <div class="is_ever_patient_container" id="is_ever_patient_add_more_container_<?php echo $i; ?>">
            <span class="is_ever_patient is_ever_patient_name"><input type="text" class="add_more_input_is_ever_patient" id="is_ever_patient_name_0" value="<?php echo $list['Hospitalization']['treated_for']; ?>" name="data[hospitalization][treated_for][]"></span>
            <span class="is_ever_patient is_ever_patient_speciality"><input type="text" class="add_more_input_is_ever_patient" id="is_ever_patient_speciality_0" value="<?php echo $list['Hospitalization']['hospital_name']; ?>" name="data[hospitalization][hospital_name][]"></span>
            <span class="is_ever_patient is_ever_patient_from"><input type="text" class="add_more_input_medication" id="duration_0" value="<?php echo $list['Hospitalization']['stay_from']; ?>" name="data[hospitalization][stay_from][]"></span>
            <span class="is_ever_patient is_ever_patient_to"><input type="text" class="add_more_input_medication" id="stay_to_0" value="<?php echo $list['Hospitalization']['stay_to']; ?>" name="data[hospitalization][stay_to][]"></span>
        <?php if($i!=0) {?>
            <span onclick="addMoreOptionCancel('is_ever_patient_add_more_container_<?php echo $i; ?>', '<?php echo $i; ?>', 'is_ever_patient')" id="is_ever_patient_cancel_<?php echo $i; ?>" class="is_ever_patient_add_more_cancel add_more_cancel">X</span>
   <?php } ?>
        </div> <div style="clear: both;"></div>
                <?php
                $i++;
            }
        } else {
            ?>
        <div class="is_ever_patient_container" id="is_ever_patient_add_more_container_0">
            <span class="is_ever_patient is_ever_patient_name"><input type="text" class="add_more_input_is_ever_patient" id="is_ever_patient_name_0" value="" name="data[hospitalization][treated_for][]"></span>
            <span class="is_ever_patient is_ever_patient_speciality"><input type="text" class="add_more_input_is_ever_patient" id="is_ever_patient_speciality_0" value="" name="data[hospitalization][hospital_name][]"></span>
            <span class="is_ever_patient is_ever_patient_from"><input type="text" class="add_more_input_medication" id="duration_0" value="" name="data[hospitalization][stay_from][]"></span>
            <span class="is_ever_patient is_ever_patient_to"><input type="text" class="add_more_input_medication" id="stay_to_0" value="" name="data[hospitalization][stay_to][]"></span>
        </div>
        <div style="clear: both;"></div>
            <?php  } ?>
    </div>
    <span id="is_ever_patient_add_more" class="add_more" onclick="addMoreOption_is_ever_patient('is_ever_patient');">Add More</span>

</div>
