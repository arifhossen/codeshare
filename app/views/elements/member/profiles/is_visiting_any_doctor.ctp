<script type="text/javascript">

    /**************************************************************************************
     *  Dynamic add more option function
     *  return addmore html
     **************************************************************************************/
    function addMoreOption_is_visiting_any_doctors(name, total){
        var no =   $('.'+name+'_add_more_cancel').length+1;
        var max_add  = parseInt(total) - 1 ;
        var text = '<div id="'+name+'_add_more_container_'+no+'" class="is_visiting_any_doctors_container" > ';
        text += ' <span class="is_visiting_any_doctors is_visiting_any_doctors_name" ><input  type="text" name="data[IsVisitingAnyDoctor][doctor_name][]" value="" id="doctor_name_'+no+'" class="add_more_input_is_visiting_any_doctors" ></span>';
        text += ' <span class="is_visiting_any_doctors is_visiting_any_doctors_speciality" ><input  type="text" name="data[IsVisitingAnyDoctor][doctor_speciality][]" value="" id="doctor_speciality_'+no+'" class="add_more_input_is_visiting_any_doctors" ></span>';
        text += ' <span class="is_visiting_any_doctors is_visiting_any_doctors_speciality" ><input  type="text" name="data[IsVisitingAnyDoctor][last_visit_date][]" value="" id="last_visit_date_'+no+'" class="add_more_input_is_visiting_any_doctors" ></span>';


        text += ' <span class="'+name+'_add_more_cancel add_more_cancel"  id="'+name+'_cancel_'+no+'" onclick="addMoreOptionCancel(\''+name+'_add_more_container_'+no+'\', \''+no+'\', \''+name+'\')" >X</span>';
        text += '</div> <div style="clear: both;"></div>';

        $('#'+name+'_add_more_panel').append(text);
        // i++;
        //var no =   $('.'+name+'_add_more_cancel').length;
        if(no >= max_add) {
            $('#'+name+'_add_more').hide();
            no=1;
        }
    }


</script>

<style>
    .add_more_input_is_visiting_any_doctors{ width: 95%!important; padding: 2px;}
    .is_visiting_any_doctors_container_header{ margin: 5px; padding-bottom:7px; font-weight: bold;}
    .is_visiting_any_doctors_container{ margin: 5px; padding-bottom:7px;}
    .is_visiting_any_doctors{ float: left;}
    .is_visiting_any_doctors_name{width: 210px;}
    .is_visiting_any_doctors_speciality{width: 200px;}
</style>

<div style="float: left; clear:both;">

    <div class="" id="is_visiting_any_doctors_add_more_panel">
        <div class="is_visiting_any_doctors_container" style="padding: 0px;" >
            <span class="is_visiting_any_doctors is_visiting_any_doctors_name">Name of Doctor</span>
            <span class="is_visiting_any_doctors is_visiting_any_doctors_speciality">Speciality</span>
            <span class="is_visiting_any_doctors is_visiting_any_doctors_relationship">Date of last Visit</span>

        </div>
        <div style="clear: both;"></div>
        <?php //pr($IsVisitingAnyDoctor);
        if(isset($IsVisitingAnyDoctor) && !empty ($IsVisitingAnyDoctor)) {
            $i=0;
            foreach ($IsVisitingAnyDoctor as $list) {

                ?>
        <div class="is_visiting_any_doctors_container" id="is_visiting_any_doctors_add_more_container_<?php echo $i; ?>">
            <span class="is_visiting_any_doctors is_visiting_any_doctors_name"><input type="text" class="add_more_input_is_visiting_any_doctors" id="is_visiting_any_doctors_name_0" value="<?php echo $list['IsVisitingAnyDoctor']['doctor_name']; ?>" name="data[IsVisitingAnyDoctor][doctor_name][]"></span>
            <span class="is_visiting_any_doctors is_visiting_any_doctors_speciality"><input type="text" class="add_more_input_is_visiting_any_doctors" id="is_visiting_any_doctors_speciality_0" value="<?php echo $list['IsVisitingAnyDoctor']['doctor_speciality']; ?>" name="data[IsVisitingAnyDoctor][doctor_speciality][]"></span>
            <span class="is_visiting_any_doctors is_visiting_any_doctors_speciality"><input type="text" class="add_more_input_is_visiting_any_doctors" id="is_visiting_any_doctors_last_visit_date_0" value="<?php echo $list['IsVisitingAnyDoctor']['last_visit_date']; ?>" name="data[IsVisitingAnyDoctor][last_visit_date][]"></span>
                    <?php if($i!=0) {?>
            <span onclick="addMoreOptionCancel('is_visiting_any_doctors_add_more_container_<?php echo $i; ?>', '<?php echo $i; ?>', 'is_visiting_any_doctors')" id="is_visiting_any_doctors_cancel_2" class="is_visiting_any_doctors_add_more_cancel add_more_cancel">X</span>
                        <?php } ?>
        </div> <div style="clear: both;"></div>
                <?php
                $i++;
            }
        } else {
            ?>
        <div class="is_visiting_any_doctors_container" id="is_visiting_any_doctors_add_more_container_0">
            <span class="is_visiting_any_doctors is_visiting_any_doctors_name"><input type="text" class="add_more_input_is_visiting_any_doctors" id="is_visiting_any_doctors_name_0" value="" name="data[IsVisitingAnyDoctor][doctor_name][]"></span>
            <span class="is_visiting_any_doctors is_visiting_any_doctors_speciality"><input type="text" class="add_more_input_is_visiting_any_doctors" id="is_visiting_any_doctors_speciality_0" value="" name="data[IsVisitingAnyDoctor][doctor_speciality][]"></span>
            <span class="is_visiting_any_doctors is_visiting_any_doctors_speciality"><input type="text" class="add_more_input_is_visiting_any_doctors" id="is_visiting_any_doctors_last_visit_date_0" value="" name="data[IsVisitingAnyDoctor][last_visit_date][]"></span>
        </div>
        <div style="clear: both;"></div>
            <?php  } ?>
    </div>
    <span id="is_visiting_any_doctors_add_more" class="add_more" onclick="addMoreOption_is_visiting_any_doctors('is_visiting_any_doctors');">Add More</span>

</div>
