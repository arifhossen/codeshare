<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {
            $("#loader").html('<div class="prompt_message" ><img src="<?php echo $this->webroot; ?>img/loadingAnimation.gif" align="absmiddle">&nbsp;Adding content... </div>');
            $("#MedicationAddForm").ajaxSubmit({
                success: function(responseText, responseCode) {
                    if(responseCode=='success'){
                        $('#report_form').hide().html('<div class="systems success prompt_message">The Medication saved</div>').fadeIn();
                        $('#report_container').prepend(responseText);
                        setTimeout(function(){$.prompt.close();}, 2000);
                    }
                }
            });
            return false;
        }
    });

    $().ready(function() {
        $("#MedicationAddForm").validate({
            rules: {
                "data[Medication][name]": {
                    required: true
                }
            },
            messages: {
                "data[Medication][name]": {
                    required: "This field cannot be left blank"
                }
            }
        });
    });

    /**************************************************************************************
     *  Dynamic add more option function
     *  return addmore html
     **************************************************************************************/
    function addMoreOption_taken_medication(name, total){
        var no =   $('.'+name+'_add_more_cancel').length+1;
        var max_add  = parseInt(total) - 1 ;
        var text = '<div id="'+name+'_add_more_container_'+no+'" class="taken_medication_container" > ';
        text += ' <span class="taken_medication taken_medication_name" ><input  type="text" name="data[taken_medication][drug_name][]" value="" id="taken_medication_name_'+no+'" class="add_more_input_taken_medication" ></span>';
        text += ' <span class="taken_medication taken_medication_strength" ><input  type="text" name="data[taken_medication][drug_strength][]" value="" id="taken_medication_strength_'+no+'" class="add_more_input_taken_medication" ></span>';
        text += ' <span class="taken_medication taken_medication_formulation" ><input  type="text" name="data[taken_medication][drug_formulation][]" value="" id="taken_medication_formulation_'+no+'" class="add_more_input_taken_medication" ></span>';
        text += ' <span class="taken_medication taken_medication_dose" ><input  type="text" name="data[taken_medication][dose][]" value="" id="dose_'+no+'" class="add_more_input_taken_medication" ></span>';
        text += ' <span class="taken_medication taken_medication_from" ><input  type="text" name="data[taken_medication][from][]" value="" id="from_'+no+'" class="add_more_input_taken_medication" ></span>';
        text += ' <span class="taken_medication taken_medication_to" ><input  type="text" name="data[taken_medication][to][]" value="" id="to_'+no+'" class="add_more_input_taken_medication" ></span>';
        text += ' <span class="taken_medication taken_medication_comment" ><input  type="text" name="data[taken_medication][comment][]" value="" id="comment_'+no+'" class="add_more_input_taken_medication" ></span>';


        text += ' <span class="'+name+'_add_more_cancel add_more_cancel"  id="'+name+'_cancel_'+no+'" onclick="addMoreOptionCancel(\''+name+'_add_more_container_'+no+'\', \''+no+'\', \''+name+'\')" >X</span>';
        text += '</div> <div style="clear: both;"></div>';

        $('#'+name+'_add_more_panel').append(text);
        // i++;
        //var no =   $('.'+name+'_add_more_cancel').length;
        if(no >= max_add) {
            $('#'+name+'_add_more').hide();
            no=1;
        }
    }


</script>
<style>
    .add_more_input_taken_medication{ width: 95%!important; padding: 2px;}
    .taken_medication_container_header{ margin: 5px; padding-bottom:7px; font-weight: bold;}
    .taken_medication_container{ margin: 5px; padding-bottom:7px;}
    .taken_medication{ float: left;}
    .taken_medication_name{width:20%;}
    .taken_medication_strength{width:15%;}
    .taken_medication_formulation{width: 15%;}
    .taken_medication_dose{width: 15%;}
    .taken_medication_from{width:8%;}
    .taken_medication_to{width:8%;}
    .taken_medication_comment{width:15%;}

</style>


<div class="" id="taken_medication_add_more_panel">
    <div class="taken_medication_container_header" style="padding: 0px;" >
        <span class="taken_medication taken_medication_name">Medication </span>
        <span class="taken_medication taken_medication_strength">Strength</span>
        <span class="taken_medication taken_medication_formulation">formulation</span>
        <span class="taken_medication taken_medication_dose">Dose</span>
        <span class="taken_medication taken_medication_from">From</span>
        <span class="taken_medication taken_medication_to">To</span>
        <span class="taken_medication taken_medication_comment">Comment</span>

    </div>
    <div style="clear: both;"></div>
    <?php //pr($drugHistory);
    if(isset($takenMedication) && !empty ($takenMedication)) {
        $i=0;
        foreach ($takenMedication as $list) {
            ?>
    <div class="taken_medication_container" id="taken_medication_add_more_container_<?php echo $i; ?>">
        <span class="taken_medication taken_medication_name"><input type="text" class="add_more_input_taken_medication" id="taken_medication_name_0" value="<?php echo $list['Medication']['drug_name']; ?>" name="data[taken_medication][drug_name][]"></span>
        <span class="taken_medication taken_medication_strength"><input type="text" class="add_more_input_taken_medication" id="taken_medication_strength_0" value="<?php echo $list['Medication']['drug_strength']; ?>" name="data[taken_medication][drug_strength][]"></span>
        <span class="taken_medication taken_medication_formulation"><input type="text" class="add_more_input_taken_medication" id="taken_medication_formulation_0" value="<?php echo $list['Medication']['drug_formulation']; ?>" name="data[taken_medication][drug_formulation][]"></span>
        <span class="taken_medication taken_medication_dose"><input type="text" class="add_more_input_taken_medication" id="dose_0" value="<?php echo $list['Medication']['dose']; ?>" name="data[taken_medication][dose][]"></span>
        <span class="taken_medication taken_medication_from"><input type="text" class="add_more_input_taken_medication" id="from_0" value="<?php echo $list['Medication']['from']; ?>" name="data[taken_medication][from][]"></span>
        <span class="taken_medication taken_medication_to"><input type="text" class="add_more_input_taken_medication" id="to_0" value="<?php echo $list['Medication']['to']; ?>" name="data[taken_medication][to][]"></span>
        <span class="taken_medication taken_medication_comment"><input type="text" class="add_more_input_taken_medication" id="comment_0" value="<?php echo $list['Medication']['comment']; ?>" name="data[taken_medication][comment][]"></span>
    <?php if($i!=0) {?>
        <span onclick="addMoreOptionCancel('taken_medication_add_more_container_<?php echo $i; ?>', '<?php echo $i; ?>', 'taken_medication')" id="taken_medication_cancel_<?php echo $i; ?>" class="taken_medication_add_more_cancel add_more_cancel">X</span>
   <?php } ?>
        </div> <div style="clear: both;"></div>
                <?php
                $i++;
            }
        } else {
            ?>
    <div class="taken_medication_container" id="taken_medication_add_more_container_0">
        <span class="taken_medication taken_medication_name"><input type="text" class="add_more_input_taken_medication" id="taken_medication_name_0" value="" name="data[taken_medication][drug_name][]"></span>
        <span class="taken_medication taken_medication_strength"><input type="text" class="add_more_input_taken_medication" id="taken_medication_strength_0" value="" name="data[taken_medication][drug_strength][]"></span>
        <span class="taken_medication taken_medication_formulation"><input type="text" class="add_more_input_taken_medication" id="taken_medication_formulation_0" value="" name="data[taken_medication][drug_formulation][]"></span>
        <span class="taken_medication taken_medication_dose"><input type="text" class="add_more_input_taken_medication" id="dose_0" value="" name="data[taken_medication][dose][]"></span>
        <span class="taken_medication taken_medication_from"><input type="text" class="add_more_input_taken_medication" id="from_0" value="" name="data[taken_medication][from][]"></span>
        <span class="taken_medication taken_medication_to"><input type="text" class="add_more_input_taken_medication" id="to_0" value="" name="data[taken_medication][to][]"></span>
        <span class="taken_medication taken_medication_comment"><input type="text" class="add_more_input_taken_medication" id="comment_0" value="" name="data[taken_medication][comment][]"></span>
    </div>
    <div style="clear: both;"></div>
        <?php  } ?>
</div>
<span id="taken_medication_add_more" class="add_more" onclick="addMoreOption_taken_medication('taken_medication');">Add More</span>

