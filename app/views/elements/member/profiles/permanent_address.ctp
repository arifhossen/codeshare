<div class="element">
    <div class="title">Permanent Address:</div>
    <div class="value"><?php echo $form->input('permanent_street_address',array('label'=>false,'div'=>false, 'rows'=>'2'));?>
        <label class="error" generated="true" for="ProfileStreetAddress"></label></div>
</div>

<div class="element">
    <div class="title">Postal code:</div>
    <div class="value"><?php echo $form->input('permanent_postal_code',array('label'=>false,'div'=>false));?>
        <label class="error" generated="true" for="ProfileStreetAddress"></label></div>
</div>

<div class="element">
    <div class="title">Police station:</div>
    <div class="value"><?php echo $form->input('permanent_police_station',array('label'=>false,'div'=>false));?>
        <label class="error" generated="true" for="ProfileCity"></label></div>
</div>

<div class="element">
    <div class="title">City:</div>
    <div class="value"><?php echo $form->input('permanent_city',array('label'=>false,'div'=>false));?>
        <label class="error" generated="true" for="ProfileCity"></label></div>
</div>

<div class="element">
    <div class="title">District:</div>
    <div class="value"><?php echo $form->input('permanent_district',array('label'=>false,'div'=>false));?>
        <label class="error" generated="true" for="ProfileCountry"></label></div>
</div>


