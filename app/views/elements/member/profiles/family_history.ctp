<script type="text/javascript">

    /**************************************************************************************
     *  Dynamic add more option function
     *  return addmore html
     **************************************************************************************/
    function addMoreOption_family_history(name, total){
        var no =   $('.'+name+'_add_more_cancel').length+1;
        var max_add  = parseInt(total) - 1 ;
        var text = '<div id="'+name+'_add_more_container_'+no+'" class="family_history_container" > ';
        text += ' <span class="family_history family_history_name" ><input  type="text" name="data[family_history][family_history_name][]" value="" id="family_history_name_'+no+'" class="add_more_input_family_history" ></span>';
        text += ' <span class="family_history family_history_speciality" ><input  type="text" name="data[family_history][family_history_speciality][]" value="" id="family_history_strength_'+no+'" class="add_more_input_family_history" ></span>';


        text += ' <span class="'+name+'_add_more_cancel add_more_cancel"  id="'+name+'_cancel_'+no+'" onclick="addMoreOptionCancel(\''+name+'_add_more_container_'+no+'\', \''+no+'\', \''+name+'\')" >X</span>';
        text += '</div> <div style="clear: both;"></div>';

        $('#'+name+'_add_more_panel').append(text);
        // i++;
        //var no =   $('.'+name+'_add_more_cancel').length;
        if(no >= max_add) {
            $('#'+name+'_add_more').hide();
            no=1;
        }
    }


</script>

<style>
    .add_more_input_family_history{ width: 95%!important; padding: 2px;}
    .family_history_container_header{ margin: 5px; padding-bottom:7px; font-weight: bold;}
    .family_history_container{ margin: 5px; padding-bottom:7px;}
    .family_history{ float: left;}
    .family_history_name{width: 150px;}
    .family_history_speciality{width: 570px;}
</style>

<div style="float: left; clear:both;">

    <div class="" id="family_history_add_more_panel">
        <div class="family_history_container_header" style="padding: 0px;" >
            <span class="family_history family_history_name">Family Member</span>
            <span class="family_history family_history_speciality">Medical Problems</span>

        </div>
        <div style="clear: both;"></div>
        <div class="family_history_container" id="family_history_add_more_container_0">
            <span class="family_history family_history_name">Mother:</span>
            <span class="family_history family_history_speciality">
                <?php
                 $other_value = '';
                //  pr($family_history);
                foreach ($family_history as $key=>$row) {
                    $checked ='';
                    $other_value = '';
                    if(isset($Profile['PhysicalInformation']['family_history_mother'])) {
                        $family_history_mother_val= explode(',',$Profile['PhysicalInformation']['family_history_mother']);
                        if(in_array($key, $family_history_mother_val)) {
                            $checked ='checked';
                        }
                        foreach ($family_history_mother_val as $v){
                            $val = explode('#', $v);
                            if(count($val)==2){
                                $other_value = $val[1];
                            }
                        }
                    }
                    echo '<input type="checkbox" '.$checked.'  name="family_history_mother[]" value="'.$key.'"  />'.$row.' ';
                }
                ?>
                <input type="text" name="family_history_mother_other" style="width: 150px;" value="<?php echo $other_value; ?>" />
            </span>
        </div>

        <div class="family_history_container" id="family_history_add_more_container_0">
            <span class="family_history family_history_name">Father:</span>
            <span class="family_history family_history_speciality">
                <?php
                //  pr($family_history);
                foreach ($family_history as $key=>$row) {
                    $checked ='';
                    $other_value = '';
                    if(isset($Profile['PhysicalInformation']['family_history_father'])) {
                        $family_history_father_val= explode(',',$Profile['PhysicalInformation']['family_history_father']);
                        if(in_array($key, $family_history_father_val)) {
                            $checked ='checked';
                        }
                        foreach ($family_history_father_val as $v){
                            $val = explode('#', $v);
                            if(count($val)==2){
                                $other_value = $val[1];
                            }
                        }
                    }
                      
                    echo '<input type="checkbox" '.$checked.'  name="family_history_father[]" value="'.$key.'"  />'.$row.' ';
                }
                ?>
                
                <input type="text" name="family_history_father_other" style="width: 150px;"value="<?php echo $other_value; ?>" />
            </span>
        </div>

        <div class="family_history_container" id="family_history_add_more_container_0">
            <span class="family_history family_history_name">Sisters:</span>
            <span class="family_history family_history_speciality">
                <?php
                //  pr($family_history);
                foreach ($family_history as $key=>$row) {
                    $checked ='';
                    $other_value = '';
                    if(isset($Profile['PhysicalInformation']['family_history_sisters'])) {
                        $family_history_sisters_val= explode(',',$Profile['PhysicalInformation']['family_history_sisters']);
                        if(in_array($key, $family_history_sisters_val)) {
                            $checked ='checked';
                        }
                          foreach ($family_history_sisters_val as $v){
                            $val = explode('#', $v);
                            if(count($val)==2){
                                $other_value = $val[1];
                            }
                        }

                    }
                    echo '<input type="checkbox" '.$checked.'  name="family_history_sisters[]" value="'.$key.'"  />'.$row.' ';
                }
                ?>
                <input type="text" name="family_history_sisters_other" style="width: 150px;"value="<?php echo $other_value; ?>" />
            </span>
        </div>

        <div class="family_history_container" id="family_history_add_more_container_0">
            <span class="family_history family_history_name">Brothers:</span>
            <span class="family_history family_history_speciality">
                 <?php
                //  pr($family_history);
                foreach ($family_history as $key=>$row) {
                    $checked ='';
                    $other_value = '';
                    if(isset($Profile['PhysicalInformation']['family_history_brothers'])) {
                        $family_history_brothers_val= explode(',',$Profile['PhysicalInformation']['family_history_brothers']);
                        if(in_array($key, $family_history_brothers_val)) {
                            $checked ='checked';
                        }
                          foreach ($family_history_brothers_val as $v){
                            $val = explode('#', $v);
                            if(count($val)==2){
                                $other_value = $val[1];
                            }
                        }
                    }
                    echo '<input type="checkbox" '.$checked.'  name="family_history_brothers[]" value="'.$key.'"  />'.$row.' ';
                }
                ?>
                <input type="text" name="family_history_brothers_other" style="width: 150px;"value="<?php echo $other_value; ?>" />
            </span>
        </div>



        <div style="clear: both;"></div>
    </div>

</div>
