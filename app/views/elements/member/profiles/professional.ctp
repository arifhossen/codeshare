<div class="element">
    <div class="title">Profession:</div>
    <div class="value">
        <?php $profession = array('House Wife'=>'House Wife','Private Service'=>'Private Service', 'Business'=>'Business','Student'=>'Student','Government Service'=>'Government Service','Other'=>'Other');
        $value = 'House Wife';
        $other_value = '';
        if(isset($Profile['Profile']['profession'])) {
            if(in_array($Profile['Profile']['profession'], $profession)) {
                $value = $Profile['Profile']['profession'];
            }
            else {
                $value = 'Other';
                $other_value = $Profile['Profile']['profession'];
            }
        }
        echo $form->input('profession',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
        'value'=>$value,
        'options'=>$profession));

        echo $form->input('profession_other',array('label'=>false,'value'=>$other_value,'div'=>false,'style'=>'width: 100px;'));
        ?>
    </div>
</div>
<div class="element">
    <div class="title">Monthly Income:</div>
    <div class="value">
        <?php $monthly_income = array('0'=>'0','<20k'=>'<20k', '<35k'=>'<35k','<50k'=>'<50k','<100k'=>'<100k','<1000k'=>'<1000k' );
        echo $form->input('monthly_income',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
        'value'=>isset ($Profile['Profile']['monthly_income'])?$Profile['Profile']['monthly_income']:"0",
        'options'=>$monthly_income));?>

    </div>
</div>

<div class="element">
    <div class="title">Organization Name:</div>
    <div class="value">
        <?php echo $form->input('organization_name',array('label'=>false,
        'value'=>isset ($Profile['Profile']['organization_name'])?$Profile['Profile']['organization_name']:"",
        'div'=>false));?>
        <label class="error" generated="true" for="ProfileProfession"></label>
    </div>
</div>


<div class="element">
    <div class="title">Position:</div>
    <div class="value">
        <?php echo $form->input('position',array('label'=>false,
        'value'=>isset ($Profile['Profile']['position'])?$Profile['Profile']['position']:"",
        'div'=>false));?>
        <label class="error" generated="true" for="ProfileCompany"></label>
    </div>
</div>

<div class="element">
    <div class="title">Phone:</div>
    <div class="value">
        <?php echo $form->input('office_phone',array('label'=>false,
        'value'=>isset ($Profile['Profile']['office_phone'])?$Profile['Profile']['office_phone']:"",
        'div'=>false));?>
        <label class="error" generated="true" for="ProfileBusinessPhone"></label>
    </div>
</div>

<div class="element">
    <div class="title">Office Address:</div>
    <div class="value">
        <?php echo $form->input('office_address',array('label'=>false,'div'=>false,
        'value'=>isset ($Profile['Profile']['office_address'])?$Profile['Profile']['office_address']:"",
        'type'=>'textarea'));?>
        <label class="error" generated="true" for="ProfileBusinessPhone"></label>
    </div>
</div>