<div class="element">
    <div class="title">First Name:</div>
    <div class="value">
        <?php echo $form->input('first_name',array('label'=>false,'div'=>false));?>
     <br/>   <label class="error" generated="true" for="ProfileFirstName"></label>
    </div>
</div>
<div class="element">
    <div class="title">Last Name:</div>
    <div class="value">
        <?php echo $form->input('last_name',array('label'=>false,'div'=>false));?>
     <br/>   <label class="error" generated="true" for="ProfileLastName"></label>
    </div>
</div>
<div class="element">
    <div class="title">Date of birth:</div>
    <div class="value">  <?php
        $current_year = date('Y');
        $max_year = $current_year - 50;
        echo $form->input('dob', array('label'=>false,'div'=>false,'type'=>'date', 'minYear'=>date('Y'), 'maxYear'=>$max_year));
        ?>
    </div>
</div>
<div class="element">
    <div class="title">Gender:</div>
    <div class="value"> 
          <?php echo $form->input('gender',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
              'value'=>isset ($Profile['Profile']['gender'])?$Profile['Profile']['gender']:"Male",
                'options'=>array('Male'=>'Male','Female'=>'Female')));?>
    </div>
</div>
<div class="element">
    <div class="title">Marital status:</div>
    <div class="value">
         <?php $marital_status = array('Never Married'=>'Never Married','Married'=>'Married', 'Single'=>'Single');
         echo $form->input('marital_status',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
             'value'=>isset ($Profile['Profile']['marital_status'])?$Profile['Profile']['marital_status']:"Never Married",
             'options'=>$marital_status));?>
    </div>
</div>

<div class="element">
    <div class="title">Religion:</div>
    <div class="value">
         <?php $religion = array('Islam'=>'Islam','Hindu'=>'Hindu', 'Christian'=>'Christian','Buddhism'=>'Buddhism','Other'=>'Other');
          $value = 'Islam';
        $other_value = '';
        if(isset($Profile['Profile']['religion'])) {
            if(in_array($Profile['Profile']['religion'], $religion)) {
                $value = $Profile['Profile']['religion'];
            }
            else {
                $value = 'Other';
                $other_value = $Profile['Profile']['religion'];
            }
        }
        echo $form->input('religion',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
        'value'=>$value,
        'options'=>$religion));

        echo $form->input('religion_other',array('label'=>false,'value'=>$other_value,'div'=>false,'style'=>'width: 100px;'));
        ?>
    </div>
</div>

<div class="element">
    <div class="title">Nationality:</div>
    <div class="value">
        <?php echo $form->input('nationality',array('label'=>false,'div'=>false));?>
        <label class="error" generated="true" for="ProfileProfession"></label>
    </div>
</div>


<div class="element">
    <div class="title">Father's Name:</div>
    <div class="value">
        <?php echo $form->input('father_name',array('label'=>false,'div'=>false));?>
        <label class="error" generated="true" for="ProfileName"></label>
    </div>
</div>

<div class="element">
    <div class="title">Mother's Name:</div>
    <div class="value">
        <?php echo $form->input('mother_name',array('label'=>false,'div'=>false));?>
        <label class="error" generated="true" for="ProfileName"></label>
    </div>
</div>

<div class="element">
    <div class="title">Spouse's Name:</div>
    <div class="value">
        <?php echo $form->input('spouse_name',array('label'=>false,'div'=>false));?>
        <label class="error" generated="true" for="ProfileName"></label>
    </div>
</div>


<div class="element">
    <div class="title">Phone(M):</div>
    <div class="value">
        <?php echo $form->input('mobile',array('label'=>false,'div'=>false));?>
        <label class="error" generated="true" for="ProfileMobile"></label>
    </div>
</div>
<div class="element">
    <div class="title">Phone(H):</div>
    <div class="value">
        <?php echo $form->input('home_phone',array('label'=>false,'div'=>false));?>
        <label class="error" generated="true" for="ProfileMobile"></label>
    </div>
</div>

<div class="element">
    <div class="title">Email:</div>
    <div class="value">
        <?php echo $form->input('email',array('label'=>false,'div'=>false));?>
        <label class="error" generated="true" for="ProfileMobile"></label>
    </div>
</div>

