<div style="float:left; width:25%; ">
    <div class="element">
        <div class="title">Weight:</div>
        <div class="value"><?php echo $form->input('weight',array('label'=>false,
            'value'=>isset ($Profile['PhysicalInformation']['weight'])?$Profile['PhysicalInformation']['weight']:'', 'div'=>false));?>kg
            <label class="error" generated="true" for="weight"></label></div>
    </div>
</div>

<div style="float:left; width:25%; ">
    <div class="element">
        <div class="title">Height:</div>
        <div class="value"><?php echo $form->input('height',array('label'=>false,'div'=>false,
            'value'=>isset ($Profile['PhysicalInformation']['weight'])?$Profile['PhysicalInformation']['height']:''));?>cm
            <label class="error" generated="true" for="height_cm"></label></div>
    </div>
</div>
<div style="float:left; width:40%; ">
    <div class="element" style="clear: both;">
        <div class="title">Blood group:</div>
        <div class="value"><?php echo $form->input('blood_group',array('label'=>false,'div'=>false,
            'value'=>isset ($Profile['PhysicalInformation']['weight'])?$Profile['PhysicalInformation']['blood_group']:''));?></div>
    </div>
</div>

<div style="clear: both;"></div>

<div class="element single">
    1. How would you describe your health?
    <?php $how_describ_your_health = array('Excellent'=>'Excellent',
            'Very Good'=>'Very Good',
            'Fair'=>'Fair',
            'Poor'=>'Poor');
    echo $form->input('how_describ_your_health',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
    'value'=>isset ($Profile['PhysicalInformation']['how_describ_your_health'])?$Profile['PhysicalInformation']['how_describ_your_health']:'Excellent',
    'options'=>$how_describ_your_health));?>

</div>

<div class="element single">
    2. Do you have any disease that is known to you?
    <?php echo $form->input('is_have_known_disease',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
    'value'=>isset ($Profile['PhysicalInformation']['is_have_known_disease'])?$Profile['PhysicalInformation']['is_have_known_disease']:'0',
    'options'=>array('1'=>'Yes','0'=>'No')));?>
</div>

<div class="element single">
    Diagnosis (if yes): <br/>
    <div style="float: left; clear:both;">

        <?php
        //  pr($CommonList);
        foreach ($previous_diagnosis_list as $key=>$row) {
            $checked = '';
            if(isset($Profile['PhysicalInformation']['previous_diagnosis'])) {
                $previous_diagnosis = explode(',',$Profile['PhysicalInformation']['previous_diagnosis']);
                if(in_array($key, $previous_diagnosis)) {
                    $checked ='checked';
                }
            }
            echo '<span class="checkbox_list"><input type="checkbox" '.$checked.' name="previous_diagnosis[]" value="'.$key.'"  />'.$row.'</span>';
        }
        ?>
    </div>
</div>

<div class="element single">
    <div style="float: left; clear:both; margin-top: 20px;">
        3. Are you visiting any doctors?
        <?php echo $form->input('is_visiting_any_doctor',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
        'value'=>isset ($Profile['PhysicalInformation']['is_visiting_any_doctor'])?$Profile['PhysicalInformation']['is_visiting_any_doctor']:'0',
        'options'=>array('1'=>'Yes','0'=>'No')));?>

        <br/>
        <?php echo $this->element('member/profiles/is_visiting_any_doctor') ?>
    </div>
</div>

<div class="element single">
    <div style="float: left; clear:both; margin-top: 20px;">
        4. Drug History
        <?php echo $this->element('member/profiles/medication') ?>
    </div>
</div>


<div class="element single">
    <div style="float: left; clear:both; margin-top: 20px;">
        5. What over-the-counter medicines, do you take regularly?
        <div style="clear: both; margin-left: 20px;">
            <?php
            //  pr($CommonList);
            foreach ($taken_medication_regularly as $key=>$row) {
                $checked = '';
                if(isset($Profile['PhysicalInformation']['taken_medication_regularly'])) {
                    $taken_medication_regularly = explode(',',$Profile['PhysicalInformation']['taken_medication_regularly']);
                    if(in_array($key, $taken_medication_regularly)) {
                        $checked ='checked';
                    }
                }
                echo '<input type="checkbox" '.$checked.' name="taken_medication_regularly[]" value="'.$key.'"  />'.$row.'<br/>';
            }
            ?></div>
        <div style="clear: both;">List if you take any </div>
        <?php echo $this->element('member/profiles/taken_medication') ?>
    </div>
</div>

<div class="element single">
    6. Do you take non allopathic Medicine?
    <?php echo $form->input('is_taken_non_allopathic_medicine',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
    'value'=>isset ($Profile['PhysicalInformation']['is_taken_non_allopathic_medicine'])?$Profile['PhysicalInformation']['is_taken_non_allopathic_medicine']:'0',
    'options'=>array('1'=>'Yes','0'=>'No')));?>
</div>



<div class="element single">
    7. Do you require palliative or nursing care?
    <?php echo $form->input('is_require_nursing_care',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
    'value'=>isset ($Profile['PhysicalInformation']['is_require_nursing_care'])?$Profile['PhysicalInformation']['is_require_nursing_care']:'0',
    'options'=>array('1'=>'Yes','0'=>'No')));?>
</div>

<div class="element single">
    8. List of past investigations
    <?php echo $this->element('member/profiles/investigation') ?>
</div>

<div class="element single">
    9. Have you ever had any allergic reaction to a medicine or a shot? <br/>
    <?php echo $form->input('is_any_allergic_reaction',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
    'value'=>isset ($Profile['PhysicalInformation']['is_any_allergic_reaction'])?$Profile['PhysicalInformation']['is_any_allergic_reaction']:'0',
    'options'=>array('1'=>'Yes. (Please write the name of the medicine and the effect you had.)','0'=>'No, I am not allergic to any medicines.')));?>
    <?php echo $this->element('member/profiles/allergic_reaction') ?>
</div>


<div class="element single">
    10. Do you get an allergic reaction from any of the following? (Check all that apply) <br/>
    <div style="float: left; margin-left: 20px; ">
        <?php
        //  pr($CommonList);
        foreach ($allergic_reaction_from as $key=>$row) {
            $checked = '';
            if(isset($Profile['PhysicalInformation']['allergic_reaction_from'])) {
                $allergic_reaction= explode(',',$Profile['PhysicalInformation']['allergic_reaction_from']);
                if(in_array($key, $allergic_reaction)) {
                    $checked ='checked';
                }
            }
            echo '<input type="checkbox" '.$checked.' name="allergic_reaction_from[]" value="'.$key.'"  />'.$row.'<br/>';
        }
        ?>

    </div>
    <?php echo $form->input('no_allergic_note',array('label'=>false,'div'=>false, 'type'=>'textarea', 'rows'=>4,
    'value'=>isset ($Profile['PhysicalInformation']['no_allergic_note'])?$Profile['PhysicalInformation']['no_allergic_note']:'',
    )); ?>
</div>
<div class="element single">
    11.	Have you ever been a patient in a hospital?
    <?php echo $form->input('is_ever_patient',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
    'value'=>isset ($Profile['PhysicalInformation']['is_ever_patient'])?$Profile['PhysicalInformation']['is_ever_patient']:'0',
    'options'=>array('1'=>'Yes','0'=>'No, I have never been a patient in a hospital.')));?>
    <?php echo $this->element('member/profiles/hospitalization') ?>
</div>


<div class="element single">
    12.	Have you ever received a blood transfusion?
    <?php echo $form->input('is_ever_received_blood_transfusion',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
    'value'=>isset ($Profile['PhysicalInformation']['is_ever_received_blood_transfusion'])?$Profile['PhysicalInformation']['is_ever_received_blood_transfusion']:'0',
    'options'=>array('1'=>'Yes','0'=>'No')));?>
    Last transfusion Date:<?php
    $current_year = date('Y');
    $max_year = $current_year - 50;
    echo $form->input('last_transfusion_date', array('label'=>false,'div'=>false,'type'=>'date',
    'value'=>isset ($Profile['PhysicalInformation']['last_transfusion_date'])?$Profile['PhysicalInformation']['last_transfusion_date']:date('Y-m-d'),
    'minYear'=>date('Y'), 'maxYear'=>$max_year));
    ?>
</div>


<div class="element single">
    13.	When was your last Tetanus shot?
    <?php
    $current_year = date('Y');
    $max_year = $current_year - 50;
    echo $form->input('last_tetanus_shot', array('label'=>false,'div'=>false,'type'=>'date', 'minYear'=>date('Y'),
    'value'=>isset ($Profile['PhysicalInformation']['last_tetanus_shot'])?$Profile['PhysicalInformation']['last_tetanus_shot']:date('Y-m-d'),
    'maxYear'=>$max_year));
    ?>
</div>
<div class="element single">
    14.	When was your last Pneumonia shot?
    <?php
    $current_year = date('Y');
    $max_year = $current_year - 50;
    echo $form->input('last_pneumonia_shot', array('label'=>false,'div'=>false,'type'=>'date', 'minYear'=>date('Y'),
    'value'=>isset ($Profile['PhysicalInformation']['last_pneumonia_shot'])?$Profile['PhysicalInformation']['last_pneumonia_shot']:date('Y-m-d'),
    'maxYear'=>$max_year));
    ?>
</div>


<div class="element single">
    15.	When was your last Flu shot?
    <?php
    $current_year = date('Y');
    $max_year = $current_year - 50;
    echo $form->input('last_flu_shot', array('label'=>false,'div'=>false,'type'=>'date', 'minYear'=>date('Y'),
    'value'=>isset ($Profile['PhysicalInformation']['last_flu_shot'])?$Profile['PhysicalInformation']['last_flu_shot']:date('Y-m-d'),
    'maxYear'=>$max_year));
    ?>
</div>
<div class="element single">
    16. Do you have any beliefs or practices from your religion, culture, or otherwise that your doctor should know?
    <div style="float: left; clear:both; margin-left: 20px;">
        <?php
        //  pr($CommonList);
        foreach ($have_any_beliefs_or_practices as $key=>$row) {
             $checked ='';
              if(isset($Profile['PhysicalInformation']['have_any_beliefs_or_practice'])) {
                $have_any_beliefs_or_practice_val= explode(',',$Profile['PhysicalInformation']['have_any_beliefs_or_practice']);
                if(in_array($key, $have_any_beliefs_or_practice_val)) {
                    $checked ='checked';
                }
            }
            echo '<input type="checkbox" '.$checked.'  name="have_any_beliefs_or_practice[]" value="'.$key.'"  />'.$row.'<br/>';
        }
        ?>
    </div>
</div>


<div style="float: left; clear:both; "></div>
<div class="element single">
    17.	Exercise Routine<br/>
    <div style="float: left; margin-left: 20px; width: 220px;">
        Describe what kind of exercise you do. (Check all that apply.) <br/>

        <input type="checkbox" name="exercise_type[]" value="walking"  />walking<br/>
        <input type="checkbox" name="exercise_type[]" value="biking"  />biking<br/>
        <input type="checkbox" name="exercise_type[]" value="swimming" />swimming<br/>
        <input type="checkbox" name="exercise_type[]" value="weight training" />weight training<br/>
        <input type="checkbox" name="exercise_type[]" value="yoga" />yoga<br/>
        <input type="checkbox" name="exercise_type[]" value="I do not exercise" />I do not exercise<br/>
    </div>

    <div style="float: left; margin-left: 20px; width: 220px;">
        How many days per week do you exercise? <br/>
        <input type="checkbox" name="how_time_exercise[]" value="once per week"  />once per week<br/>
        <input type="checkbox" name="how_time_exercise[]" value="twice per week"  />twice per week<br/>
        <input type="checkbox" name="how_time_exercise[]" value="3 times a week"  />3 times a week<br/>
        <input type="checkbox" name="how_time_exercise[]" value="4 times a week"  />4 times a week<br/>
        <input type="checkbox" name="how_time_exercise[]" value="5 times a week"  />5 times a week<br/>
        <input type="checkbox" name="how_time_exercise[]" value="6 times a week"  />6 times a week<br/>
        <input type="checkbox" name="how_time_exercise[]" value="6 times a week"  />7 times a week or more<br/>

    </div>


    <div style="float: left; margin-left: 20px; width: 220px;">
        For how long do you exercise each day?<br/>
        <input type="checkbox" name="how_long_exercise[]" value="less than 15 minutes"  />less than 15 minutes<br/>
        <input type="checkbox" name="how_long_exercise[]" value="15-30 minutes"  />15-30 minutes<br/>
        <input type="checkbox" name="how_long_exercise[]" value="30 – 45 minutes"  />30 – 45 minutes<br/>
        <input type="checkbox" name="how_long_exercise[]" value="45 minutes – 1 hour"  />45 minutes – 1 hour<br/>
        <input type="checkbox" name="how_long_exercise[]" value="over 1 hour"  />over 1 hour<br/>
    </div>
</div>

<div class="element single">
    18.	Have you ever smoked cigarettes, cigars or betel leafs or chewed tobacco?
    <?php echo $form->input('is_ever_smoking',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
    'value'=>isset ($Profile['PhysicalInformation']['is_visiting_any_doctor'])?$Profile['PhysicalInformation']['is_visiting_any_doctor']:'0',
    'options'=>array('1'=>'Yes','0'=>'No')));?>
    <div style="float: left; margin: 20px;">

        a. When did you start?	Date  <?php
        $current_year = date('Y');
        $max_year = $current_year - 50;
        echo $form->input('smoking_start_date', array('label'=>false,'div'=>false,'type'=>'date', 'minYear'=>date('Y'), 'maxYear'=>$max_year));
        ?><br/>
        b. How many per week?<?php echo $form->input('smoking_per_weeek',array('label'=>false,'div'=>false,'style'=>'width: 50px;'));?>	<br/>
        c. Have you quit?
        <?php echo $form->input('is_smoking_quit',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
        'value'=>isset ($Profile['PhysicalInformation']['is_visiting_any_doctor'])?$Profile['PhysicalInformation']['is_visiting_any_doctor']:'0',
        'options'=>array('1'=>'Yes','0'=>'No')));?>
        <?php
        $current_year = date('Y');
        $max_year = $current_year - 50;
        echo $form->input('smoking_quit_date', array('label'=>false,'div'=>false,'type'=>'date', 'minYear'=>date('Y'), 'maxYear'=>$max_year));
        ?>
        <br/>
        d. Do you want to quit?	 <?php echo $form->input('is_smoking_want_to_quit',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
        'value'=>isset ($Profile['PhysicalInformation']['is_visiting_any_doctor'])?$Profile['PhysicalInformation']['is_visiting_any_doctor']:'0',
        'options'=>array('1'=>'Yes','0'=>'No')));?>
    </div>


</div>


<div class="element single">
    19.	Do you drink alcohol?
    <?php echo $form->input('is_drink_alcohol',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio','value'=>'No',
    'options'=>array('Regular'=>'Regular','Social'=>'Social','No'=>'No' )));?>
</div>

<h2>For Women Only</h2>

<div class="element single">
    20. Have you ever been pregnant?
    <?php echo $form->input('is_ever_pregnant',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
    'value'=>isset ($Profile['PhysicalInformation']['is_visiting_any_doctor'])?$Profile['PhysicalInformation']['is_visiting_any_doctor']:'0',
    'options'=>array('1'=>'Yes','0'=>'No')));?>
    <div style="clear: both;"></div>
    <div style="float: left; margin: 20px; clear: both;">
       	a. Number of pregnancies <?php echo $form->input('number_of_pregnancy',array('label'=>false,'div'=>false,'style'=>'width: 50px;'));?>
	b. Number of Birth <?php echo $form->input('number_of_birth',array('label'=>false,'div'=>false,'style'=>'width: 50px;'));?>
	c. Number of abortions <?php echo $form->input('number_of_abortion',array('label'=>false,'div'=>false,'style'=>'width: 50px;'));?>

    </div>
</div>

<div class="element single">
    21. Is your menstrual cycle regular?
    <?php echo $form->input('is_regular_menstrual_cycle',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
    'value'=>isset ($Profile['PhysicalInformation']['is_visiting_any_doctor'])?$Profile['PhysicalInformation']['is_visiting_any_doctor']:'0',
    'options'=>array('1'=>'Yes','0'=>'No')));?>
</div>


<div class="element single">
    22. Have you had a PAP smear?
    <?php echo $form->input('is_had_pap_smear',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio','value'=>'No',
    'options'=>array('No'=>'No','Normal'=>'Normal','Abnormal'=>'Abnormal' )));?>
    Date:<?php
    $current_year = date('Y');
    $max_year = $current_year - 50;
    echo $form->input('pap_smear_date', array('label'=>false,'div'=>false,'type'=>'date', 'minYear'=>date('Y'), 'maxYear'=>$max_year));
    ?>
</div>

<div class="element single">
    23.	Have you had a mammogram?
    <?php echo $form->input('is_had_mammogram',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio','value'=>'No',
    'options'=>array('No'=>'No','Normal'=>'Normal','Abnormal'=>'Abnormal' )));?>
    Date:<?php
    $current_year = date('Y');
    $max_year = $current_year - 50;
    echo $form->input('mammogram_date', array('label'=>false,'div'=>false,'type'=>'date', 'minYear'=>date('Y'), 'maxYear'=>$max_year));
    ?>
</div>