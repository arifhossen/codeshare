<div style="float: left; width:63%; height: 124px;">
    <legend></legend>

    <?php if(!empty($this->data['Profile']['afc_member_id'])) { ?>
    <div class="element">
        <div class="title">ID:</div>
        <div class="value">
                <?php echo $this->data['Profile']['afc_member_id'];?>
            <br/>  <label class="error" generated="true" for="ProfileName"></label>
        </div>
    </div>
        <?php } ?>

    <div class="element">
        <div class="title">National ID:</div>
        <div class="value">
            <?php echo $form->input('national_id',array('label'=>false,'div'=>false,'type'=>'text'));?>
            <br/>  <label class="error" generated="true" for="ProfileName"></label>
        </div>
    </div>


    <div style="clear: both;"></div>
    <div class="element">
        <div class="title">Date:</div>
        <div class="value">  <?php
            $current_year = date('Y');
            $max_year = $current_year - 50;
            echo $form->input('created', array('label'=>false,'div'=>false,'type'=>'date', 'minYear'=>2020, 'maxYear'=>$max_year));
            ?>
        </div>
    </div>

    <div class="element">
        <div class="title">Membership Type:</div>
        <div class="value">
            <?php
            echo $form->input('user_type',array('label'=>false,'div'=>false,'legend'=>false,'fieldset'=>false,'type'=>'radio',
            'value'=>isset($Profile['User']['user_type'])?$Profile['User']['user_type']:'',
            'options'=>array('Regular'=>'Regular','Gold'=>'Gold','Platinum'=>'Platinum')));?>
        </div>
    </div>
</div>
<div style="float: left; width: 28%;">
    <legend></legend>
    <div class="element">
        <?php if(isset($Profile['Profile']['profile_image'])) {?> <img class="profile" src="<?php echo $this->webroot;?>app/webroot/files/profile_image/<?php echo $Profile['Profile']['profile_image']; ?>" alt="Profile Image" style="width:100px; height: 100px;" />
            <?php } else  echo $this->Html->image('no_image.jpg',array('alt'=>'no image','class'=>'profile_image','align'=>'middle')); ?>
        <?php echo $this->Form->input("upload_image",array('label'=>false,'div'=>false,'type'=>'file'));?>
    </div>
</div>