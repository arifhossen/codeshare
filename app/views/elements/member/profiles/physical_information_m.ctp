<div style="float:left; width:25%; ">

    <?php echo $form->hidden('physical_information_id', array('label' => false,
        'value' => isset($Profile['PhysicalInformation']['id']) ? $Profile['PhysicalInformation']['id'] : '', 'div' => false)); ?>
    <div class="element">
        <div class="title">Weight:</div>
        <div class="value"><?php echo $form->input('weight', array('label' => false,
        'value' => isset($Profile['PhysicalInformation']['weight']) ? $Profile['PhysicalInformation']['weight'] : '', 'div' => false)); ?> kg
            <label class="error" generated="true" for="weight"></label></div>
    </div>
</div>

<div style="float:left; width:25%; ">
    <div class="element">
        <div class="title">Height:</div>
        <div class="value"><?php echo $form->input('height', array('label' => false, 'div' => false,
                'value' => isset($Profile['PhysicalInformation']['weight']) ? $Profile['PhysicalInformation']['height'] : '')); ?> cm
            <label class="error" generated="true" for="height_cm"></label></div>
    </div>
</div>
<div style="float:left; width:40%; ">
    <div class="element" style="clear: both;">
        <div class="title">Blood group:</div>
        <div class="value"><?php echo $form->input('blood_group', array('label' => false, 'div' => false,
                'value' => isset($Profile['PhysicalInformation']['weight']) ? $Profile['PhysicalInformation']['blood_group'] : '')); ?></div>
    </div>
</div>

<div style="clear: both;"></div>

<ol  style="padding-left: 20px;">
    <li>How would you describe your health?
        <?php
            $how_describ_your_health = array('Excellent' => 'Excellent',
                'Very Good' => 'Very Good',
                'Fair' => 'Fair',
                'Poor' => 'Poor');
            echo $form->input('how_describ_your_health', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                'value' => isset($Profile['PhysicalInformation']['how_describ_your_health']) ? $Profile['PhysicalInformation']['how_describ_your_health'] : 'Excellent',
                'options' => $how_describ_your_health));
        ?>
        </li>
        <li>
            Do you have any disease that is known to you?
        <?php
            echo $form->input('is_have_known_disease', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                'value' => isset($Profile['PhysicalInformation']['is_have_known_disease']) ? $Profile['PhysicalInformation']['is_have_known_disease'] : '0',
                'options' => array('1' => 'Yes', '0' => 'No')));
        ?><br/>
            Diagnosis (if yes): <br/>
            <div>
            <?php
            //  pr($CommonList);
            foreach ($previous_diagnosis_list as $key => $row) {
                $checked = '';
                if (isset($Profile['PhysicalInformation']['previous_diagnosis'])) {
                    $previous_diagnosis = explode(',', $Profile['PhysicalInformation']['previous_diagnosis']);
                    if (in_array($key, $previous_diagnosis)) {
                        $checked = 'checked';
                    }
                }
                echo '<span><input type="checkbox" ' . $checked . ' name="previous_diagnosis[]" value="' . $key . '"  />' . $row . '</span>';
            }
            ?></div>
    </li>
    <li>
        Are you visiting any doctors?
        <?php
            echo $form->input('is_visiting_any_doctor', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                'value' => isset($Profile['PhysicalInformation']['is_visiting_any_doctor']) ? $Profile['PhysicalInformation']['is_visiting_any_doctor'] : '0',
                'options' => array('1' => 'Yes', '0' => 'No')));
        ?>

            <br/>
        <?php echo $this->element('member/profiles/is_visiting_any_doctor') ?>
        </li>
        <li>
            Drug History
        <?php echo $this->element('member/profiles/medication') ?>
        </li>
        <li>
            What over-the-counter medicines, do you take regularly?
            <div style="clear: both; margin-left: 20px;">
            <?php
            //  pr($CommonList);
            foreach ($taken_medication_regularly as $key => $row) {
                $checked = '';
                if (isset($Profile['PhysicalInformation']['taken_medication_regularly'])) {
                    $taken_medication_regularly = explode(',', $Profile['PhysicalInformation']['taken_medication_regularly']);
                    if (in_array($key, $taken_medication_regularly)) {
                        $checked = 'checked';
                    }
                }
                echo '<input type="checkbox" ' . $checked . ' name="taken_medication_regularly[]" value="' . $key . '"  />' . $row . '<br/>';
            }
            ?>
            <br />
            <div style="clear: both;">List if you take any </div>
            <?php echo $this->element('member/profiles/taken_medication') ?>
        </div>
    </li>
    <li>Do you take non allopathic Medicine?
        <?php
            echo $form->input('is_taken_non_allopathic_medicine', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                'value' => isset($Profile['PhysicalInformation']['is_taken_non_allopathic_medicine']) ? $Profile['PhysicalInformation']['is_taken_non_allopathic_medicine'] : '0',
                'options' => array('1' => 'Yes', '0' => 'No'))); ?>
        </li>
        <li>Do you require palliative or nursing care?
        <?php
            echo $form->input('is_require_nursing_care', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                'value' => isset($Profile['PhysicalInformation']['is_require_nursing_care']) ? $Profile['PhysicalInformation']['is_require_nursing_care'] : '0',
                'options' => array('1' => 'Yes', '0' => 'No'))); ?>
        </li>
        <li>List of past investigations
        <?php echo $this->element('member/profiles/investigation') ?>
        </li>
        <li>Have you ever had any allergic reaction to a medicine or a shot? <br/>
        <?php
            echo $form->input('is_any_allergic_reaction', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                'value' => isset($Profile['PhysicalInformation']['is_any_allergic_reaction']) ? $Profile['PhysicalInformation']['is_any_allergic_reaction'] : '0',
                'options' => array('1' => 'Yes. (Please write the name of the medicine and the effect you had.)', '0' => 'No, I am not allergic to any medicines.'))); ?>
        <?php echo $this->element('member/profiles/allergic_reaction') ?>
        </li>
        <li>Do you get an allergic reaction from any of the following? (Check all that apply) <br/>
        <?php
            //  pr($CommonList);
            foreach ($allergic_reaction_from as $key => $row) {
                $checked = '';
                if (isset($Profile['PhysicalInformation']['allergic_reaction_from'])) {
                    $allergic_reaction = explode(',', $Profile['PhysicalInformation']['allergic_reaction_from']);
                    if (in_array($key, $allergic_reaction)) {
                        $checked = 'checked';
                    }
                }
                echo '<input type="checkbox" ' . $checked . ' name="allergic_reaction_from[]" value="' . $key . '"  />' . $row . '<br/>';
            }
        ?>
            <br />
        <?php
            echo $form->input('no_allergic_note', array('label' => false, 'div' => false, 'type' => 'textarea', 'rows' => 4,
                'value' => isset($Profile['PhysicalInformation']['no_allergic_note']) ? $Profile['PhysicalInformation']['no_allergic_note'] : '',
            ));
        ?>
        </li>
        <li>Have you ever been a patient in a hospital?
        <?php
            echo $form->input('is_ever_patient', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                'value' => isset($Profile['PhysicalInformation']['is_ever_patient']) ? $Profile['PhysicalInformation']['is_ever_patient'] : '0',
                'options' => array('1' => 'Yes', '0' => 'No, I have never been a patient in a hospital.'))); ?>
        <?php echo $this->element('member/profiles/hospitalization') ?>
        </li>
        <li>Have you ever received a blood transfusion?
        <?php
            echo $form->input('is_ever_received_blood_transfusion', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                'value' => isset($Profile['PhysicalInformation']['is_ever_received_blood_transfusion']) ? $Profile['PhysicalInformation']['is_ever_received_blood_transfusion'] : '0',
                'options' => array('1' => 'Yes', '0' => 'No'))); ?>
            Last transfusion Date:<?php
            $current_year = date('Y');
            $max_year = $current_year - 50;
            echo $form->input('last_transfusion_date', array('label' => false, 'div' => false, 'type' => 'date',
                'value' => isset($Profile['PhysicalInformation']['last_transfusion_date']) ? $Profile['PhysicalInformation']['last_transfusion_date'] : date('Y-m-d'),
                'minYear' => date('Y'), 'maxYear' => $max_year));
        ?>
        </li>
        <li>When was your last Tetanus shot?
        <?php
            $current_year = date('Y');
            $max_year = $current_year - 50;
            echo $form->input('last_tetanus_shot', array('label' => false, 'div' => false, 'type' => 'date', 'minYear' => date('Y'),
                'value' => isset($Profile['PhysicalInformation']['last_tetanus_shot']) ? $Profile['PhysicalInformation']['last_tetanus_shot'] : date('Y-m-d'),
                'maxYear' => $max_year));
        ?>
        </li>
        <li>When was your last Pneumonia shot?
        <?php
            $current_year = date('Y');
            $max_year = $current_year - 50;
            echo $form->input('last_pneumonia_shot', array('label' => false, 'div' => false, 'type' => 'date', 'minYear' => date('Y'),
                'value' => isset($Profile['PhysicalInformation']['last_pneumonia_shot']) ? $Profile['PhysicalInformation']['last_pneumonia_shot'] : date('Y-m-d'),
                'maxYear' => $max_year));
        ?>
        </li>
        <li>When was your last Flu shot?
        <?php
            $current_year = date('Y');
            $max_year = $current_year - 50;
            echo $form->input('last_flu_shot', array('label' => false, 'div' => false, 'type' => 'date', 'minYear' => date('Y'),
                'value' => isset($Profile['PhysicalInformation']['last_flu_shot']) ? $Profile['PhysicalInformation']['last_flu_shot'] : date('Y-m-d'),
                'maxYear' => $max_year));
        ?>
        </li>
        <li>Do you have any beliefs or practices from your religion, culture, or otherwise that your doctor should know?
            <div style="float: left; clear:both; margin-left: 20px;">
            <?php
            //  pr($CommonList);
            foreach ($have_any_beliefs_or_practices as $key => $row) {
                $checked = '';
                if (isset($Profile['PhysicalInformation']['have_any_beliefs_or_practice'])) {
                    $have_any_beliefs_or_practice_val = explode(',', $Profile['PhysicalInformation']['have_any_beliefs_or_practice']);
                    if (in_array($key, $have_any_beliefs_or_practice_val)) {
                        $checked = 'checked';
                    }
                }
                echo '<input type="checkbox" ' . $checked . '  name="have_any_beliefs_or_practice[]" value="' . $key . '"  />' . $row . '<br/>';
            }
            ?>
    </li>
    <li>Exercise Routine<br/>
        <div>
            <div style="float: left; margin-left: 20px; width: 220px;">
                Describe what kind of exercise you do. (Check all that apply.) <br/>
                <?php
                //  pr($exercise_type);
                foreach ($exercise_type as $key => $row) {
                    $checked = '';
                    if (isset($Profile['PhysicalInformation']['exercise_type'])) {
                        $exercise_type_val = explode(',', $Profile['PhysicalInformation']['exercise_type']);
                        if (in_array($key, $exercise_type_val)) {
                            $checked = 'checked';
                        }
                    }
                    echo '<input type="checkbox" ' . $checked . '  name="exercise_type[]" value="' . $key . '"  />' . $row . '<br/>';
                }
                ?>

            </div>

            <div style="float: left; margin-left: 20px; width: 220px;">
                How many days per week do you exercise? <br/>
                <?php
                //  pr($exercise_type);
                foreach ($how_time_exercise as $key => $row) {
                    $checked = '';
                    if (isset($Profile['PhysicalInformation']['how_time_exercise'])) {
                        $how_time_exercise_val = explode(',', $Profile['PhysicalInformation']['how_time_exercise']);
                        if (in_array($key, $how_time_exercise_val)) {
                            $checked = 'checked';
                        }
                    }
                    echo '<input type="checkbox" ' . $checked . '  name="how_time_exercise[]" value="' . $key . '"  />' . $row . '<br/>';
                }
                ?>


            </div>


            <div style="float: left; margin-left: 20px; width: 220px;">
                For how long do you exercise each day?<br/>
                <?php
                //  pr($how_long_exercise);
                foreach ($how_long_exercise as $key => $row) {
                    $checked = '';
                    if (isset($Profile['PhysicalInformation']['how_long_exercise'])) {
                        $how_long_exercise_val = explode(',', $Profile['PhysicalInformation']['how_long_exercise']);
                        if (in_array($key, $how_long_exercise_val)) {
                            $checked = 'checked';
                        }
                    }
                    echo '<input type="checkbox" ' . $checked . '  name="how_long_exercise[]" value="' . $key . '"  />' . $row . '<br/>';
                }
                ?>
            </div>
        </div>
    </li>
    <li>Have you ever smoked cigarettes, cigars or betel leafs or chewed tobacco?
        <?php
                echo $form->input('is_ever_smoking', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                    'value' => isset($Profile['PhysicalInformation']['is_ever_smoking']) ? $Profile['PhysicalInformation']['is_ever_smoking'] : '0',
                    'options' => array('1' => 'Yes', '0' => 'No'))); ?>
                <div style="float: left; margin: 20px;">

                    a. When did you start?	Date  <?php
                $current_year = date('Y');
                $max_year = $current_year - 50;
                echo $form->input('smoking_start_date', array('label' => false, 'div' => false, 'type' => 'date', 'minYear' => date('Y'),
                    'value' => isset($Profile['PhysicalInformation']['smoking_start_date']) ? $Profile['PhysicalInformation']['smoking_start_date'] : date('Y-m-d'),
                    'maxYear' => $max_year));
        ?><br/>
                b. How many per week?<?php
                echo $form->input('smoking_per_weeek', array('label' => false, 'div' => false,
                    'value' => isset($Profile['PhysicalInformation']['smoking_per_weeek']) ? $Profile['PhysicalInformation']['smoking_per_weeek'] : '',
                    'style' => 'width: 50px;'));
        ?>	<br/>
                c. Have you quit?
            <?php
                echo $form->input('is_smoking_quit', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                    'value' => isset($Profile['PhysicalInformation']['is_smoking_quit']) ? $Profile['PhysicalInformation']['is_smoking_quit'] : '0',
                    'options' => array('1' => 'Yes', '0' => 'No')));
            ?>
            <?php
                $current_year = date('Y');
                $max_year = $current_year - 50;
                echo $form->input('smoking_quit_date', array('label' => false, 'div' => false, 'type' => 'date', 'minYear' => date('Y'),
                    'value' => isset($Profile['PhysicalInformation']['smoking_quit_date']) ? $Profile['PhysicalInformation']['smoking_quit_date'] : date('Y-m-d'),
                    'maxYear' => $max_year));
            ?>
                <br/>
                d. Do you want to quit?	 <?php
                echo $form->input('is_smoking_want_to_quit', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                    'value' => isset($Profile['PhysicalInformation']['is_smoking_want_to_quit']) ? $Profile['PhysicalInformation']['is_smoking_want_to_quit'] : '0',
                    'options' => array('1' => 'Yes', '0' => 'No'))); ?>
            </div>
        </li>
        <li>Do you drink alcohol?
        <?php
                echo $form->input('is_drink_alcohol', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                    'value' => isset($Profile['PhysicalInformation']['is_drink_alcohol']) ? $Profile['PhysicalInformation']['is_drink_alcohol'] : 'No',
                    'options' => array('Regular' => 'Regular', 'Social' => 'Social', 'No' => 'No')));
        ?>
            </li>
            <li>
                <h2>For Women Only</h2><br />
                Have you ever been pregnant?
        <?php
                echo $form->input('is_ever_pregnant', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                    'value' => isset($Profile['PhysicalInformation']['is_ever_pregnant']) ? $Profile['PhysicalInformation']['is_ever_pregnant'] : '0',
                    'options' => array('1' => 'Yes', '0' => 'No')));
        ?>
                <div style="clear: both;"></div>
                <div style="float: left; margin: 20px; clear: both;">
                                                                                   	a. Number of pregnancies <?php
                echo $form->input('number_of_pregnancy', array('label' => false, 'div' => false,
                    'value' => isset($Profile['PhysicalInformation']['number_of_pregnancy']) ? $Profile['PhysicalInformation']['number_of_pregnancy'] : '',
                    'style' => 'width: 50px;')); ?>
                                                	b. Number of Birth <?php
                echo $form->input('number_of_birth', array('label' => false, 'div' => false,
                    'value' => isset($Profile['PhysicalInformation']['number_of_birth']) ? $Profile['PhysicalInformation']['number_of_birth'] : '',
                    'style' => 'width: 50px;')); ?>
                                                	c. Number of abortions <?php
                echo $form->input('number_of_abortion', array('label' => false, 'div' => false,
                    'value' => isset($Profile['PhysicalInformation']['number_of_abortion']) ? $Profile['PhysicalInformation']['number_of_abortion'] : '',
                    'style' => 'width: 50px;'));
        ?>

            </div>
        </li>
        <li>Is your menstrual cycle regular?
        <?php
                echo $form->input('is_regular_menstrual_cycle', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                    'value' => isset($Profile['PhysicalInformation']['is_regular_menstrual_cycle']) ? $Profile['PhysicalInformation']['is_regular_menstrual_cycle'] : '0',
                    'options' => array('1' => 'Yes', '0' => 'No'))); ?>
            </li>
            <li>Have you had a PAP smear?
        <?php
                echo $form->input('is_had_pap_smear', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                    'value' => isset($Profile['PhysicalInformation']['is_had_pap_smear']) ? $Profile['PhysicalInformation']['is_had_pap_smear'] : 'No',
                    'options' => array('No' => 'No', 'Normal' => 'Normal', 'Abnormal' => 'Abnormal')));
        ?>
                Date:<?php
                $current_year = date('Y');
                $max_year = $current_year - 50;
                echo $form->input('pap_smear_date', array('label' => false, 'div' => false, 'type' => 'date', 'minYear' => date('Y'),
                    'value' => isset($Profile['PhysicalInformation']['pap_smear_date']) ? $Profile['PhysicalInformation']['pap_smear_date'] : date('Y-m-d'),
                    'maxYear' => $max_year));
        ?>
            </li>
            <li>Have you had a mammogram?
        <?php
                echo $form->input('is_had_mammogram', array('label' => false, 'div' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio',
                    'value' => isset($Profile['PhysicalInformation']['is_had_mammogram']) ? $Profile['PhysicalInformation']['is_had_mammogram'] : 'No',
                    'options' => array('No' => 'No', 'Normal' => 'Normal', 'Abnormal' => 'Abnormal')));
        ?>
                Date:<?php
                $current_year = date('Y');
                $max_year = $current_year - 50;
                echo $form->input('mammogram_date', array('label' => false, 'div' => false, 'type' => 'date', 'minYear' => date('Y'),
                    'value' => isset($Profile['PhysicalInformation']['mammogram_date']) ? $Profile['PhysicalInformation']['mammogram_date'] : date('Y-m-d'),
                    'maxYear' => $max_year));
        ?>
    </li>
</ol>