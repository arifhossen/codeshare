
<script type="text/javascript">

    /**************************************************************************************
     *  Dynamic add more option function
     *  return addmore html
     **************************************************************************************/
    function addMoreOption_investigation(name, total){
        var no =   $('.'+name+'_add_more_cancel').length+1;
        var max_add  = parseInt(total) - 1 ;
        var text = '<div id="'+name+'_add_more_container_'+no+'" class="investigation_container" > ';
        text += ' <span class="investigation investigation_name" ><input  type="text" name="data[investigation][name][]" value="" id="investigation_name_'+no+'" class="add_more_input_investigation" ></span>';
        text += ' <span class="investigation investigation_speciality" ><input  type="text" name="data[investigation][investigation_date][]" value="" id="investigation_strength_'+no+'" class="add_more_input_investigation" ></span>';
        text += ' <span class="investigation investigation_speciality" ><input  type="text" name="data[investigation][findings][]" value="" id="investigation_strength_'+no+'" class="add_more_input_investigation" ></span>';


        text += ' <span class="'+name+'_add_more_cancel add_more_cancel"  id="'+name+'_cancel_'+no+'" onclick="addMoreOptionCancel(\''+name+'_add_more_container_'+no+'\', \''+no+'\', \''+name+'\')" >X</span>';
        text += '</div> <div style="clear: both;"></div>';

        $('#'+name+'_add_more_panel').append(text);
        // i++;
        //var no =   $('.'+name+'_add_more_cancel').length;
        if(no >= max_add) {
            $('#'+name+'_add_more').hide();
            no=1;
        }
    }


</script>

<style>
    .add_more_input_investigation{ width: 95%!important; padding: 2px;}
    .investigation_container_header{ margin: 5px; padding-bottom:7px;}
    .investigation_container{ margin: 5px; padding-bottom:7px;}
    .investigation{ float: left;}
    .investigation_name{width: 210px;}
    .investigation_speciality{width: 250px;}
</style>

<div style="float: left; clear:both;">

    <div class="" id="investigation_add_more_panel">
        <div class="investigation_container_header" style="padding: 0px;" >
            <span class="investigation investigation_name">Name</span>
            <span class="investigation investigation_speciality">Date</span>
            <span class="investigation investigation_relationship">Findings</span>

        </div>
        <div style="clear: both;"></div>
        <?php //pr($Investigation);
        if(isset($Investigation) && !empty ($Investigation)) {
        $i=0;
        foreach ($Investigation as $list) {
        ?>
        <div class="investigation_container" id="investigation_add_more_container_<?php echo $i; ?>">
            <span class="investigation investigation_name"><input type="text" class="add_more_input_investigation" id="investigation_name_0" value="<?php echo $list['Investigation']['name']; ?>" name="data[investigation][name][]"></span>
            <span class="investigation investigation_speciality"><input type="text" class="add_more_input_investigation" id="investigation_speciality_0" value="<?php echo $list['Investigation']['investigation_date']; ?>" name="data[investigation][investigation_date][]"></span>
            <span class="investigation investigation_speciality"><input type="text" class="add_more_input_investigation" id="investigation_speciality_0" value="<?php echo $list['Investigation']['findings']; ?>" name="data[investigation][findings][]"></span>
            <?php if($i!=0) {?>
            <span onclick="addMoreOptionCancel('investigation_add_more_container_<?php echo $i; ?>', '<?php echo $i; ?>', 'investigation')" id="investigation_cancel_<?php echo $i; ?>" class="investigation_add_more_cancel add_more_cancel">X</span>
            <?php } ?>
        </div> <div style="clear: both;"></div>
        <?php
        $i++;
        }
        } else {
        ?>
        <div class="investigation_container" id="investigation_add_more_container_0">
            <span class="investigation investigation_name"><input type="text" class="add_more_input_investigation" id="investigation_name_0" value="" name="data[investigation][name][]"></span>
            <span class="investigation investigation_speciality"><input type="text" class="add_more_input_investigation" id="investigation_speciality_0" value="" name="data[investigation][investigation_date][]"></span>
            <span class="investigation investigation_speciality"><input type="text" class="add_more_input_investigation" id="investigation_speciality_0" value="" name="data[investigation][findings][]"></span>
        </div>
        <div style="clear: both;"></div>
        <?php  } ?>
    </div>
    <span id="investigation_add_more" class="add_more" onclick="addMoreOption_investigation('investigation');">Add More</span>

</div>