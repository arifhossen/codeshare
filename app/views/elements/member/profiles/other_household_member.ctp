<script type="text/javascript">

    /**************************************************************************************
     *  Dynamic add more option function
     *  return addmore html
     **************************************************************************************/
    function addMoreOption_other_household_members(name, total){
        var no =   $('.'+name+'_add_more_cancel').length+1;
        var max_add  = parseInt(total) - 1 ;
        var text = '<div id="'+name+'_add_more_container_'+no+'" class="other_household_members_container" > ';
        text += ' <span class="other_household_members other_household_members_name" ><input  type="text" name="data[other_household_member][name][]" value="" id="other_household_members_name_'+no+'" class="add_more_input_other_household_members" ></span>';
        text += ' <span class="other_household_members other_household_members_relationship" ><input  type="text" name="data[other_household_member][relationship][]" value="" id="other_household_members_relationship_'+no+'" class="add_more_input_other_household_members" ></span>';


        text += ' <span class="'+name+'_add_more_cancel add_more_cancel"  id="'+name+'_cancel_'+no+'" onclick="addMoreOptionCancel(\''+name+'_add_more_container_'+no+'\', \''+no+'\', \''+name+'\')" >X</span>';
        text += '</div> <div style="clear: both;"></div>';

        $('#'+name+'_add_more_panel').append(text);
        // i++;
        //var no =   $('.'+name+'_add_more_cancel').length;
        if(no >= max_add) {
            $('#'+name+'_add_more').hide();
            no=1;
        }
    }


</script>

<style>
    .add_more_input_other_household_members{ width: 95%; padding: 2px;}
    .other_household_members_container_header{ margin: 5px; padding-bottom:7px; font-weight: bold;}
    .other_household_members_container{ margin: 5px; padding-bottom:7px;}
    .other_household_members{ float: left;}
    .other_household_members_name{width: 375px;}
    .other_household_members_relationship{width: 300px;}
</style>

<div class="" id="other_household_members_add_more_panel">
    <div class="other_household_members_container" style="padding: 0px;" >
        <span class="other_household_members other_household_members_name">Name</span>
        <span class="other_household_members other_household_members_relationship">Relationship</span>
    </div>
    <div style="clear: both;"></div>
    <?php //pr($OtherHouseholdMember);
    if(isset($OtherHouseholdMember) && !empty ($OtherHouseholdMember)) {
        $i=0;
        foreach ($OtherHouseholdMember as $list) {
            ?>
    <div class="other_household_members_container" id="other_household_members_add_more_container_<?php echo $i; ?>">
        <span class="other_household_members other_household_members_name"><input type="text" class="add_more_input_other_household_members" id="other_household_members_name_0" value="<?php echo $list['OtherHouseholdMember']['name']; ?>" name="data[other_household_member][name][]"></span>
        <span class="other_household_members other_household_members_relationship"><input type="text" class="add_more_input_other_household_members" id="other_household_members_relationship_0" value="<?php echo $list['OtherHouseholdMember']['relationship']; ?>" name="data[other_household_member][relationship][]"></span>
                <?php if($i!=0) {?>
        <span onclick="addMoreOptionCancel('other_household_members_add_more_container_<?php echo $i; ?>', '<?php echo $i; ?>', 'other_household_members')" id="other_household_members_cancel_<?php echo $i; ?>" class="other_household_members_add_more_cancel add_more_cancel">X</span>
                    <?php } ?>
    </div> <div style="clear: both;"></div>
            <?php
            $i++;
        }
    } else {
        ?>
    <div class="other_household_members_container" id="other_household_members_add_more_container_0">
        <span class="other_household_members other_household_members_name"><input type="text" class="add_more_input_other_household_members" id="other_household_members_name_0" value="" name="data[other_household_member][name][]"></span>
        <span class="other_household_members other_household_members_relationship"><input type="text" class="add_more_input_other_household_members" id="other_household_members_relationship_0" value="" name="data[other_household_member][relationship][]"></span>
    </div>
    <div style="clear: both;"></div>
        <?php  } ?>

</div>
<span id="other_household_members_add_more" class="add_more" onclick="addMoreOption_other_household_members('other_household_members');">Add More</span>