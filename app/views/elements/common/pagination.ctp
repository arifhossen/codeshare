

<div class="pagination-container">
  <div class="f-left">
    <?php echo $this->Paginator->counter(array( 'format' => __('Page %page% of %pages%', true))); ?>
  </div>
  <div class="f-right">
    <?php echo $this->Paginator->prev('« ' . __('previous', true), array(), null, array('class' => 'disabled')); ?>&nbsp;
    <?php echo $this->Paginator->numbers(); ?> &nbsp;
    <?php echo $this->Paginator->next(__('next', true) . ' »', array(), null, array('class' => 'disabled')); ?>
  </div>
</div>


	