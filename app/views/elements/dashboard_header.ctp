<script type="text/javascript">
$(document).ready(function($){
	$('#mega-1').dcVerticalMegaMenu({
		rowItems: '3',
		speed: 'fast',
		effect: 'slide',
		direction: 'right'
	});
});
</script>
<script type="text/javascript">
$(function() {
    $(".dashboardMenuList").jCarouselLite({
		btnNext: ".feature-next",
        btnPrev: ".feature-prev",
		hoverPause:true,
		visible: 7,
		auto:false,
		scroll:1
    });
});
</script>

<?php

	$userInfo = $session->read('userInfo');

?>

	

<div id="dashboardWelcomeMessage">

	<div class="left">

	 <span align="left" style="font-size:30px; text-align:center; color:#666666; font-weight:bold;"> Welcome, <?php echo $userInfo['UserAgentInformation']['first_name']; ?> </span>	
	
</div>


<div class="right">

	
	
		<table style="float:right; padding-right:0px; margin-right:20px;">
			<tr>
				<td><span style="color:#3499B5; font-weight:bold;">User Name : <span style="color:black;"><?php echo $userInfo['User']['username']; ?></span> </span> </td>
				<td style="padding-left:20px;"><span style=" color:#3499B5; font-weight:bold;">Member ID : <span style="color:black;"><?php echo $userInfo['User']['member_id']; ?> </span></span> </td>
			</tr>
				
	
			<tr>
				<td> 	<span style="color:red;"><a  style="color:red;" href="<?php echo $this->webroot.'dashboards/change_password'; ?>">[Change password]</a>
		</span>	</td>
				<td style="padding-left:20px;">	
		<span style="color:#3499B5; font-weight:bold;">Last Login : <span style="color:black;"><?php echo date('m/d/Y'); ?> </span></span>
		 </td>
			</tr>
			
		</table>	
		

</div>

	
</div>	


<br /><br /><br />

<div id="dashboardMenuDiv">
	
		
		<div class="slide-icon-left">
		<a class="feature-prev" href="#"><img src="<?php echo $this->webroot; ?>images/leftMenuArrow.png" alt="" /></a>
	</div>
	<div class="dashboardMenuList" style="float:left;">
		<div class="dashboardMenuListView">
			<ul>
				<li style="height:120px;">														
			<a href="<?php echo $this->webroot; ?>dashboards/"><img src="<?php echo $this->webroot; ?>images/menu/icon_myprofile.png" alt="" />					</a>
				</li>
				
				<li>														
						<a href="#">
						<img src="<?php echo $this->webroot; ?>images/menu/icon_statements.png" alt="" />
						</a>
				</li>
				
				<li>														
						<a href="<?php echo $this->webroot; ?>dashboards/paybill">
							<img src="<?php echo $this->webroot; ?>images/menu/icon_paybill.png" alt="" />
						</a>
				</li>
				
				<li>														
						<a href="<?php echo $this->webroot; ?>dashboards/news/">
							<img src="<?php echo $this->webroot; ?>images/menu/icon_news.png" alt="" />
						</a>
				</li>
				
				<li>														
						<a href="<?php echo $this->webroot; ?>dashboards/app">
							<img src="<?php echo $this->webroot; ?>images/menu/icon_app_dashboard.png" alt="" />
						</a>
				</li>
				
				<li>	
				
				<a href="<?php echo $this->webroot; ?>dashboards/mobile">
							<img src="<?php echo $this->webroot; ?>images/menu/icon_mobile_dashboard.png" alt="" />
						</a>
				
				
				</li>
				
				
				<li>														
							<a href="<?php echo $this->webroot; ?>dashboards/web">
							<img src="<?php echo $this->webroot; ?>images/menu/icon_web_dashboard.png" alt="" />
						</a>
				</li>
				
			</ul>
		</div>
	</div>
	<div class="slide-icon-right">
		<a class="feature-next" href="#"><img src="<?php echo $this->webroot; ?>images/rightMenuArrow.png" alt=""/></a>
	</div>
	

	
</div>
