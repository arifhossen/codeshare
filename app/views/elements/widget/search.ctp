
<div class="hl-box" id="hl-box-main">
    <div class="hl-box-header">Health Search</div>
    <div class="hl-box-separator-bottom">
        <div class="hl-box-white-border">
            <div class="hl-box-content">
                <form method="get" action="<?php echo $this->webroot; ?>search" name="symptomform">
                    <div class="hl-search-image float-right" id="hl-search-symptom">&nbsp;</div>
                    <h3 class="hl-box-main-header">Symptoms</h3>
                    <input type="text" default="Enter a Symptom" value="Enter a Symptom" name="addterm" class="hl-inputShadow hl-search-widget-input float-left" />
                    <input type="image" src="/images/clear.gif" value="" class="hl-submit-go float-left" />
                    <p style="font-size: 11px; padding: 30px 0pt 0pt 2px; line-height: 1.4em;" class="clear-left">
                        Discover possible causes based on your symptoms
                    </p>
                </form>
            </div>
        </div>
    </div>
    <div class="hl-box-separator-bottom">
        <div class="hl-box-white-border">
            <div class="hl-box-content">
                <form method="get" action="<?php echo $this->webroot; ?>search" name="treatmentform">
                    <div class="hl-search-image float-right" id="hl-search-treatment">&nbsp;</div>
                    <h3 class="hl-box-main-header">Treatment</h3>
                    <input type="text" default="Enter Condition" value="Enter Condition" name="term" class="hl-inputShadow hl-search-widget-input float-left" />
                    <input type="image" src="/images/clear.gif" value="" class="hl-submit-go float-left" />
                </form>
            </div>
        </div>
    </div>
    <div class="hl-box-separator-bottom">
        <div class="hl-box-white-border">
            <div class="hl-box-content">
              <form method="get" action="<?php echo $this->webroot; ?>search" name="treatmentform">
                <div class="hl-search-image float-right" id="hl-search-doctor">&nbsp;</div>
                <h3 class="hl-box-main-header">Doctor</h3>
                <input type="text" default="Enter Doctor name" value="Enter Doctor name" name="item" class="hl-inputShadow hl-search-widget-input float-left" />
                <input type="image" src="/images/clear.gif" value="" class="hl-submit-go float-left" />
              </form>
            </div>
        </div>
    </div><div class="hl-box-white-border hl-box-separator-bottom">
        <div class="hl-box-content">
            <form method="get" action="<?php echo $this->webroot; ?>search" name="drugform">
                <div class="hl-search-image float-right" id="hl-search-drugs">&nbsp;</div>
                <h3 class="hl-box-main-header">Drugs</h3>
                <input type="text" default="Enter a Drug Name" value="Enter a Drug Name" name="item" class="hl-inputShadow hl-search-widget-input float-left" />
                <input type="image" src="/images/clear.gif" value="" class="hl-submit-go float-left" />
            </form>
        </div>
    </div>
</div>