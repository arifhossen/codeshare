<div class="hl-box">
    <div class="hl-box-white-border hl-box-separator-bottom">
        <div onclick="location.href='<?php echo $this->webroot; ?>dashboard/tips'" class="hl-tip-of-day">
            <div class="box-header"><?php echo date('M d, Y'); ?></div>
            <p class="div-margin-t-large content">Turn up the life-lengthening powers of your baked goods.<br />
               <a href="<?php echo $this->webroot; ?>dashboard/tips">More »</a>
            </p>
            <div class="view-all"><a href="<?php echo $this->webroot; ?>dashboard/tips">View All Tips »</a></div>
        </div>
    </div>
</div>