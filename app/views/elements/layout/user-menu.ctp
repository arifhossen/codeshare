<div class="hl-box" id="hl-box-main">
    <div class="hl-box-header">Health Information</div>

    <div>
        <div class="hl-box-white-border">
            <div style="position: relative;" class="hl-box-content">
                <ul class="ehr-menu">
                    <li><a href="<?php echo $this->webroot; ?>prescriptions">Prescriptions</a>
                        <ul class="ehr-sub-menu">
                            <li><a href="<?php echo $this->webroot; ?>cheif_complains">» Cheif Complains</a></li>
                            <li><a href="<?php echo $this->webroot; ?>medications">» Medications</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo $this->webroot; ?>hospitalizations">Hospitalizations</a></li>
                    <li><a href="<?php echo $this->webroot; ?>lab_reports">Lab reports</a></li>
                    <li><a href="<?php echo $this->webroot; ?>alergies">Alergies</a></li>
                    <li><a href="<?php echo $this->webroot; ?>surgeries">Surgeries</a></li>
                    <li><a href="<?php echo $this->webroot; ?>doctors">doctors</a></li>
                    <li><a href="<?php echo $this->webroot; ?>investigations">Investigations</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="hl-box" id="hl-box-main">
    <div class="hl-box-header">Quicklinks</div>

    <div>
        <div class="hl-box-white-border">
            <div style="position: relative;" class="hl-box-content">
                <ul class="ehr-menu">
                    <li><a href="<?php echo $this->webroot; ?>profiles/add">Add new member</a></li>
                    <li><a href="<?php echo $this->webroot; ?>prescriptions/add">Add new prescription</a></li>
                    <li><a href="<?php echo $this->webroot; ?>diets/add">Add new diet prescription</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>