
<div class="flash_alert" style="
	width:489px;
	padding:10px;
	vertical-align:center;
	z-index:16;
	left: 25px;
	background-color: #FFFF66;
	border: 1px solid #C18800;
	font-weight:bold;
	margin:0 0 15px;">
     <?php echo $html->image('icons/attention.png', array( 'alt' => 'Alert', 'style' => 'vertical-align:middle;')) ?>
     <span style="flash_alert"> <?php echo $message; ?> </span> 
</div>  

