<script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-1.4.3.min.js"></script>
<script src="<?php echo $this->webroot; ?>jquery/slides.min.jquery.js"></script>

<script type="text/javascript">	



$.noConflict();
jQuery(function(){ 

   jQuery('.iPhone').bind('mouseover', apple_mouseover)
   jQuery('.iPhone').bind('mouseout',  apple_mouseout)	
	
	function apple_mouseover()
	{  		
	
			jQuery('.iPhone').attr("src","<?php echo $this->webroot; ?>images/layouts/Apple_Logo_ON.png");
		
		
	}	

	function apple_mouseout()
	{ 	
		 jQuery('.iPhone').attr("src","<?php echo $this->webroot; ?>images/layouts/Apple_Logo_OFF.png");	
		 
		
	}
	
	
	
   jQuery('.android').bind('mouseover', android_mouseover)
   jQuery('.android').bind('mouseout',  android_mouseout)	
	
	function android_mouseover()
	{  		
		jQuery('.android').attr("src","<?php echo $this->webroot; ?>images/layouts/Android_Logo_ON.png");
	}	

	function android_mouseout()
	{ 
		
		 jQuery('.android').attr("src","<?php echo $this->webroot; ?>images/layouts/Android_Logo_OFF.png"); 		 
		  
		
	}
	
	
   jQuery('.windows').bind('mouseover', windows_mouseover)
   jQuery('.windows').bind('mouseout',  windows_mouseout)	
	
	function windows_mouseover()
	{  		
		jQuery('.windows').attr("src","<?php echo $this->webroot; ?>images/layouts/Windows_Logo_ON.png");
	}	

	function windows_mouseout()
	{ 	
		 jQuery('.windows').attr("src","<?php echo $this->webroot; ?>images/layouts/Windows_Logo_OFF.png");	
	}
	
   
   jQuery('.blackbery').bind('mouseover', blackbery_mouseover)
   jQuery('.blackbery').bind('mouseout',  blackbery_mouseout)	
	
	function blackbery_mouseover()
	{  		
		jQuery('.blackbery').attr("src","<?php echo $this->webroot; ?>images/layouts/Mobile_Icon_ON.png");
	}	

	function blackbery_mouseout()
	{ 	
		 jQuery('.blackbery').attr("src","<?php echo $this->webroot; ?>images/layouts/Mobile_Icon_OFF.png");	
	}	
	
	
jQuery('#features_arrow').click(function() {
    var getClass = '';
	getClass = jQuery(this).attr('class');
	
	if(getClass == 'features_down_arrow')
	{ 
		jQuery(this).removeClass('features_down_arrow');
		jQuery(this).addClass('features_right_arrow');	
		jQuery('#iPhoneFeatures').slideUp();  
		
	}
	else
	{
		jQuery(this).removeClass('features_right_arrow');
		jQuery(this).addClass('features_down_arrow');	
		jQuery('#iPhoneFeatures').slideDown();  	
	
	}	
});

jQuery('#benefits_arrow').click(function() {
    var getClass = '';
	getClass = jQuery(this).attr('class');
	
	if(getClass == 'features_down_arrow')
	{ 
		jQuery(this).removeClass('features_down_arrow');
		jQuery(this).addClass('features_right_arrow');	
		jQuery('#iPhoneBenefits').slideUp();  
		
	}
	else
	{
		jQuery(this).removeClass('features_right_arrow');
		jQuery(this).addClass('features_down_arrow');	
		jQuery('#iPhoneBenefits').slideDown();  	
	
	}	
});







//Slideshow code
			jQuery('#slides').slides({
				preload: true,
				preloadImage: 'img/loading.gif',
				play: 5000,
				pause: 2500,
				hoverPause: true,
				animationStart: function(current){
					jQuery('.caption').animate({
						bottom:-35
					},100);
					if (window.console && console.log) {
						// example return of current slide number
						console.log('animationStart on slide: ', current);
					};
				},
				animationComplete: function(current){
					jQuery('.caption').animate({
						bottom:0
					},200);
					if (window.console && console.log) {
						// example return of current slide number
						console.log('animationComplete on slide: ', current);
					};
				},
				slidesLoaded: function() {
					jQuery('.caption').animate({
						bottom:0
					},200);
				}
			});
			
			
			
			
});




	function getSelectedPlatform(sp_id)
	{	
	
	
		$.ajax({
		   type: "GET",
		   url: '<?php echo $this->webroot.'supportedPlatforms/index/'; ?>'+sp_id,		  
		   success: function(msg){	
		   
			$('#spContentPanelUpdateDiv').html(msg);
			
		   }
		 });
		 
		 
		 
		 
		 if(sp_id == 'iPhone')
		 {
		 	jQuery('.iPhone').attr("src","<?php echo $this->webroot; ?>images/layouts/Apple_Logo_ON.png");
			jQuery('.android').attr("src","<?php echo $this->webroot; ?>images/layouts/Android_Logo_OFF.png");
			jQuery('.windows').attr("src","<?php echo $this->webroot; ?>images/layouts/Windows_Logo_OFF.png");	
			jQuery('.blackbery').attr("src","<?php echo $this->webroot; ?>images/layouts/Mobile_Icon_OFF.png");
		 }		 
		
		 
		  if(sp_id == 'android')
		 {
		 	
			jQuery('.iPhone').attr("src","<?php echo $this->webroot; ?>images/layouts/Apple_Logo_OFF.png");
			jQuery('.android').attr("src","<?php echo $this->webroot; ?>images/layouts/Android_Logo_ON.png");
			jQuery('.windows').attr("src","<?php echo $this->webroot; ?>images/layouts/Windows_Logo_OFF.png");	
			jQuery('.blackbery').attr("src","<?php echo $this->webroot; ?>images/layouts/Mobile_Icon_OFF.png");
		 }
		 if(sp_id == 'windows')
		 {
		 	jQuery('.iPhone').attr("src","<?php echo $this->webroot; ?>images/layouts/Apple_Logo_OFF.png");
			jQuery('.android').attr("src","<?php echo $this->webroot; ?>images/layouts/Android_Logo_OFF.png");
			jQuery('.windows').attr("src","<?php echo $this->webroot; ?>images/layouts/Windows_Logo_ON.png");	
			jQuery('.blackbery').attr("src","<?php echo $this->webroot; ?>images/layouts/Mobile_Icon_OFF.png");
		 }
		 
		 if(sp_id == 'blackbery')
		 {
		 	jQuery('.iPhone').attr("src","<?php echo $this->webroot; ?>images/layouts/Apple_Logo_OFF.png");
			jQuery('.android').attr("src","<?php echo $this->webroot; ?>images/layouts/Android_Logo_OFF.png");
			jQuery('.windows').attr("src","<?php echo $this->webroot; ?>images/layouts/Windows_Logo_OFF.png");	
			jQuery('.blackbery').attr("src","<?php echo $this->webroot; ?>images/layouts/Mobile_Icon_ON.png");
		 }
	  
	  
	  
	 } 
	 

</script>	




			
		
	 
	 
<div id='spContentPanelUpdateDiv'>	 




<div id="spContentPanel">

	<div id="spLogoLeft">
	
	<img src="<?php echo $this->webroot ?>images/layouts/AvailableontheiPhoneAppStore.png"  />
	
	</div>
	
	<div id="spMobilePanelMiddel">
	
		<div class="spMobilePanelMiddel_iconPanel">
		
		
					
			<div id="slides">
				<div class="slides_container">			
							
					<?php
					
					
					 foreach($supportedPlatformsInfo['SupportedPlatformPicture'] as $key=>$data)
					 {
			
						 $picture = $data['picture'];
						
						
							?>		
						
						
						
								<div class="slide">					
									<img src="<?php echo $this->webroot.$picture; ?>" alt='Platform Image' />						
								</div>
					 
					 
					 <?php
					 }		
					
					 ?>
							
							
				</div>
							
			</div>
		
		
		  <img style='cursor:pointer; margin-left:-100px; margin-top:22px;' src='<?php echo $this->webroot; ?>images/layouts/Shadow.png'>
		
		</div>
	
	</div>
	
	
	
	<div id="spFeaturesRight">
	
	
	<table>
	<tr>
		<td>	
			<div id="features_arrow" class="features_down_arrow"></div>
		</td>
		<td style="padding-left:10px;">
			<img src="<?php echo $this->webroot."images/layouts/features.png"; ?> "  />
		</td>
	</tr>
	</table>
	
	
	
	<div id="iPhoneFeatures">
	<?php	
		echo $supportedPlatformsInfo['SupportedPlatform']['description'];	
	?>	
	</div>
	
	
	<table>
	<tr>
		<td>	
			<div id="benefits_arrow" class="features_down_arrow"></div>
		</td>
		<td style="padding-left:10px;">
			<img src="<?php echo $this->webroot."images/layouts/benefits.png"; ?> "  />
		</td>
	</tr>
	</table>
	
	
	
	<div id="iPhoneBenefits">
	<?php	
		echo $supportedPlatformsInfo['SupportedPlatform']['benefits'];	
	?>	
	</div>
	
	
	
	
	
	
	<br />
	<div style="margin-left:30px;">
	<a href="<?php echo $this->webroot;?>packages">
<img src="<?php echo $this->webroot;?>images/packages/signup.png" /> </a>

</div>
	
	</div>

</div>

 </div>


<div style="clear:both;"></div>
				

<div id="supoportedPlatformText">
<?php

	$logoImg = 'images/layouts/supported_platforms.png';
	echo $supportedPlatform = '<img src="'.$this->webroot.$logoImg.'" alt="Smartactic Logo"  width="350" />';	

?>
</div>



<div id="SupportPlatFormMenu">
	<div id='spMenuControl'>
	
		<a onClick="getSelectedPlatform('iPhone')" class='appleSelect'>
		<img style='cursor:pointer;' src='<?php echo $this->webroot; ?>images/layouts/Apple_Logo_ON.png' width='68' class="iPhone" title="Apple">
		</a>
		
		<a onClick="getSelectedPlatform('android')"  class='androidSelect'>
		<img style='cursor:pointer;' src='<?php echo $this->webroot; ?>images/layouts/Android_Logo_OFF.png' width='68' class="android">
		</a>
		
		<a onClick="getSelectedPlatform('windows')">
		
		<img style='cursor:pointer;' src='<?php echo $this->webroot; ?>images/layouts/Windows_Logo_OFF.png' width='87' class="windows">
		</a>
		
			<a onClick="getSelectedPlatform('blackberry')">
		<img  style='cursor:pointer;' src='<?php echo $this->webroot; ?>images/layouts/Mobile_Icon_OFF.png' width='48' class="blackbery">
		</a>
  </div>

</div>



