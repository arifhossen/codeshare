<div class="action_title"><span>General Settings</span></div>
<script type="text/javascript">
    $(function(){
        $('#left_content li').removeClass('selLeft');
        $('#admin_mngt_list').addClass('selLeft');
        $('.nav_admin').show();
        $('#adminManage').css('font-weight','bold');
    });
</script>
 
<table cellpadding="0" cellspacing="1" class="listContainer">
    <?php
    $tableHeaders =  $html->tableHeaders(array(
    $paginator->sort('id'),
    $paginator->sort('sitename'),
	$paginator->sort('companyslogan'),
	$paginator->sort('welcomemsg'),
	$paginator->sort('logoname'),
	$paginator->sort('adminemail'),
    __('Actions', true),
    ));
    echo $tableHeaders;

    $rows = array();
    foreach ($generalsettings AS $generalsetting) {
   
    $actions  =  $this->Html->link($this->Html->image('admin/icon_edit.gif'), array('action'=>'edit', $generalsetting['Generalsetting']['id']), array('escape' => false, 'title' => 'Edit'));

	 if(isset($generalsetting['Generalsetting']['logo'])){
		//$img_url = "files/product_files/".$value['AllFile'][0]['attach_file']."";
		$imagepath = '<img src="'.$this->webroot.'timthumb.php?src='.$generalsetting['Generalsetting']['logo'].'&w=60&h=60&zc=1" alt="Product Photo" />';	
		}
		else{
			$imagepath = $this->Html->image('no_image.jpg',array('alt'=>'no image','style'=>'height:60px;width:60px;','align'=>'middle'));
		}
   
    $rows[] = array(
    $generalsetting['Generalsetting']['id'],
    $generalsetting['Generalsetting']['sitename'],
	$generalsetting['Generalsetting']['companyslogan'],
	$generalsetting['Generalsetting']['welcomemsg'],
	$imagepath,
	$generalsetting['Generalsetting']['adminemail'],
    $actions,
    );
    }
    echo $html->tableCells($rows);
    ?>
</table>

<?php echo $this->element('common/pagination'); ?>