<div class="generalsettings index">
	<h2><?php __('Generalsettings');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('sitename');?></th>
			<th><?php echo $this->Paginator->sort('companyslogan');?></th>
			<th><?php echo $this->Paginator->sort('welcomemsg');?></th>
			<th><?php echo $this->Paginator->sort('logoname');?></th>
			<th><?php echo $this->Paginator->sort('adminemail');?></th>
			<th><?php echo $this->Paginator->sort('feedbackemail');?></th>
			<th><?php echo $this->Paginator->sort('contactusemail');?></th>
			<th><?php echo $this->Paginator->sort('tenderperpage');?></th>
			<th><?php echo $this->Paginator->sort('maxnumofimage');?></th>
			<th><?php echo $this->Paginator->sort('maxsizeofimage');?></th>
			<th><?php echo $this->Paginator->sort('maxattachfilesize');?></th>
			<th><?php echo $this->Paginator->sort('maxnumproduct');?></th>
			<th><?php echo $this->Paginator->sort('paginatorlimit');?></th>
			<th><?php echo $this->Paginator->sort('tenderadvertiseprice');?></th>
			<th><?php echo $this->Paginator->sort('closeauctionprice');?></th>
			<th><?php echo $this->Paginator->sort('postauctionreportprice');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($generalsettings as $generalsetting):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $generalsetting['Generalsetting']['id']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['sitename']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['companyslogan']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['welcomemsg']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['logoname']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['adminemail']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['feedbackemail']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['contactusemail']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['tenderperpage']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['maxnumofimage']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['maxsizeofimage']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['maxattachfilesize']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['maxnumproduct']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['paginatorlimit']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['tenderadvertiseprice']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['closeauctionprice']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['postauctionreportprice']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['created']; ?>&nbsp;</td>
		<td><?php echo $generalsetting['Generalsetting']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $generalsetting['Generalsetting']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $generalsetting['Generalsetting']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $generalsetting['Generalsetting']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $generalsetting['Generalsetting']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Generalsetting', true), array('action' => 'add')); ?></li>
	</ul>
</div>