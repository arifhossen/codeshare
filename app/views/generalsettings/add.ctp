<div class="generalsettings form">
<?php echo $this->Form->create('Generalsetting');?>
	<fieldset>
 		<legend><?php __('Add Generalsetting'); ?></legend>
	<?php
		echo $this->Form->input('sitename');
		echo $this->Form->input('companyslogan');
		echo $this->Form->input('welcomemsg');
		echo $this->Form->input('logoname');
		echo $this->Form->input('adminemail');
		echo $this->Form->input('feedbackemail');
		echo $this->Form->input('contactusemail');
		echo $this->Form->input('tenderperpage');
		echo $this->Form->input('maxnumofimage');
		echo $this->Form->input('maxsizeofimage');
		echo $this->Form->input('maxattachfilesize');
		echo $this->Form->input('maxnumproduct');
		echo $this->Form->input('paginatorlimit');
		echo $this->Form->input('tenderadvertiseprice');
		echo $this->Form->input('closeauctionprice');
		echo $this->Form->input('postauctionreportprice');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Generalsettings', true), array('action' => 'index'));?></li>
	</ul>
</div>