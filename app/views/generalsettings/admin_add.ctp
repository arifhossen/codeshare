<div class="action_title"><span>General Settings</span></div>
<div class="tabitem">
    <div class="common_from">
    <?php echo $this->Form->create("Generalsetting",array('enctype'=>"multipart/form-data"));?>

        <div class="element">
            <div class="title">Store Name</div>
            <div class="value">
            <?php echo $form->input('sitename',array('label'=>false,'div'=>false));?>
            </div>
        </div>

        <div class="element">
            <div class="title">Company Slogan</div>
            <div class="value">
			<?php echo $form->input('companyslogan',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		
		  <div class="element">
            <div class="title">Welcome Message</div>
            <div class="value">
			<?php echo $form->input('welcomemsg',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		  <div class="element">
            <div class="title">Logo Name</div>
            <div class="value">
			<?php echo $form->input('logoname',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		  <div class="element">
            <div class="title">Admin Email</div>
            <div class="value">
			<?php echo $form->input('adminemail',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		  <div class="element">
            <div class="title">Fedback Email</div>
            <div class="value">
			<?php echo $form->input('feedbackemail',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		<div class="element">
            <div class="title">Contact us Email</div>
            <div class="value">
			<?php echo $form->input('contactusemail',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		<div class="element">
            <div class="title">Perduct per Page</div>
            <div class="value">
			<?php echo $form->input('productperpage',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		<div class="element">
            <div class="title">Max Num of Image</div>
            <div class="value">
			<?php echo $form->input('maxnumofimage',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		<div class="element">
            <div class="title">Max Size of Size</div>
            <div class="value">
			<?php echo $form->input('maxsizeofimage',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		<div class="element">
            <div class="title">Max Attach File Size</div>
            <div class="value">
			<?php echo $form->input('maxattachfilesize',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		<div class="element">
            <div class="title">Max Number per Product</div>
            <div class="value">
			<?php echo $form->input('maxnumproduct',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		<div class="element">
            <div class="title">Paginator Limit</div>
            <div class="value">
			<?php echo $form->input('paginatorlimit',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
    </div>
</div>
<div class="button-container">
    <button value="signin" name="btnAction" type="submit"><span>Submit</span></button>
    or <?php echo $this->Html->link(__('Back to list', true),array('action' => 'index')); ?>
</div>
