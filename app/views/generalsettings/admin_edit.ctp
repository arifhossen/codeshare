<div class="action_title"><span>General Settings</span></div>
<div class="tabitem">
    <div class="common_from">
    <?php echo $this->Form->create("Generalsetting",array('enctype'=>"multipart/form-data"));  echo $form->input('id');?>

        <div class="element">
            <div class="title">Company Name</div>
            <div class="value">
            <?php echo $form->input('sitename',array('label'=>false,'div'=>false));?>
            </div>
        </div>

        <div class="element">
            <div class="title">Company Slogan</div>
            <div class="value">
			<?php echo $form->input('companyslogan',array('label'=>false,'div'=>false));?>
            </div>
        </div>		
		
		  <div class="element">
            <div class="title">Welcome Message</div>
            <div class="value">
			<?php echo $form->input('welcomemsg',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		  <div class="element">
            <div class="title">Logo</div>

			<div class="value">
        <?php 
		$data = $this->data['Generalsetting'];		
		//	debug($data);
			if(!empty($data['logo'])){
			?> <img src="<?php echo $this->webroot;?>timthumb.php?src=<?php echo $data['logo']; ?>&w=100&h=100&zc=2" alt='Company Logo' />
			<input name="data[Generalsetting][logo_pre_image]" value="<?php echo $data['logo'];?>" type="hidden" />
				<?php 
		   } 
		else  {
			echo $this->Html->image('no_image.jpg',array('alt'=>'no image','style'=>'height:100px;width:100px;','align'=>'middle'));
		} ?><br />
        <?php echo $this->Form->input("logo",array('label'=>false,'div'=>false,'type'=>'file'));?>
    </div>
			
        </div>
		
		  <div class="element">
            <div class="title">Admin Email</div>
            <div class="value">
			<?php echo $form->input('adminemail',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		  <div class="element">
            <div class="title">Fedback Email</div>
            <div class="value">
			<?php echo $form->input('feedbackemail',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		<div class="element">
            <div class="title">Contact us Email</div>
            <div class="value">
			<?php echo $form->input('contactusemail',array('label'=>false,'div'=>false));?>
            </div>
        </div>

		<div class="element">
            <div class="title">Paginator Limit</div>
            <div class="value">
			<?php echo $form->input('paginatorlimit',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
    </div>
</div>
<div class="button-container">
    <button value="signin" name="btnAction" type="submit"><span>Submit</span></button>
    or <?php echo $this->Html->link(__('Back to list', true),array('action' => 'index')); ?>
</div>
