<!--[if !IE]>start section<![endif]-->
				<div class="section">
					
					<!--[if !IE]>start title wrapper<![endif]-->
					<div class="title_wrapper">
						<span class="title_wrapper_top"></span>
						<div class="title_wrapper_inner">
							<span class="title_wrapper_middle"></span>
							<div class="title_wrapper_content">
								<h2>Payment Gateway Integration</h2>
								<ul class="section_menu right">
									<li><a href="#" class="selected_lk"><span class="l"><span></span></span><span class="m"><em>Payment Method Lists</em><span></span></span><span class="r"><span></span></span></a></li>
											</ul>
							</div>
						</div>
						<span class="title_wrapper_bottom"></span>
					</div>
					<!--[if !IE]>end title wrapper<![endif]-->
					
					<!--[if !IE]>start section content<![endif]-->
					<div class="section_content">
						<span class="section_content_top"></span>
						
						<div class="section_content_inner">
							<!--[if !IE]>start dashboard menu<![endif]-->
								<div class="dashboard_menu_wrapper">
								<ul class="dashboard_menu">
									<li><a href="<?php echo $this->webroot.'admin/payment_gateways/authorize_net_trasaction'; ?>" class="d17"><span>Authorized.net</span></a></li>
									<li><a href="<?php echo $this->webroot.'admin/payment_gateways/paypal_transaction_payment_pro'; ?>" class="dpaypal"><span>Paypal</span></a></li>
									
									<li><a href="<?php echo $this->webroot.'admin/payment_gateways/first_data_global_gateway_transaction'; ?>" class="dfirstData"><span>First Data</span></a></li>
									
								
								</ul>
								</div>
								<!--[if !IE]>end dashboard menu<![endif]-->	
						</div>
						
						<span class="section_content_bottom"></span>
					</div>
					<!--[if !IE]>end section content<![endif]-->
				</div>
				<!--[if !IE]>end section<![endif]-->
				