<script type="text/javascript">
jQuery(function(){
		jQuery("#first_name").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
			
		});
		jQuery("#last_name").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
			
		});
		jQuery("#office_name").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
			
		});
		jQuery("#agent_lic").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
			
		});
		jQuery("#home_address1").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
			
		});
		jQuery("#city").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
			
		});
		jQuery("#office_number1").validate({
			 expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
			message: ""
			
		});
		jQuery("#office_number2").validate({
			 expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
			message: ""
			
		});
		jQuery("#office_number3").validate({
			 expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
			message: ""
			
		});
		
		jQuery("#office_ext").validate({
			 expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
			message: ""
			
		});
		
		
		
		
		
		/*jQuery("#cell_number2").validate({
			 expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
			message: ""
		});
		jQuery("#cell_number2").validate({
			 expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
			message: ""
		});
		jQuery("#cell_number3").validate({
			 expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
			message: ""
		});*/
		
		jQuery("#email_address").validate({
             expression: "if (VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/)) return true; else return false;",
                    message: ""
          }); 
		  
		  var alt_email= jQuery("#alt_email").val();		
		  if(alt_email!=''){
			  jQuery("#alt_email").validate({
				 expression: " if( VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/)) return true; else return false;",
						message: ""
			  });
		  }
		   	
		
		
		
		jQuery("#broker_first_name").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
			
		});
		
		jQuery("#broker_last_name").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
			
		});
		
		jQuery("#broker_lic").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
			
		});
		jQuery("#broker_address1").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
			
		});
		jQuery("#broker_city").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
			
		});
	
	   jQuery("#broker_state").validate({
			expression: "if (VAL != '') return true; else return false;",
			message: ""
	   });
	   
		jQuery("#broker_zip").validate({
			expression: "if (VAL) return true; else return false;",
			message: ""
	   });
		
		
		  
		    jQuery('#contactInfoUpdateForm').validate(function(){
		});	
	});
</script>

<div id="userdashboard">

<div id="dashboarheader">
	<?php echo $this->element('dashboard_header'); ?>
</div>

<div id="dashboardContentArea" style="border:0px solid red; height:800px;">

	<div class="outBox">
		<div id="dashboardBox">			
					<div id="titleBarInnerPage">
						<div id="headerLeftBar"> </div>
						<div id="headerMiddelBar" style="width:912px;"> <div class='leftTitle'>Edit Contact Info</div> <div class='rightTitle'> </div>   </div>
						<div id="headerRightBar"> </div>
					</div>	
					
					<div class="content" style="text-align:left; height:750px;">
					
					
					<div style="float:left; width:38%; margin:20px;">
					
					
					
						<div class="outBox">
									<div id="dashboardBox" style="height:330px;">			
												<div id="titleBarInnerPage" style="margin-right:-4px;">
													<div id="headerLeftBar"> </div>
													<div id="headerMiddelBar"> <div class='BarTitleCommon'>Current Contact Info</div>  </div>
													<div id="headerRightBar"> </div>
												</div>	
												
												<div class="content">
												
												
												<div style="float:left; width:100%; text-align:left; padding:10px;  padding-top:3px;">	
												
											
								
														
				<span class="dashLevel"> Member Since: </span>&nbsp;<?php echo date('m/d/Y',strtotime($this->data['UserAgentInformation']['created'])); ?> 
				
				<br />
				<span class="dashLevel"> Agent's Name :</span><?php echo $this->data['UserAgentInformation']['first_name'].'&nbsp;&nbsp;'.$this->data['UserAgentInformation']['last_name']; ?> (<b>ID:&nbsp;<?php echo $this->data['User']['member_id']; ?></b>)
<br /> 

<span class="dashLevel"> Office Name: </span><?php echo $this->data['UserAgentInformation']['office_name']; ?>
<br /> 

<span class="dashLevel"> Agent Lic#: </span><?php echo $this->data['UserAgentInformation']['agent_lic']; ?>
<br /> 

<table>
<tr>
<td width="35%" valign="top">
<span class="dashLevel"> Home Address#: </span>
</td>
<td>
<?php echo $this->data['UserAgentInformation']['address1']."&nbsp;".$this->data['UserAgentInformation']['address2'].'<br>&nbsp;'.
$this->data['UserAgentInformation']['city'].',&nbsp;'.$this->data['UserAgentInformation']['state'].'&nbsp;'.$this->data['UserAgentInformation']['zip']; ?>
</td>
</tr>
</table>

<span class="dashLevel"> Office Number #: </span><?php echo $this->data['UserAgentInformation']['office_number'].'-'.$this->data['UserAgentInformation']['office_number2'].'-'.$this->data['UserAgentInformation']['office_number3'].'<span class="dashLevel">&nbsp; ext : </span>'.$this->data['UserAgentInformation']['office_number_ext']; ?>
<br /> 


<span class="dashLevel"> Cell #: </span><?php echo $this->data['UserAgentInformation']['cell_number'].'-'.$this->data['UserAgentInformation']['cell_number2'].'-'.$this->data['UserAgentInformation']['cell_number3']; ?>
<br /> 


<span class="dashLevel"> Fax #: </span><?php echo $this->data['UserAgentInformation']['fax_number'].'-'.$this->data['UserAgentInformation']['fax_number2'].'-'.$this->data['UserAgentInformation']['fax_number3']; ?>
<br /> 


<span class="dashLevel"> Email: </span><?php echo $this->data['UserAgentInformation']['email']; ?>
<br /> 


<span class="dashLevel"> Alt Email #: </span><?php echo $this->data['UserAgentInformation']['alt_email']; ?>
<br /> 

<span class="dashLevel"> Website: </span><?php echo $this->data['UserAgentInformation']['website']; ?>
<br /> 
<br />

<span class="dashLevel"> Broker Name: </span><?php echo $this->data['UserBrokerInformation']['first_name'].'&nbsp;&nbsp;'.$this->data['UserBrokerInformation']['last_name']; ?>
<br /> 
<span class="dashLevel"> Broker Lic #: </span><?php echo $this->data['UserBrokerInformation']['broker_lic']; ?>
<br /> 

<span class="dashLevel"> Office Address#: </span><?php echo $this->data['UserBrokerInformation']['office_address1']."&nbsp;".$this->data['UserBrokerInformation']['address2'].'&nbsp;'.
$this->data['UserBrokerInformation']['city'].',&nbsp;'.$this->data['UserBrokerInformation']['state'].'&nbsp;'.$this->data['UserBrokerInformation']['zip']; ?>
<br /> 



		
													
													
													
													<div style="clear:both;"> </div>								
												  <div style="clear:both;"> </div>	
												  <div style="clear:both;"> </div>	
												
												
												
												</div>												
																	
												
												
												
												
												</div>			
									 </div>	
									
									
								</div>					
					
					
					</div>
					
					<div style="float:left; width:50%; height:1000px; margin:10px 0px 0px 0px;">
					
					<span id="agentName">AGENT INFO </span>
					
					<div id="contactInfoUpdateForm">
					
				<?php echo $this->Form->create(null, array('id'=>'create_an_account','controller'=>'dashboards','action' => 'contactinfo_edit'));
				
						echo $this->Form->input('User.id',array('type'=>'hidden'));
						echo $this->Form->input('UserAgentInformation.id',array('type'=>'hidden'));
						echo $this->Form->input('UserBrokerInformation.id',array('type'=>'hidden'));
				 ?>

					<table width='100%'>
				<tr>
					
					<td style='width:30%;text-align:right;'>  First Name *</td> 
					<td style='width:60%;  text-align:left;'>
					
	
					 <?php echo $this->Form->input('UserAgentInformation.first_name',array('type'=>'text','label'=>false,'div'=>false,'id'=>'first_name','size'=>'11'));?>
					
					
					&nbsp;&nbsp;  Last Name *
					
					 <?php echo $this->Form->input('UserAgentInformation.last_name',array('type'=>'text','label'=>false,'div'=>false,'id'=>'last_name','size'=>'10'));?>
					 </td>
				</tr>
				
				<tr>
					
					<td style='width:30%;text-align:right;'>  Office Name*</td> 
					<td style='width:60%;  text-align:left;'>
					
					 <?php echo $this->Form->input('UserAgentInformation.office_name',array('type'=>'text','label'=>false,'div'=>false,'id'=>'office_name','size'=>'43'));?>
				 </td>
				</tr>
				
				
				<tr>
				
					<td style='width:30%;text-align:right;'>  Agent Lic #*</td> 
					<td style='width:60%;  text-align:left;'>
						 <?php echo $this->Form->input('UserAgentInformation.agent_lic',array('type'=>'text','label'=>false,'div'=>false,'id'=>'agent_lic','size'=>'43'));?>
					
					 </td>
				</tr>
				
				<tr>
					
					<td style='width:30%;text-align:right;'>  Home Address 1 *</td> 
					<td style='width:60%;  text-align:left;'>
					
					 <?php echo $this->Form->input('UserAgentInformation.address1',array('type'=>'text','label'=>false,'div'=>false,'id'=>'address1','size'=>'43'));?>
					 </td>
				</tr>
				
				
				<tr>
					<td style='width:30%;text-align:right;'>   Address 2</td> 
					<td style='width:60%;  text-align:left;'>
					
					 <?php echo $this->Form->input('UserAgentInformation.address2',array('type'=>'text','label'=>false,'div'=>false,'id'=>'address2','size'=>'43'));?>
					</td>
				</tr>
				
				
				<tr>
				
					<td style='width:30%;text-align:right;'>  City *</td> 
					<td style='width:60%;  text-align:left;'>
							
						<?php echo $this->Form->input('UserAgentInformation.city',array('type'=>'text','label'=>false,'div'=>false,'id'=>'city','size'=>'4'));?>
						 &nbsp;&nbsp;&nbsp;
						State *&nbsp;&nbsp;<select name="data[UserAgentInformation][state]" style="width:67px;"> 
						
						<?php
						
							
							foreach($stateLists as $key=>$data)
							{
							
								$name = $data['State']['name'];
								$code = $data['State']['code'];
								
								if($this->data['UserAgentInformation']['state'] == $code)
								{
									$selected ='selected';
								}
								else
								{
									$selected = '';							
								}	
								
								
								echo "<option  $selected value='$code'>$name</option>";	
							
							
							}
						
						
						
						?>
						
						</select>
						
						
						

						&nbsp;&nbsp;&nbsp;ZIP &nbsp; <?php echo $this->Form->input('UserAgentInformation.zip',array('type'=>'text','label'=>false,'div'=>false,'id'=>'zip','size'=>'5'));?>
				
						
					</td>
				</tr>
				
				
				<tr>
					<td style='width:20%;text-align:right;'>   Office Number * </td> 
					<td style='width:60%;  text-align:left;'>
		<?php echo $this->Form->input('UserAgentInformation.office_number',array('type'=>'text','label'=>false,'div'=>false,'id'=>'office_number1','size'=>'3'));?>
			&nbsp;		 
		<?php echo $this->Form->input('UserAgentInformation.office_number2',array('type'=>'text','label'=>false,'div'=>false,'id'=>'office_number2','size'=>'3'));?>
			&nbsp;		 
		<?php echo $this->Form->input('UserAgentInformation.office_number3',array('type'=>'text','label'=>false,'div'=>false,'id'=>'office_number3','size'=>'3'));?>
				
					&nbsp;
			ext&nbsp;
					<?php echo $this->Form->input('UserAgentInformation.office_number_ext',array('type'=>'text','label'=>false,'div'=>false,'id'=>'office_ext','size'=>'4'));?>
		
					</td>
				</tr>
				
				
				<tr>
					<td style='width:20%;text-align:right;'>   Cell Number </td> 
					<td style='width:60%;  text-align:left;'>
					
		<?php echo $this->Form->input('UserAgentInformation.cell_number',array('type'=>'text','label'=>false,'div'=>false,'id'=>'cell_number1','size'=>'3'));?>
			&nbsp;		 
		<?php echo $this->Form->input('UserAgentInformation.cell_number2',array('type'=>'text','label'=>false,'div'=>false,'id'=>'cell_number2','size'=>'3'));?>
			&nbsp;		 
		<?php echo $this->Form->input('UserAgentInformation.cell_number3',array('type'=>'text','label'=>false,'div'=>false,'id'=>'cell_number3','size'=>'3'));?>
		
					
					</td>
				</tr>
				
				<tr>
					<td style='width:20%;text-align:right;'>   Fax Number</td> 
				<td style='width:60%;  text-align:left;'>
				
		<?php echo $this->Form->input('UserAgentInformation.fax_number',array('type'=>'text','label'=>false,'div'=>false,'id'=>'fax1','size'=>'3'));?>
			&nbsp;		 
		<?php echo $this->Form->input('UserAgentInformation.fax_number2',array('type'=>'text','label'=>false,'div'=>false,'id'=>'fax2','size'=>'3'));?>
			&nbsp;		 
		<?php echo $this->Form->input('UserAgentInformation.fax_number3',array('type'=>'text','label'=>false,'div'=>false,'id'=>'fax3','size'=>'3'));?>
		
				
				
				
				</td>
				</tr>
				
				
				
					<tr>
					<td style='width:30%;text-align:right;'>   Email *</td> 
					<td style='width:60%;  text-align:left;'>
					<?php echo $this->Form->input('UserAgentInformation.email',array('type'=>'text','label'=>false,'div'=>false,'id'=>'email_address','size'=>'43'));?>
					
					</td>
				</tr>
				
				
				<tr>
					<td style='width:30%;text-align:right;'>   Alt Email</td> 
					<td style='width:60%;  text-align:left;'>
					<?php echo $this->Form->input('UserAgentInformation.alt_email',array('type'=>'text','label'=>false,'div'=>false,'id'=>'alt_email','size'=>'43'));?>
					
					
					</td>
				</tr>
				
				<tr>
					<td style='width:30%;text-align:right;'>   Website</td> 
					<td style='width:60%;  text-align:left;'>
						<?php echo $this->Form->input('UserAgentInformation.website',array('type'=>'text','label'=>false,'div'=>false,'id'=>'website','size'=>'43'));?>
				
					
					 </td>
				</tr>
				
			
			</table>
			
			
			
			
			<span id="agentName">BROKER INFO</span>
			<table width='100%'>
				<tr>
					 
				<td style='width:30%;text-align:right;'>  First Name *</td> 
					<td style='width:60%;  text-align:left;'>
						<?php echo $this->Form->input('UserBrokerInformation.first_name',array('type'=>'text','label'=>false,'div'=>false,'id'=>'broker_first_name','size'=>'11'));?>
				
					
					&nbsp;&nbsp; Last Name *&nbsp;
						<?php echo $this->Form->input('UserBrokerInformation.last_name',array('type'=>'text','label'=>false,'div'=>false,'id'=>'broker_last_name','size'=>'10'));?>
					
					</td>
				</tr>
				
				<tr>
					<td style='width:30%;text-align:right;'>  Broker Lic#*</td> 
					<td style='width:60%;  text-align:left;'>
					
				<?php echo $this->Form->input('UserBrokerInformation.broker_lic',array('type'=>'text','label'=>false,'div'=>false,'id'=>'broker_lic','size'=>'27'));?>
					
					 </td>
				</tr>
				
				
			
				<tr>
					<td style='width:30%;text-align:right;'>  Office Address 1*</td> 
					<td style='width:60%;  text-align:left;'>
					
		<?php echo $this->Form->input('UserBrokerInformation.office_address1',array('type'=>'text','label'=>false,'div'=>false,'id'=>'broker_address1','size'=>'43'));?>
				
				</td>
				</tr>
				
				
				<tr>
					<td style='width:30%;text-align:right;'>   Address 2</td> 
					<td style='width:60%;  text-align:left;'>
					<?php echo $this->Form->input('UserBrokerInformation.address2',array('type'=>'text','label'=>false,'div'=>false,'id'=>'broker_address2','size'=>'43'));?>
		
					
					 </td>
				</tr>
				
				
				
				<tr>
					<td style='width:30%;text-align:right;'>  City *</td> 
					<td style='width:60%;  text-align:left;'>
						
						<?php echo $this->Form->input('UserBrokerInformation.city',array('type'=>'text','label'=>false,'div'=>false,'id'=>'broker_city','size'=>'5'));?>
		
						
						 &nbsp;&nbsp;&nbsp;
						State *&nbsp;&nbsp;<select id="broker_state" name='data[UserBrokerInformation][state]' style="width:60px;"> 
						
						
						UserBrokerInformation
						
						
						<?php
						
							
							foreach($stateLists as $key=>$data)
							{
							
								$name = $data['State']['name'];
								$code = $data['State']['code'];
								
								if($this->data['UserBrokerInformation']['state'] == $code)
								{
									$selected ='selected';
								}
								else
								{
									$selected = '';							
								}	
								
								
								echo "<option  $selected value='$code'>$name</option>";	
							
							
							}
						
						
						
						?>
						
						</select>
						
				
 
 
 

						&nbsp;&nbsp;&nbsp;ZIP *&nbsp;
				<?php echo $this->Form->input('UserBrokerInformation.zip',array('type'=>'text','label'=>false,'div'=>false,'id'=>'broker_zip','size'=>'4'));?>
					
						
					</td>
				</tr>
				
				
			
			</table>
			
			
			
			<div style="clear:both;"> </div>
			<br />
				<table width='100%'>
				<tr>
					<td style='width:750px; text-align:left; padding-left:20px;'> *Required field </td> 
					
					
					<td>
					<a href="<?php echo $this->webroot.'dashboards/'; ?>"> <img src="<?php echo $this->webroot.'img/back.png'; ?>" /> </a>
					
					 </td>
					
					<td>
					<?php echo $form->submit('update.png', array('div'=>false)); ?>
					
					 </td>
					
					
				</tr>
			</table>
						
					
					</div>
					
					
										
					
				</div>
							
					
					</div>			
		 </div>	
		
		
	</div>
</div>
	
	
	<div style="clear:both; float:left; margin-top:10px; height:20px;"> </div>
	























