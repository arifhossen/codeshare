<div class="shipping-info-main-box">
					
				<table width="100%" border="0">
				<tr>
				<td colspan="3" bgcolor="#CCCCCC"><div align="left"><strong>Order Information </strong></div></td>
				</tr>
				
				<tr>
				<td width="20%"><div align="left">Order  Number</div></td>
				<td width="1%">:</td>
				<td width="79%"><div align="left"><?php echo $orders['Order']['id']; ?></div></td>
				</tr>
				
				<tr>
				  <td><div align="left">Order  Date</div></td>
				  <td>:</td>
				  <td><div align="left"><?php echo $orders['Order']['created']; ?></div></td>
				  </tr>
				<tr>
				  <td><div align="left">Order  Status</div></td>
				  <td>:</td>
				  <td><div align="left">
				  
				  <?php
					if($orders['Order']['isActive']==0){
				
					 echo $order_status = 'Pending';
					}
					else if($orders['Order']['isActive']==1){
						
						echo $order_status = 'Processing';
					}
					else if($orders['Order']['isActive']==2){
						
						echo $order_status = 'Complete';
					}
					else if($orders['Order']['isActive']==3){
						
						echo $order_status = 'Delivered';
					}
					else{
						echo $order_status = 'Pending';
					}
					
				   ?>
				  
				  </div></td>
				  </tr>
				<tr>
				<td><div align="left"></div></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
					<table width="100%" border="0">
					<tr>
					<td colspan="4" bgcolor="#CCCCCC"><div align="left"><strong>Product Information </strong></div></td>
					</tr>
					<tr>
					<td width="17%"><div align="left"><strong>Quantity</strong></div></td>
					<td width="26%"><div align="left"><strong>Name</strong></div></td>
					<td width="22%"><div align="left"><strong>Product Code</strong></div></td>
					<td width="18%"><div align="right"><strong>Price</strong></div></td>
					</tr>
					
					<?php	
					$grandTotal=0;	
					$currency_value = 80;					
					
					
					foreach($orders['OrderProduct'] as $key => $value):	
					$itemPrice = $value['unit_price'];
					?>
					<tr>
					<td height="23"><div align="left"><?php echo $value['quantity']; ?></div></td>
					<td><div align="left"><?php echo $value['product_name']; ?></div></td>
					<td><div align="left"><?php echo $value['product_code']; ?></div></td>
					<td>$<?php echo number_format(($itemPrice/$currency_value),2); ?></td>
					</tr>
					<?php					
					
					$grandTotal += $value['unit_price'];
					
					endforeach;
					?>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><div align="right"><strong>Total</strong></div></td>
					<td align="left"><div align="right"><strong>$&nbsp;
		            <?php
					echo number_format(($orders['Order']['total_amount']/$currency_value),2);
					?>					
				    </strong></div></td>
					</tr>
					
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="left">&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					</table>
					
					<table width="100%" border="0">
					<tr>
					<td colspan="3" bgcolor="#CCCCCC"><div align="left"><strong>Customer Information </strong></div></td>
					</tr>
				
					<tr>
					  <td>Full Name </td>
					  <td>&nbsp;</td>
					  <td><?php echo $users['User']['full_name']; ?></td>
					</tr>
					<tr>
					  <td>E-mail</td>
					  <td>&nbsp;</td>
					  <td><?php echo $users['User']['email']; ?></td>
					</tr>
					<tr>
					  <td colspan="3">&nbsp;</td>
					  </tr>
					<tr>
					<td colspan="3"><div align="left"><strong>Ship to Information </strong></div></td>
					</tr>
					
					<tr>
					  <td width="18%"><div align="left">Contact Name</div></td>
					  <td width="1%"> : </td>
					  <td width="81%"><div align="left"><?php echo $orders['Order']['ShippingInformation_contact_name']; ?></div></td>
					</tr>
					<tr>
					  <td><div align="left">Phone No </div></td>
					  <td>:</td>
					  <td><div align="left"><?php echo $orders['Order']['ShippingInformation_contact_no']; ?></div></td>
					  </tr>
					<tr>
					  <td><div align="left">Mobile No </div></td>
					  <td>:</td>
					  <td><div align="left"><?php echo $orders['Order']['ShippingInformation_mobile_no']; ?></div></td>
					</tr>
					<tr>
					  <td><div align="left">Address1</div></td>
					  <td>:</td>
					  <td><div align="left"><?php echo $orders['Order']['ShippingInformation_address1']; ?></div></td>
					</tr>
					<tr>
					  <td>Address2</td>
					  <td>:</td>
					  <td><div align="left"><?php echo $orders['Order']['ShippingInformation_address2']; ?></div></td>
					</tr>
					<tr>
					  <td><div align="left">City</div></td>
					  <td>:</td>
					  <td><div align="left"><?php echo $orders['Order']['ShippingInformation_city']; ?></div></td>
					</tr>
					<tr>
					  <td><div align="left">Zip/Postal Code </div></td>
					  <td>:</td>
					  <td><div align="left"><?php echo $orders['Order']['ShippingInformation_post_code']; ?></div></td>
					</tr>
					<tr>
					  <td><div align="left">Country</div></td>
					  <td>:</td>
					  <td><div align="left"><?php echo $orders['Order']['ShippingInformation_country']; ?></div></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					</tr>
					</table>
					
									
				<table width="100%" border="0">
				<tr>
				<td bgcolor="#CCCCCC"><div align="left"><strong>Customer enquery </strong></div></td>
				</tr>
				
				<tr>
				  <td><?php echo $orders['Order']['order_instruction']; ?></td>
				  </tr>
				<tr>
				  <td>&nbsp;</td>
				  </tr>
				<tr>
				  <td>				</tr>
				</table>
				
				
				<table width="100%" border="0">
				<tr>
				<td bgcolor="#CCCCCC"><div align="left"><strong>Payment Information </strong></div></td>
				</tr>
				
				<tr>
				<td></td>
				</tr>
				<tr>
				  <td><?php echo $orders['Order']['payment_method']; ?></td>
				  </tr>
				<tr>
				  <td>&nbsp;</td>
				  </tr>
				<tr>
				  <td>&nbsp;</td>
				  </tr>
				</table>
</div>

<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>		
		<li><?php echo $this->Html->link(__('List Orders', true), array('action' => 'index')); ?> </li>		
	</ul>
</div>
