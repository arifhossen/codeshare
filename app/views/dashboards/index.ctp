<div id="userdashboard">

<div id="dashboarheader">
	<?php echo $this->element('dashboard_header'); ?>
</div>




<div id="dashboardContentArea">


	<div class="left">		
		
		
		<div id="dashboardBox">			
			<div id="titleBarLeft">
				<div id="headerLeftBar"> </div>
				<div id="headerMiddelBar"> <div class='leftTitle'>Profile Pic</div> <div class='rightTitle'> <a href="<?php echo $this->webroot.'dashboards/profile_edit'; ?>"> [ edit  pic ]</a></div>   </div>
				<div id="headerRightBar"> </div>
		    </div>	
			
			<div class="content" style="text-align:center;">
			<?php
					
						if(isset($userInfo))
						{
						
							$picture = $userInfo['User']['picture'];
							
							if($picture == '')
							{
								$file = $this->webroot.'images/profile/profile_pic.jpg'; 
								echo "<img src='$file' width='150' height='140' /> ";
							
							}
							else
							{
								$file = $this->webroot.$picture;
								
								echo "<img src='$file' width='150'  height='140' /> ";															
							
							}
						
						
						
						}
					
					
					?>			
			<br /><br />	
			</div>			
		</div>
		
		
		<div class='dividerSpace'> </div>		
		
		<div id="dashboardBox">			
			<div id="titleBarLeft">
				<div id="headerLeftBar"> </div>
				<div id="headerMiddelBar"> <div class='leftTitle'>Market Info</div> <div class='rightTitle'> <a href="#"> [ Dallas-Fort Worth ]</a></div>   </div>
				<div id="headerRightBar"> </div>
		    </div>	
			
			<div class="content" style='padding-top:10px;'> 			
				<span class="dashLevel"> Referred By : </span> <?php echo $userInfo['User']['referral_full_name']. ' (ID: '.$userInfo['User']['referral_member_id'].')'; ?> 
				<br />
				<span class="dashLevel"> Sales Rep : </span>  
			</div>			
		</div>
		
		
		
		
		<div class='dividerSpace'> </div>		
		
		<div id="dashboardBox">			
			<div id="titleBarLeft">
				<div id="headerLeftBar"> </div>
				<div id="headerMiddelBar"> <div class='leftTitle'>Contact Info</div> <div class='rightTitle'> <a href="<?php echo $this->webroot.'dashboards/contactinfo_edit'; ?>"> [ Edit ]</a></div>   </div>
				<div id="headerRightBar"> </div>
		    </div>	
			
			<div class="content" style='padding-top:10px;'> 			
				<span class="dashLevel"> Member Since: </span>&nbsp;<?php  echo date('m/d/Y',strtotime($userInfo['UserAgentInformation']['created'])); ?> 
				
				<br />
				<span class="dashLevel"> Agent's Name :</span>&nbsp;<?php echo $userInfo['UserAgentInformation']['first_name'].'&nbsp;&nbsp;'.$userInfo['UserAgentInformation']['last_name']; ?> (<b>ID:&nbsp;<?php echo $userInfo['User']['member_id']; ?></b>)
<br /> 


<span class="dashLevel"> Office Name:</span>&nbsp;<?php echo $userInfo['UserAgentInformation']['office_name']; ?>
<br /> 

<span class="dashLevel"> Agent Lic#:</span>&nbsp;<?php echo $userInfo['UserAgentInformation']['agent_lic']; ?>
<br /> 
<table>
<tr>
<td width="40%" valign="top">
<span class="dashLevel"> Home Address#: </span>
</td>
<td>
<?php echo $userInfo['UserAgentInformation']['address1']."&nbsp;".$userInfo['UserAgentInformation']['address2'].'<br>&nbsp;'.
$userInfo['UserAgentInformation']['city'].',&nbsp;'.$userInfo['UserAgentInformation']['state'].'&nbsp;'.$userInfo['UserAgentInformation']['zip']; ?>
</td>
</tr>
</table>



<span class="dashLevel"> Cell #: </span><?php echo $userInfo['UserAgentInformation']['cell_number'].'-'.$userInfo['UserAgentInformation']['cell_number2'].'-'.$userInfo['UserAgentInformation']['cell_number3']; ?>
<br /> 


<span class="dashLevel"> Fax #: </span><?php echo $userInfo['UserAgentInformation']['fax_number'].'-'.$userInfo['UserAgentInformation']['fax_number2'].'-'.$userInfo['UserAgentInformation']['fax_number3']; ?>
<br /> 


<span class="dashLevel"> Email: &nbsp;</span><?php echo $userInfo['UserAgentInformation']['email']; ?>
<br /> 


<span class="dashLevel"> Alt Email #:&nbsp;</span><?php echo $userInfo['UserAgentInformation']['alt_email']; ?>
<br /> 

<span class="dashLevel"> Website:</span>&nbsp;<?php echo $userInfo['UserAgentInformation']['website']; ?>
<br /> 
<br />

<span class="dashLevel"> Broker Name: </span>
<?php echo $userInfo['UserBrokerInformation']['first_name']."&nbsp;&nbsp;".$userInfo['UserBrokerInformation']['last_name']; ?>
<br /> 
<span class="dashLevel"> Broker Lic #: </span><?php echo $userInfo['UserBrokerInformation']['broker_lic']; ?>
<br /> 

<span class="dashLevel"> Office Address#: </span><?php echo $userInfo['UserBrokerInformation']['office_address1']."&nbsp;".$userInfo['UserBrokerInformation']['address2'].'&nbsp;'.
$userInfo['UserBrokerInformation']['city'].',&nbsp;'.$userInfo['UserBrokerInformation']['state'].'&nbsp;'.$userInfo['UserBrokerInformation']['zip']; ?>
<br /> 

				<br />
				<br />
			</div>			
		</div>
		
		
		
		
		<div class='dividerSpace'> </div>		
		
		<div id="dashboardBox">			
			<div id="titleBarLeft">
				<div id="headerLeftBar"> </div>
				<div id="headerMiddelBar"> <div class='leftTitle'>Billing Info</div> <div class='rightTitle'>  <a href="<?php echo $this->webroot.'dashboards/billinginfo_edit'; ?>"> [edit]</a> <a href="#"> [pay bill]</a><a href="#"> [statements]</a></div>   </div>
				<div id="headerRightBar"> </div>
		    </div>	
			
			<div class="content" style='padding-top:10px;'> 			
				<?php
 $orderInfo = $userInfo['UserOrderInformation'];
// pr($orderInfo);
 
 ?>
<span class="dashLevel"> Plan Type 1 :</span><?php echo  $orderInfo[0]['Package']['name']; ?>(<?php echo  $orderInfo[0]['Term']['month_name']; ?>)
<br /> 

<span class="dashLevel"> Contact:</span>
<br /> 
<span class="dashLevel"> Due date:</span>
<br /> 
<span class="dashLevel"> Amount Due:</span><?php echo  $orderInfo[0]['amount']; ?>
<br /> 

<span class="dashLevel"> Card Type date:</span><?php echo  $orderInfo[0]['type']; ?>
<br /> 
<span class="dashLevel"> Name on Card:</span><?php echo  $orderInfo[0]['name_on_card']; ?>
<br /> 
<span class="dashLevel"> Card Number:</span>
<?php echo $ccmask = str_pad(substr( $orderInfo[0]['card_number'], -4), strlen( $orderInfo[0]['card_number']), 'x', STR_PAD_LEFT);  ?>

<br /> 
<span class="dashLevel"> Exp Date:</span><?php echo  $orderInfo[0]['month'].'/'.$orderInfo[0]['year']; ?><br /> 
<br /> 

<span class="dashLevel"> CCV Code:</span><?php echo  $orderInfo[0]['cvv_no']; ?><br /> 
<br /> 


<table>
<tr>
<td width="50%" valign="top">
<span class="dashLevel"> Billing Address: </span>
</td>
<td>
 <?php echo  $orderInfo[0]['billing_address']; ?> 
</td>
</tr>
</table>



				
				<br />
				<br />
				<br />
			</div>			
		</div>
		
		
		
		
		
	
	</div>
	
	
	<div class="right">
 
 	<div id="dashboardBox">			
			<div id="titleBarRight">
				<div id="headerLeftBar"> </div>
				<div id="headerMiddelBar"> <div class='leftTitle'>Alerts</div> <div class='rightTitle'> <a href="<?php echo $this->webroot.'dashboards/alerts'; ?>"> [ read all alerts]</a></div>   </div>
				<div id="headerRightBar"> </div>
		    </div>	
			
			<div class="content" style="text-align:center; background-color:#FFFF00; font-weight:bold; text-align:justify;">
			
			<?php
		
			if(!empty($newsLists))
			{
			
				foreach($alerts as $key=>$alert)
				{
				
				
					
					echo $description  = $alert['Alert']['description'];
					
					
					
										
					
				 }				
			
			}
		
		
		
		?>
			
					
			<br /><br />	
			</div>			
	</div>
	
	<div class='dividerSpace'> </div>		
	<div id="dashboardBox">			
			<div id="titleBarRight">
				<div id="headerLeftBar"> </div>
				<div id="headerMiddelBar"> <div class='leftTitle'>News</div> <div class='rightTitle'> <a href="<?php echo $this->webroot.'dashboards/news'; ?>"> [ read all news]</a></div>   </div>
				<div id="headerRightBar"> </div>
		    </div>	
			
			<div class="content" style="text-align:center;">
			
			<br /><br />
			
			<div style="height:300px; overflow:scroll">
			<table>
			
				<tr> <td colspan='2'><hr></td> </tr>
			<?php
		
			if(!empty($newsLists))
			{
			
				foreach($newsLists as $key=>$news)
				{
				
					$id = $news['News']['id'];
					$title = $news['News']['title'];
					$author = $news['News']['author'];
					$created = 	date('F d,Y   h:i a',strtotime($news['News']['created']));
					$description  = $news['News']['description'];
					$image  = $news['News']['image'];
					
					$img = $this->webroot.$image;
					
					echo "<tr>";
						echo "<td><a href='./dashboards/news_details/$id'><img src='$img' height='100' width='100' style='float:left;'></a> </td>";
						echo "<td>
								<a href='./dashboards/news_details/$id'><span id='newsTitle'>$title</span></a>
								<br>
								Posted  $created       by $author
								<br><br>
								$description
						 </td>";
					echo "</tr>";	
					
					echo "<tr> <td colspan='2'><hr></td> </tr>";
				
										
					
				 }				
			
			}
		
		
		
		?>
		</table>
		</div>
			
			
			<br /><br />	
			</div>			
	</div>
	
	
	<div class='dividerSpace'> </div>		
	<div id="dashboardBox">			
			<div id="titleBarRight">
				<div id="headerLeftBar"> </div>
				<div id="headerMiddelBar"> <div class='leftTitle'>Referral Summary</div> <div class='rightTitle'> <a href="<?php echo $this->webroot.'dashboards/referral_summary'; ?>"> [ read all]</a></div>   </div>
				<div id="headerRightBar"> </div>
		    </div>	
			
			<div class="content" style="text-align:center;">
	
			<table width="100%" style="text-align:left;" id="alterRowTable">
		<tr class="alterRowColor">
			<th> Id</th> <th>First Name </th> <th>Last Name </th> <th> Date Signed</th> <th>Plan </th>
		</tr>	
		
		
		<?php
		
		
		if(!empty($referralSummaryLists))
		{
		    $count = 0;
			foreach($referralSummaryLists as $data)
			{
				
				$member_id = $data['User']['member_id'];
				$first_name = $data['UserAgentInformation']['first_name'];
				$last_name = $data['UserAgentInformation']['last_name'];
				$date_signed_up =date("d/m/Y", strtotime($data['User']['signup_date']));
				$plan = 'none';
				
				
				if($count%2 == 1)
				{
					$class = "class='alterRowColor'";
				}
				else
				{
					$class = '';
				}
				
			 
			 	echo "
					<tr $class>
						<td> $member_id</td> <td>$first_name </td> <td>$last_name </td> <td>$date_signed_up</td> <td>$plan</td>
					</tr>";
			 
			
			$count++;
			}
		
		
		
		}
			
		
		
		
		?>
		
		
		
	
	
		
	
	</table>
			<br /><br />	
			</div>			
	</div>
	
	
	<div class='dividerSpace'> </div>		
	<div id="dashboardBox">			
			<div id="titleBarRight">
				<div id="headerLeftBar"> </div>
				<div id="headerMiddelBar"> <div class='leftTitle'>Reward History</div> <div class='rightTitle'> <a href="<?php echo $this->webroot.'dashboards/reward_history'; ?>"> [ read all]</a></div>   </div>
				<div id="headerRightBar"> </div>
		    </div>	
			
			<div class="content" style="text-align:left;">
			
			
	
			<br /><br />	
			</div>			
	</div>







</div>

</div>
	
	
	<div style="clear:both; float:left; margin-top:10px; height:20px;"> </div>
	























