<?php echo $javascript->link('/js/tiny_mce/tiny_mce.js'); ?>
<script type="text/javascript">
	tinyMCE.init({
		theme : "advanced",
		mode : "textareas",
		convert_urls : false
	});
</script>
<h1>Add New  </h1>
<div class="tabitem">
<div class="common_from">
<?php echo $this->Form->create('News',array('enctype'=>"multipart/form-data")); ?>


    <div class="element">
        <div class="title">Title:</div>
        <div class="value">
			<?php echo $form->input('title', array('label' => false, 'div' => false)); ?>          
        </div>
    </div>
	<br />

    <div class="element">
        <div class="title">Description:</div>
        <div class="value">			
			 <?php echo $this->Form->input('description',array('type'=>'textarea','label'=>false));?>           
        </div>
    </div>
	<br />
	
	 <div class="element">
        <div class="title">Author By:</div>
        <div class="value">
			<?php echo $form->input('author', array('label' => false, 'div' => false)); ?>          
        </div>
    </div>
	
	 <div class="element">
        <div class="title">Upload Picture:</div>
        <div class="value">
			<?php echo $form->input('picture', array('type'=>'file','label' => false, 'div' => false)); ?>          
        </div>
    </div>


    <div class="button-container">
        <button value="signin" name="btnAction" type="submit"><span>Submit</span></button>
        or         <?php echo $this->Html->link(__('Back to list', true), array('action' => 'index')); ?>

    </div>
</div>
</div>
