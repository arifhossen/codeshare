

<!-- Jquery Simple Slideshow with no controls CSS -->
<link type="text/css" href="<?php echo $this->webroot.'css/slideshow'; ?>/simple_slideshow.css" rel="stylesheet" />
<!-- END JQuery slideshow with NO Controls css -->




<!-- JQuery slideshow with tooltips css -->
<link type="text/css" href="<?php echo $this->webroot.'css/slideshow'; ?>/bottom.css" rel="stylesheet" />
<!-- END JQuery slideshow with tooltips css -->



<script type="text/javascript" src="<?php echo $this->webroot.'js'; ?>/jquery.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot.'jquery/slideshow'; ?>/jquery.codeshare.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot.'jquery/slideshow'; ?>/jquery.jcarousel.min.js"></script>
<script language="javascript">


 
	jQuery(document).ready(function (){
	
	    /***************** Jquery Simple Slideshow with no controls ***********/
	
			jQuery("#slidelist").PikaChoose();		
			
		/********************* End jquery simple slideshow with no controls ******/
		
		
		 /***************** Jquery Simple Slideshow with tooltips ***********/
	
			$("#slideshow_next_previous_control").PikaChoose({showTooltips:true});	
			
		/********************* End jquery simple slideshow tooltips ******/
		
		
		
			
		});
</script>


<div class="simple_slide_show">

<h1>Jquery Simple Slideshow with no controls </h1>
	<ul id="slidelist" >
		<li><a target="_blank" href="<?php echo $this->webroot.'images/slideshow'; ?>/1_1.jpg"><img  src="<?php echo $this->webroot.'images/slideshow'; ?>/1_1.jpg"/></a><span>Enter title caption 1</span></li>
		<li><a href="<?php echo $this->webroot.'images/slideshow'; ?>/2_2.jpg"><img src="<?php echo $this->webroot.'images/slideshow'; ?>/2_2.jpg"/></a><span>Enter title caption 2</span></li>
		<li><a href="<?php echo $this->webroot.'images/slideshow'; ?>/3_3.jpg"><img src="<?php echo $this->webroot.'images/slideshow'; ?>/3_3.jpg"/></a><span>Enter title caption 3 
		<a href="http://www.codeshare.info">CodeShare.info</a> </span></li>
		
	</ul>
	
<br /><br />

<h6>Jquery Simple Slideshow Source code : </h6>
<br />

	<div style="clear:both"></div>
<a id="download_sourcecode" href="<?php echo $this->webroot.'files/download/jquery_simple_slideshow.zip'; ?>">Download Source Code </a>


<div id="linebreak"> </div>
	
	
	
<h1>Jquery Slideshow with image Tooltips options</h1>
	<ul id="slideshow_next_previous_control" >
		
		<li><a target="_blank" href="<?php echo $this->webroot.'images/slideshow'; ?>/arif0000.jpg"><img  src="<?php echo $this->webroot.'images/slideshow'; ?>/arif0000.jpg"/></a><span>Enter title caption 1</span></li>
		
		<li><a target="_blank" href="<?php echo $this->webroot.'images/slideshow'; ?>/arif0001.jpg"><img  src="<?php echo $this->webroot.'images/slideshow'; ?>/arif0001.jpg"/></a><span>Enter title caption 1</span></li>
		
		<li><a href="<?php echo $this->webroot.'images/slideshow'; ?>/arif0002.jpg"><img src="<?php echo $this->webroot.'images/slideshow'; ?>/arif0002.jpg"/></a><span>Enter title caption 2</span></li>
		<li><a href="<?php echo $this->webroot.'images/slideshow'; ?>/arif0003.jpg"><img src="<?php echo $this->webroot.'images/slideshow'; ?>/arif0003.jpg"/></a><span>Enter title caption 3 
		<a href="http://www.codeshare.info">CodeShare.info</a> </span></li>
		
		
		
	</ul>
	
<div id="linebreak"> </div>

<h6>Jquery Slideshow with image Tooltips  Source code : </h6>
<br />
<div id="linebreak"> </div>
<a id="download_sourcecode" href="<?php echo $this->webroot.'files/download/jquery_simple_slideshow.zip'; ?>">Download Source Code </a>

	
	
	
	
</div>

<div id="linebreak"> </div>
	




