


<div class="row" style='float:right;'>
	<div class="inputs">

	<span class="button blue_button"><span><span><em><a style="float:right;" href="contents/add">Add New </a></em></span></span></span>
		
	</div>		
</div>	

<div style="clear:both;"> </div>							
							
<div class="table_wrapper">
								<div class="table_wrapper_inner">
								
								<table cellpadding="0" cellspacing="0" width="100%">
									<tbody><tr>
										<th style="width: 5px;">Serial.</th>
										<th style="width: 120px;"><?php echo $paginator->sort('Page Name', 'page_name');?></th>
      
											<th style="width: 120px;"><?php echo $paginator->sort('Title', 'page_title');?></th>
      
											
										<th style="width: 120px;"><?php echo $paginator->sort('Status', 'status');?></th>
										<th style="width: 100px;">Actions</th>
										
										
										
									</tr>
									
									
								<?php
                                   if(isset($contents))
 {								   
									$serial = 1;
                                    foreach ($contents AS $data) 
									{

										$id = $data['Content']['id'];
										$page_name = $data['Content']['page_name'];
										$page_title 	 = $data['Content']['page_title'];
										
										if($serial %2 == 1)
										{
											$alter_row = 'first';
										}
										else
										{
										
											$alter_row = 'second';
										
										}
										
										/*
										if($status == 1)
										{
											$checked = 'checked=checked';
										}
										else
										{
										
										 $checked = '';
										}
										
										*/
										

									?>								
									
									<tr class="<?php echo $alter_row; ?>">
										<td style='width:10px;'><?php echo $serial; ?></td>
										
										<td>
										<?php
										echo   $this->Html->link($page_name, array('action'=>'edit', $id), array('escape' => false, 'title' => 'View'));   ?>
										</td>
										
										<td>
										<?php
										echo   $this->Html->link($page_title 	, array('action'=>'edit', $id), array('escape' => false, 'title' => 'View'));   ?>
										</td>
										
										
										
										<td><?php	
										
									
											if($data['Content']['status'] == 1)
											{
											   $wbroot = $this->webroot.'images/admin_panel/approved.gif';
												echo "<img src=$wbroot>";
											
											}
											else
											{
												 $wbroot = $this->webroot.'images/admin_panel/not_approved.gif';
												echo "<img src=$wbroot>";
											
											}
											
										
											?>
										</td>	
										
										<td>
										
										<?php							
	echo   $this->Html->link($this->Html->image('admin/icon_edit.gif'), array('action'=>'edit', $id), array('escape' => false, 'title' => 'Edit'));
	
	echo "&nbsp;&nbsp;&nbsp;&nbsp;";
		echo $this->Html->link($this->Html->image('admin/icon_delete.gif'), array('action'=>'delete',$id), array('escape' => false, 'title' => 'Delete'), sprintf(__('Are you sure you want to delete # %s?', true),  $id));
		
?>
										
										</td>																			
										
									</tr>
									
									<?php
									$serial++;
									 
}
									 }
									?>
									
									
								</tbody></table>
								
<div style="float:right;" id="paginator_control">	
	<?php echo $paginator->prev(); ?> -
<?php echo $paginator->numbers(array('separator'=>' - ')); ?>
<?php echo $paginator->next('Next Page'); ?>
</div>
								</div>
							</div>