<div id="creat-an-account-container">
 <?php echo $form->create('User',array('action'=>'add')); ?>
 <input type="hidden" value="7" name="data[User][role_id]">
				<h4>CREATE AN ACCOUNT</h4>
				<div class="persional-information">
					<p><span><b>Personal Information</b></span></p>
				</div>
				<div class="creat-an-account-input-area">
					<table>
						<tr>
							<td>
							<span><b>First Name</b> <sub style="color:#FF0000;">*</sub></span><br />							
							<?php echo $form->input('first_name',array('label'=>false,'div'=>false,'class'=>'login-inpu-email'));?>
							<div class="requred-feild" style="text-align:left;"></div>
							</td>
							<td class="last-name">
							<span><b>Last Name </b></span><br />							
							<?php echo $form->input('last_name',array('label'=>false,'div'=>false,'class'=>'login-inpu-email'));?>
							<div class="requred-feild" style="text-align:left;"></div>
							</td>
						</tr>
						
						<tr>
							<td>
							<span><b>Username</b> <sub style="color:#FF0000;">*</sub></span><br />
							<?php echo $form->input('username',array('label'=>false,'div'=>false));?>
							<div class="requred-feild" style="text-align:left;"></div>												
							</td>
							
							<td class="last-name">
							<span><b>Email Address </b> <sub style="color:#FF0000;">*</sub></span><br />							
							<?php echo $form->input('email',array('label'=>false,'div'=>false,'class'=>'login-inpu-email'));?>
							<div class="requred-feild" style="text-align:left;"></div>
							</td>
						</tr>
						<tr>
							<td>
							<span><b>Password</b> <sub style="color:#FF0000;">*</sub></span><br />
							<?php echo $form->input('clear_password',array('type'=>'password','label'=>false,'div'=>false));?>	
							<div class="requred-feild" style="text-align:left;"></div>	
							<br />							
							<?php echo $form->input('signupfornewsletter',array('type'=>'checkbox','label'=>false,'div'=>false));?>
							<span><b>Sign Up for Newsletter</b></span>
							</td>
							<td class="last-name">
							<span><b>Confirm Password </b> <sub style="color:#FF0000;">*</sub></span><br />							
							<?php echo $form->input('confirm_password',array('type'=>'password','label'=>false,'div'=>false));?>
							<div class="requred-feild" style="text-align:left;"></div>
							</td>
						</tr>
					</table>
				</div>
				
				<div class="persional-information">
					<p><span><b>Billing Information</b></span></p>
				</div>
				
				<div class="creat-an-account-input-area">
					<table>
						<tr>
							<td>
							<span><b>Contact Name</b> <sub style="color:#FF0000;">*</sub></span><br />							
							<?php echo $form->input('contact_name',array('label'=>false,'div'=>false,'class'=>'login-inpu-email'));?>
							<div class="requred-feild" style="text-align:left;"></div>							</td>
							<td >
							<span><b>Phone No </b></span><br />							
							<?php echo $form->input('contact_no',array('label'=>false,'div'=>false,'class'=>'login-inpu-email'));?>
							<div class="requred-feild" style="text-align:left;"></div>							</td>
						</tr>
						
						<tr>
							<td>
							<span><b>Mobile No </b> <sub style="color:#FF0000;">*</sub></span><br />							
							<?php echo $form->input('mobile_no',array('label'=>false,'div'=>false,'class'=>'login-inpu-email'));?>
							<div class="requred-feild" style="text-align:left;"></div>							</td>
						
						 <td>
							<span><b>Address1 </b> <sub style="color:#FF0000;">*</sub></span><br />							
							<?php echo $form->input('address1',array('label'=>false,'div'=>false,'class'=>'login-inpu-email'));?>
							<div class="requred-feild" style="text-align:left;"></div>							</td>
						</tr>
						<tr>
							<td>
							<span><b>Address2 </b></span><br />							
							<?php echo $form->input('address2',array('label'=>false,'div'=>false,'class'=>'login-inpu-email'));?>
							<div class="requred-feild" style="text-align:left;"></div>							</td>
							
							<td>
							<span><b>City </b> <sub style="color:#FF0000;">*</sub></span><br />							
							<?php echo $form->input('city',array('value'=>'Dhaka','label'=>false,'div'=>false,'class'=>'login-inpu-email'));?>
							<div class="requred-feild" style="text-align:left;"></div>							</td>
						</tr>
						<tr>
							<td>
							<span><b>Zip/Postal Code </b> </span><br />							
							<?php echo $form->input('post_code',array('label'=>false,'div'=>false,'class'=>'login-inpu-email'));?>
							<div class="requred-feild" style="text-align:left;"></div>							</td>
							<td>
							<span><b>Country </b> <sub style="color:#FF0000;">*</sub></span><br />							
							<?php echo $form->input('country',array('value'=>'Bangladesh','label'=>false,'div'=>false,'class'=>'login-inpu-email'));?>
							<div class="requred-feild" style="text-align:left;"> </div>							</td>
						</tr>
					</table>
				</div>
				
				<div class="submit-main-box" style="border-top: 1px solid #CCCCCC;">
					<span class="forgot-password"><a href="#" style="color:#005b9a;"><b>&lt;&lt; BACK</b></a></span>					
					<?php echo $form->submit('submit.jpg', array('div'=>false,'class'=>'submit-btn-box')); ?>
					<div class="clear"></div>
				</div>
			<div class="clear"></div>	
			</div>