<script type="text/javascript">
    $(function(){
        $('#left_content li').removeClass('selLeft');
        $('#admin_mngt_list').addClass('selLeft');
        $('.nav_admin').show();
        $('#adminManage').css('font-weight','bold');
    });
</script>
<div class="action_title"><span>Update User information</span></div>
<div class="tabitem">
    <div class="common_from">
    <?php echo $this->Form->create("User",array('enctype'=>"multipart/form-data")); echo $form->input('id');?>
      
	   <div class="element">
            <div class="title">User Role:</div>
            <div class="value">
            <?php echo $form->input('role_id',array('label'=>false,'div'=>false));?>
            </div>
        </div>
	    <div class="element">
            <div class="title">Full name:</div>
            <div class="value">
            <?php echo $form->input('full_name',array('label'=>false,'div'=>false));?>
            </div>
        </div>


        <div class="element">
            <div class="title">Username:</div>
            <div class="value">
            <?php echo $form->input('username',array('label'=>false,'div'=>false));?>
            </div>
        </div>

        <div class="element">
            <div class="title">Password:</div>
            <div class="value">
            <?php echo $form->input('password',array('label'=>false,'div'=>false));?>

            </div>
        </div>


        <div class="element">
            <div class="title">Confirm password:</div>
            <div class="value">
            <?php echo $form->input('confirm_password',array('label'=>false,'div'=>false,'type'=>'password'));?>

            </div>
        </div>

        <div class="element">
            <div class="title">Email:</div>
            <div class="value">
            <?php echo $form->input('email',array('label'=>false,'div'=>false));?>

            </div>
        </div>
    </div>
</div>
<div class="button-container">
  <button value="signin" name="btnAction" type="submit"><span>Submit</span></button>
    or <?php echo $this->Html->link(__('Back to list', true),array('action' => 'index')); ?>
</div>