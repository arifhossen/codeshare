<div class="section">
					
					<!--[if !IE]>start title wrapper<![endif]-->
					<div class="title_wrapper">
						<span class="title_wrapper_top"></span>
						<div class="title_wrapper_inner">
							<span class="title_wrapper_middle"></span>
							<div class="title_wrapper_content">
								<h2>Users List:</h2>
							</div>
						</div>
						<span class="title_wrapper_bottom"></span>
					</div>
					<!--[if !IE]>end title wrapper<![endif]-->
					
					<!--[if !IE]>start section content<![endif]-->
					<div class="section_content">
						<span class="section_content_top"></span>
						
						<div class="section_content_inner">
							
							<div class="table_tabs_menu">
							<!--[if !IE]>start  tabs<![endif]-->
					
							<!--[if !IE]>end  tabs<![endif]-->
							<a href="add" class="update"><span><span><em>Add New Record</em><strong></strong></span></span></a>
							</div>
						
							<!--[if !IE]>start table_wrapper<![endif]-->
							<div class="table_wrapper">
								<div class="table_wrapper_inner">
								<table cellpadding="0" cellspacing="0" width="100%">
									<tbody><tr>
										<th style="width: 17px;">No.</th>
										
										<th style="width: 120px;"><a href="#" class="asc">User Name</a></th>
										<th style="width: 100px;">Full Name</th>
										<th style="width: 100px;"><a href="#" class="desc">Signup Date</a></th>
									
										
									</tr>
									
									
								<?php
                                    
									$serial = 1;
                                    foreach ($users AS $user) 
									{

										$id = $user['User']['id'];
										
										if($serial %2 == 1)
										{
											$alter_row = 'first';
										}
										else
										{
										
											$alter_row = 'second';
										
										}

									?>								
									
									<tr class="<?php echo $alter_row; ?>">
										<td><?php echo $serial; ?></td>
										<td><a href="#" class="product_name"><?php	echo $user['User']['username'];  ?></a></td>
										<td><?php	echo $user['User']['full_name'];  ?></td>
										<td><?php	echo $user['User']['signup_date'];  ?></td>
										
										
									</tr>
									
									<?php
									$serial++;
									    }
									?>
									
									
								</tbody></table>
								</div>
							</div>
							<!--[if !IE]>end table_wrapper<![endif]-->
							
						</div>
						
						<!--[if !IE]>start pagination<![endif]-->
							<div class="pagination_wrapper">
							<span class="pagination_top"></span>
							<div class="pagination_middle">
							<div class="pagination">
								<span class="page_no">Page 1 of 217</span>
								
								
 
								
								<ul class="pag_list">
									
									
									<?php echo $paginator->first();?>		
				
									<?php
									echo $paginator->prev(''.__('Previous', true), array(), null, array('class'=>'pag_nav'));?>
									
									<?php echo $paginator->numbers();?>
	                               <?php echo $paginator->next(__('Next', true).'', array(), null, array('class'=>'pag_nav'));?> 
                                   <?php echo $paginator->last();?>		
				
									
									</ul>
							</div>
							</div>
							<span class="pagination_bottom"></span>
							</div>
						<!--[if !IE]>end pagination<![endif]-->
						
						
						<span class="section_content_bottom"></span>
					</div>
					<!--[if !IE]>end section content<![endif]-->
				</div>
				<!--[if !IE]>end section<![endif]-->









