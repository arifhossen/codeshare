<!--[if !IE]>start section<![endif]-->
				<div class="section">
					
					<!--[if !IE]>start title wrapper<![endif]-->
					<div class="title_wrapper">
						<span class="title_wrapper_top"></span>
						<div class="title_wrapper_inner">
							<span class="title_wrapper_middle"></span>
							<div class="title_wrapper_content">
								<h2>Dashboard</h2>
								<ul class="section_menu right">
									<li><a href="#" class="selected_lk"><span class="l"><span></span></span><span class="m"><em>Module Lists</em><span></span></span><span class="r"><span></span></span></a></li>
											</ul>
							</div>
						</div>
						<span class="title_wrapper_bottom"></span>
					</div>
					<!--[if !IE]>end title wrapper<![endif]-->
					
					<!--[if !IE]>start section content<![endif]-->
					<div class="section_content">
						<span class="section_content_top"></span>
						
						<div class="section_content_inner">
							<!--[if !IE]>start dashboard menu<![endif]-->
								<div class="dashboard_menu_wrapper">
								<ul class="dashboard_menu">
									<li><a href="index" class="d1"><span>User Management Tools</span></a></li>
									<li><a href="#" class="d2"><span>Manage Pages / Entity</span></a></li>	

                                    <li><a href="<?php echo $this->webroot.'admin/settings/payment_gateway_integration'; ?>" class="d4"><span>Payment Gateway Integration</span></a></li>
							
									
									<li><a href="<?php echo $this->webroot.'admin/settings/manage_template'; ?>" class="d4"><span>Change Admin templates</span></a></li>
							
								</ul>
								
								<hr />
								<fieldset> 
								
									<legend> Manage Blog</legend>
									
									<ul class="dashboard_menu">
									
									
										<li>
									
									<?php
										echo   $this->Html->link('Manage Category', array('controller'=>'categories','action'=>'index'), array('escape' => false,'class'=>'d1', 'title' => 'Category'));   ?>										
								   </li>
								   
								   <li>
									
									<?php
										echo   $this->Html->link('Manage Tag', array('controller'=>'tags','action'=>'index'), array('escape' => false,'class'=>'d1', 'title' => 'Tag'));   ?>										
								   </li>
									
									<li>
									
									<?php
										echo   $this->Html->link('Manage Post', array('controller'=>'posts','action'=>'index'), array('escape' => false,'class'=>'d1', 'title' => 'Post'));   ?>										
								   </li>
								   
								   
								   	<li>
									
									<?php
										echo   $this->Html->link('Manage Comments', array('controller'=>'post_comments','action'=>'index'), array('escape' => false,'class'=>'d1', 'title' => 'Post'));   ?>										
								   </li>
									
										<li>
									
									<?php
										echo   $this->Html->link('Manage Content', array('controller'=>'contents','action'=>'index'), array('escape' => false,'class'=>'d1', 'title' => 'Content'));   ?>
										
										</li>
										
												<li>
									
									<?php
										echo   $this->Html->link('Manage Tools', array('controller'=>'tools','action'=>'index'), array('escape' => false,'class'=>'d1', 'title' => 'Tool'));   ?>
										
										</li>
									
									
									</ul>
									
									
								</fieldset>
								
								
								</div>
								<!--[if !IE]>end dashboard menu<![endif]-->	
						</div>
						
						<span class="section_content_bottom"></span>
					</div>
					<!--[if !IE]>end section content<![endif]-->
				</div>
				<!--[if !IE]>end section<![endif]-->
				