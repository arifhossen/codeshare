<div id="login-container">
<?php echo $this->Form->create('User', array('action' => 'login')); ?>
	
	<h4>LOGIN OR CREATE AN ACCOUNT</h4>
	<div class="login-content-left" style='width:300px;'>
		<h5 style="color:#005b9a;">NEW CUSTOMERS</h5>
		<p>
		<span>
		By creating an account with our store, you will be able to move through the checkout process faster, store multiple 
		shipping addresses, view and track your orders in your account and more.
		</span>
		</p>
		<div class="creat-an-account-btn">			
			   <a class="login-btn" href="<?php echo $this->webroot; ?>packages/">
			   <img src="<?php echo $this->webroot;?>img/create_an_account.jpg" alt='Logo' />
			  </a>
			  
			  
		</div>
	</div>
	<div class="login-content-right">
		<h5>REGISTERED CUSTOMERS</h5>
		<p><span>If you have an account with us, please log in.</span></p>
		<p><span>
		<?php If ($this->Session->check('Message.flash'))
		echo $this->Session->flash();
		?></span>
		</p>
		<span><b>User Name</b> <sub style="color:#FF0000;">*</sub></span><br />
		<?php echo $this->Form->input('username', array('div' => false, 'label' => false, 'class' => 'login-inpu-email')); ?><br />
		<span><b>Password</b> <sub style="color:#FF0000;">*</sub></span><br />
		<?php echo $this->Form->password('password', array('div' => false, 'label' => false, 'class' => 'login-inpu-email')); ?>
		
		
		<div class="element">
        <div class="title">&nbsp;</div>
        <div class="value">
            <input type="checkbox" name="data[User][remember_me]" value="1" <?php if (isset($checked))
        echo 'checked="checked"'; ?> /> <span class="label_text">Remember Me</span>
        </div>
    </div>
		
		<div class="forgot-password-box">
			<span class="forgot-password">
			  <a class="link" href="<?php echo $this->webroot; ?>users/forgot_password">Forgot password?</a>
			
			</span>			
			<?php echo $form->submit('login_btn.jpg', array('div'=>false,'class'=>'login-btn')); ?>
			<div class="clear"></div>
		</div>
	</div>
<div class="clear"></div>	
</div>
  


  