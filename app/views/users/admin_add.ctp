<div class="section">
					
					<div class="title_wrapper">
						<span class="title_wrapper_top"></span>
						<div class="title_wrapper_inner">
							<span class="title_wrapper_middle"></span>
							<div class="title_wrapper_content">
								<h2>Add New User:</h2>
								<ul class="section_menu left">
									<li><a href="#" class="selected_lk"><span class="l"><span></span></span><span class="m"><em>Add New User</em><span></span></span><span class="r"><span></span></span></a></li>
									<li><a href="index"><span class="l"><span></span></span><span class="m"><em>Users Lists</em><span></span></span><span class="r"><span></span></span></a></li>
										</ul>
							</div>
						</div>
						<span class="title_wrapper_bottom"></span>
					</div>
					
					<!--[if !IE]>start section content<![endif]-->
					<div class="section_content">
						<span class="section_content_top"></span>
						
						<div class="section_content_inner">
							
							
						
							
							<div class="section_content_inner">
						   <?php echo $this->Form->create("User",array('enctype'=>"multipart/form-data",'class'=>'search_form'));?>
						
							<fieldset>
								<!--[if !IE]>start forms<![endif]-->
								<div class="forms">
								<h3>Enter New User info</h3>
								<!--[if !IE]>start row<![endif]-->
								<div class="row">
									<label>First/Last name:</label>
									<div class="inputs">
										<span class="input_wrapper"><input class="text" name="" type="text" /></span>
										<span class="input_wrapper"><input class="text" name="" type="text" /></span>
									</div>
								</div>
								<!--[if !IE]>end row<![endif]-->
								<!--[if !IE]>start row<![endif]-->
								<div class="row">
									<label>City/State/Zip:</label>
									<div class="inputs">
										<span class="input_wrapper"><input class="text" name="" type="text" /></span>
										<span class="input_wrapper select_wrapper">
											<select>
												<option>Washington DC</option>
												<option>Washington DC</option>
												<option>Washington DC</option>
												<option>Washington DC</option>
												<option>Washington DC</option>
											</select>
										</span>
										<span class="input_wrapper short_input"><input class="text" name="" type="text" /></span>
									</div>
								</div>
								<!--[if !IE]>end row<![endif]-->
								
								
								<!--[if !IE]>start row<![endif]-->
								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>SEARCH DATABASE</em></span></span><input name="" type="submit" /></span>
										<!--<span class="button green_button"><span><span>SEARCH DATABASE</span></span><input name="" type="submit" /></span>
										<span class="button gray_button"><span><span>SEARCH DATABASE</span></span><input name="" type="submit" /></span> -->
									</div>
								</div>
								<!--[if !IE]>end row<![endif]-->
								
								</div>
								
								<!--[if !IE]>end forms<![endif]-->
								
								
								
								
								
								
								
							</fieldset>
							
							</form>
							
							</div>
							</div>
							
							
						</div>
						
						
						
						
						<span class="section_content_bottom"></span>
					</div>
					<!--[if !IE]>end section content<![endif]-->
				</div>
				<!--[if !IE]>end section<![endif]-->

           








