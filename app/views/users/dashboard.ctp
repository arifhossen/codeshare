<table width="800" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('users.png', array('alt'=>'users','height'=>'48', 'title'=>'System User')); ?>
			<h3>Manage System User</h3>
			<div class="link"><?php echo $this->Html->link('Manage User Information', array('controller'=>'users', 'action'=>'index'),array('escape' => false));?></div>
			<div class="link"><?php echo $this->Html->link('Change Password', array('controller'=>'users', 'action'=>'dashboard'),array('escape' => false));?></div>
		</div>
	</td>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('trainer.png', array('alt'=>'trainer','height'=>'48', 'title'=>'Trainer')); ?>
			<h3>Manage Trainer</h3>
			<div class="link"><?php echo $this->Html->link('Manage Trainer Information', array('controller'=>'trainers', 'action'=>'index'),array('escape' => false));?></div>
		</div>
	</td>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('trainee.png', array('alt'=>'trainee','height'=>'48', 'title'=>'Trainee')); ?>
			<h3>Manage Participant</h3>
				<div class="link"><?php echo $this->Html->link('Manage Participant Information', array('controller'=>'participants', 'action'=>'index'),array('escape' => false));?></div>
		</div>
	</td>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('notebook.png', array('alt'=>'notebook','height'=>'48', 'title'=>'Notebook')); ?>
			<h3>Manage Training Course</h3>
			<div class="link"><?php echo $this->Html->link('Manage Course Information', array('controller'=>'courses', 'action'=>'index'),array('escape' => false));?></div>
		</div>
   </td>
  </tr>
  <tr>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('home.png', array('alt'=>'institute','height'=>'48', 'title'=>'Manage Training Institute')); ?>
			<h3>Manage Training Institute</h3>
			<div class="link"><?php echo $this->Html->link('Manage Instituton Information', array('controller'=>'institutions', 'action'=>'index'),array('escape' => false));?></div>
		</div>
	</td>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('home.png', array('alt'=>'institute','height'=>'48', 'title'=>'Manage Training Branch')); ?>
			<h3>Manage Training Branch</h3>
			<div class="link"><?php echo $this->Html->link('Manage Branch Information', array('controller'=>'branches', 'action'=>'index'),array('escape' => false));?></div>
		</div>
	</td>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('home.png', array('alt'=>'institute','height'=>'48', 'title'=>'Manage Training Section')); ?>
			<h3>Manage Training Section</h3>
			<div class="link"><?php echo $this->Html->link('Manage Section Information', array('controller'=>'sections', 'action'=>'index'),array('escape' => false));?></div>
		</div>
	</td>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('home.png', array('alt'=>'institute','height'=>'48', 'title'=>'Manage Training Unit')); ?>
			<h3>Manage Training Unit</h3>
			<div class="link"><?php echo $this->Html->link('Manage Unit Information', array('controller'=>'units', 'action'=>'index'),array('escape' => false));?></div>
		</div>
	</td>
  </tr>
  <tr>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('home.png', array('alt'=>'institute','height'=>'48', 'title'=>'Manage Training Department')); ?>
			<h3>Manage Training Department</h3>
			<div class="link"><?php echo $this->Html->link('Manage Department Information', array('controller'=>'departments', 'action'=>'index'),array('escape' => false));?></div>
		</div>
	</td>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('folder.png', array('alt'=>'Vanue','height'=>'48', 'title'=>'Manage Training Vanue')); ?>
			<h3>Manage Training Venue</h3>
			<div class="link"><?php echo $this->Html->link('Manage Vanue Information', array('controller'=>'venues', 'action'=>'index'),array('escape' => false));?></div>
		</div>
	</td>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('designation.png', array('alt'=>'Vanue','height'=>'48', 'title'=>'Manage Training Vanue')); ?>
			<h3>Manage Designation</h3>
			<div class="link"><?php echo $this->Html->link('Manage Designation Information', array('controller'=>'designations', 'action'=>'index'),array('escape' => false));?></div>
		</div>
	</td>
	<td>
		<div class="box">
			<?php  echo $this->Html->image('rank.png', array('alt'=>'Vanue','height'=>'48', 'title'=>'Manage Rank')); ?>
			<h3>Manage Rank</h3>
			<div class="link"><?php echo $this->Html->link('Manage Rank Information', array('controller'=>'ranks', 'action'=>'index'),array('escape' => false));?></div>
		</div>
	</td>
  </tr>
  <tr>
  	<td>
		<div class="box">
			<?php  echo $this->Html->image('report.png', array('alt'=>'Report','height'=>'48', 'title'=>'Report')); ?>
			<h3>Reports</h3>
			<div class="link"><?php echo $this->Html->link('Trainer Report', array('controller'=>'reports', 'action'=>'index'),array('escape' => false));?></div>
			<div class="link"><?php echo $this->Html->link('Trained People Report', array('controller'=>'reports', 'action'=>'trained_report'),array('escape' => false));?></div>
			<div class="link"><?php echo $this->Html->link('Trainee People Report', array('controller'=>'reports', 'action'=>'trainee_report'),array('escape' => false));?></div>
			<div class="link"><?php echo $this->Html->link('NGO Report', array('controller'=>'reports', 'action'=>'ngo_report'),array('escape' => false));?></div>
			<div class="link"><?php echo $this->Html->link('Custom Report', array('controller'=>'reports', 'action'=>'custom_report'),array('escape' => false));?></div>
		</div>
	</td>
	<td>&nbsp;
		
	</td>
	<td>&nbsp;
		
	</td>
	<td>&nbsp;
		
	</td>
	<td>&nbsp;
		
	</td>
  </tr>
  <tr>
	<td>&nbsp;
		
	</td>
	<td>&nbsp;
		
	</td>
	<td>&nbsp;
		
	</td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
