<script type="text/javascript">
    $(function(){
        $('#left_content li').removeClass('selLeft');
        $('#admin_mngt_list').addClass('selLeft');
        $('.nav_admin').show();
        $('#adminManage').css('font-weight','bold');
    });
</script>
<div class="action_title"><span>Change Password</span></div>
<div class="tabitem">
    <div class="common_from">
    <?php echo $this->Form->create("User",array('action'=>'changepassword'));?>
	   
		<?php 
		$username =  $users['User']['username'];
		$userid =  $users['User']['id'];
		echo $form->input('id',array('type'=>'hidden','value'=>$userid,'label'=>false,'div'=>false));
		echo $form->input('username',array('type'=>'hidden','value'=>$username,'label'=>false,'div'=>false));
		?>
	   
	    <div class="element">
            <div class="title">Old password :</div>
            <div class="value">
            <?php echo $form->input('password',array('label'=>false,'div'=>false));?>
            </div>
        </div>

        <div class="element">
            <div class="title">New Password:</div>
            <div class="value">
          <?php echo $form->input('clear_password',array('type'=>'password','label'=>false,'div'=>false));?>	

            </div>
        </div>
       
    </div>
</div>
<div class="button-container">
  <button value="signin" name="btnAction" type="submit"><span>Submit</span></button>
    or <?php echo $this->Html->link(__('Back to list', true),array('action' => 'index')); ?>
</div>