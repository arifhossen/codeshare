


<div class="row" style='float:right;'>
	<div class="inputs">

	<span class="button blue_button"><span><span><em>
	
	<?php
		echo   $this->Html->link('Add New', array('action'=>'add'), array('escape' => false, 'title' => 'Add New Tool')); 
		
   ?>
									
	
	</em></span></span></span>
		
	</div>		
</div>	

<div style="clear:both;"> </div>							
							
<div class="table_wrapper">
								<div class="table_wrapper_inner">
								
								<table cellpadding="0" cellspacing="0" width="100%">
									<tbody><tr>
										<th style="width: 5px;">Serial.</th>
										<th style="width: 120px;"><?php echo $paginator->sort('Title', 'title');?></th>										
										<th style="width: 120px;"><?php echo $paginator->sort('Version', 'version');?></th>
										<th style="width: 120px;"><?php echo $paginator->sort('Tools', 'download_source');?></th>
										<th style="width: 120px;"><?php echo $paginator->sort('Status', 'status');?></th>
										<th style="width: 100px;">Actions</th>
										
										
										
									</tr>
									
									
								<?php
                                   if(isset($tools))
 {								   
									$serial = 1;
                                    foreach ($tools AS $data) 
									{

										$id = $data['Tool']['id'];
										$title = $data['Tool']['title'];
										$version = $data['Tool']['version'];
										$download_source = $data['Tool']['download_source'];
										
										if($serial %2 == 1)
										{
											$alter_row = 'first';
										}
										else
										{
										
											$alter_row = 'second';
										
										}
										
										/*
										if($status == 1)
										{
											$checked = 'checked=checked';
										}
										else
										{
										
										 $checked = '';
										}
										
										*/
										

									?>								
									
									<tr class="<?php echo $alter_row; ?>">
										<td style='width:10px;'><?php echo $serial; ?></td>
										
										<td>
										<?php
										echo   $this->Html->link($title, array('action'=>'edit', $id), array('escape' => false, 'title' => 'View'));   ?>
										</td>
										
									
										
										<td>
											<?php echo $version; ?>
										</td>
										
										
											<td>
										
										 <?php echo $download_source; ?>
										</td>
										
										<td><?php	
										
									
											if($data['Tool']['status'] == 1)
											{
											   $wbroot = $this->webroot.'images/admin_panel/approved.gif';
												echo "<img src=$wbroot>";
											
											}
											else
											{
												 $wbroot = $this->webroot.'images/admin_panel/not_approved.gif';
												echo "<img src=$wbroot>";
											
											}
											
										
											?>
										</td>	
										
									
										
										<td>
										
										<?php							
	echo   $this->Html->link($this->Html->image('admin/icon_edit.gif'), array('action'=>'edit', $id), array('escape' => false, 'title' => 'Edit'));
	
	echo "&nbsp;&nbsp;&nbsp;&nbsp;";
		echo $this->Html->link($this->Html->image('admin/icon_delete.gif'), array('action'=>'delete',$id), array('escape' => false, 'title' => 'Delete'), sprintf(__('Are you sure you want to delete # %s?', true),  $id));
		
?>
										
										</td>																			
										
									</tr>
									
									<?php
									$serial++;
									 
}
									 }
									?>
									
									
								</tbody></table>
								
<div style="float:right;" id="paginator_control">	
	<?php echo $paginator->prev(); ?> -
<?php echo $paginator->numbers(array('separator'=>' - ')); ?>
<?php echo $paginator->next('Next Page'); ?>
</div>
								</div>
							</div>