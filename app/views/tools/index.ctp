  
    <script type="text/javascript">  
        $(document).ready(function(){
            $("#report tr:odd").addClass("odd");
            $("#report tr:not(.odd)").hide();
            $("#report tr:first-child").show();
            
            $("#report tr.odd").click(function(){
                $(this).next("tr").toggle();
                $(this).find(".arrow").toggleClass("up");
            });
            //$("#report").jExpand();
        });
    </script>        

<h1>Development Tools </h1>
    <table id="report">
        <tr>
            <th>Name</th>
            <th>Version</th>
            <th>Details</th>
            <th>Download</th>
            <th></th>
        </tr>
		
	<?php
	
	if(isset($tools))
	{								   
		$serial = 1;
		
	
		foreach ($tools AS $data) 
		{
		
			$id = $data['Tool']['id'];
			$title = $data['Tool']['title'];
			$version = $data['Tool']['version'];
			$details = $data['Tool']['details'];
			$tool_category = $data['ToolCategory']['title'];
			$download_source = '';
		
		
		?>
        <tr>
            <td><?php echo $title; ?></td>
            <td><?php echo $version; ?></td>
            <td><?php echo $details; ?></td>
            <td>Save</td>
            <td><div class="arrow"></div></td>
        </tr>
		
        <tr>
            <td colspan="5">
              
                <h4>Additional information</h4>
                <ul>
                    <li><?php	echo   $this->Html->link('Download', array('action'=>'download',$id), array('escape' => false, 'title' => 'Download Software'));    ?></li>
                  
                    
                 </ul>   
            </td>
        </tr>
		
		<?php
		
				}
	
}	
		
		?>
	</table>	
		
    


  
 <?php 
 	if(isset($tools) && sizeof($tools)>5): ?>
		 
  <div style="float:right;" id="paginator_control">	
	<?php echo $paginator->prev('Prev'); ?> 
<?php echo $paginator->numbers(array('separator'=>' ')); ?>
<?php echo $paginator->next('Next'); ?>
</div>
  
    <?php endif; ?>



</div>


