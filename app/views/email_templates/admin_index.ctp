	<script>
	$(document).ready(function () 
	{
		$("#grid").wijgrid({
		allowSorting: true,
		allowPaging: true,
		pageSize:10,
		});

	});

	</script>

	<div style="padding-left:10px; width:750px;">
	<h1>Manage Email Templates </h1>
	<p align='right'> <a style='font-size:16px; color:green;' href="<?php echo $this->webroot; ?>admin/email_templates/add/">Add new</a> </p>
		<table id="grid" width="100%">
			<thead>
				<tr>
					<th width="1%">
					SL No.
					</th>
					<th width="70%">
					Identifier
					</th>
					
					<th width="70%">
					Subject
					</th>
					
					<th width="70%">
						Body
					</th>
					
					
					
					
					<th width="20%">
					Action 
					</th>
				</tr>
			</thead>

			<tbody>
				<?php 
				
				if(!empty($emailtemplates))
				{
			
				$serial_no = 1;
				foreach($emailtemplates as $emailtemplate)
				   { ?>
					<tr>
						<th>
							<?php echo $serial_no++ ; ?>
						</th>
						
						<td><?php  echo $emailtemplate['EmailTemplate']['identifier'];  ?></td>
						
						<td> <?php echo $emailtemplate['EmailTemplate']['subject']; ?></td>
						 
						 <td> <?php echo $emailtemplate['EmailTemplate']['body']; ?></td>      
						        
								<td>
								
								 
<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $emailtemplate['EmailTemplate']['id'])); ?> | 
<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $emailtemplate['EmailTemplate']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $emailtemplate['EmailTemplate']['id'])); ?>


            </td>
								
								
									
					</tr>

				<?php 
				  }//EO FOR EACH
				}//EO NOT EMPTY ?>
			</tbody>
		</table>
	</div>


















