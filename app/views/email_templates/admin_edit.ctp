<?php echo $javascript->link('fckeditor'); ?>
<h1>Edit Email Template </h1>
<div class="tabitem">
<div class="common_from">
<?php echo $this->Form->create('EmailTemplate'); echo $this->Form->input('id'); ?>

   <div class="element">
        <div class="title">Identifier:</div>
        <div class="value">
			<?php echo $form->input('identifier', array('label' => false, 'div' => false,'size'=>'50')); ?>            
        </div>
    </div>
	
	 <div class="element">
        <div class="title">Subject:</div>
        <div class="value">
			<?php echo $form->input('subject', array('label' => false, 'div' => false,'size'=>'50')); ?>          
        </div>
    </div>
	
	
	 <div class="element">
        <div class="title">From Title:</div>
        <div class="value">
			<?php echo $form->input('from_title', array('label' => false, 'div' => false,'size'=>'50')); ?>           
        </div>
    </div>
	
	
	 <div class="element">
        <div class="title">From Email:</div>
        <div class="value">
			<?php echo $form->input('from_email', array('label' => false, 'div' => false,'size'=>'50')); ?>          
        </div>
    </div>
	
	
	

	

    <div class="element">
        <div class="title">Body:</div>
        <div class="value">			
			 <?php echo $this->Form->input('body',array('type'=>'textarea','label'=>false,'col'=>'100'));
			 	   echo $fck->load('EmailTemplate/body');	
			  ?>          
        </div>
    </div>

    <div class="button-container">
        <button value="signin" name="btnAction" type="submit"><span>Submit</span></button>
        or         <?php echo $this->Html->link(__('Back to list', true), array('action' => 'index')); ?>

    </div>
</div>
</div>
