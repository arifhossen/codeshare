<script type="text/javascript">
    $(function(){
        $('#left_content li').removeClass('selLeft');
        $('#admin_mngt_list').addClass('selLeft');
        $('.nav_admin').show();
        $('#adminManage').css('font-weight','bold');
    });
</script>
<div class="action_title"><span>Manage Contact Us In formation</span></div>
<div id="action" style="text-align: right;">   <?php echo  $this->Html->link(__('Add new', true), array('action' => 'add')); ?></div>
 
<table cellpadding="0" cellspacing="1" class="listContainer">
    <?php
    $tableHeaders =  $html->tableHeaders(array(
    $paginator->sort('id'),
    $paginator->sort('name'),
	$paginator->sort('subject'),
	$paginator->sort('from'),
	$paginator->sort('created'),
    __('Actions', true),
    ));
    echo $tableHeaders;

    $rows = array();
    foreach ($contactuses AS $contactus) {
	$actions  =  $this->Html->link($this->Html->image('admin/icon_edit.gif'), array('action'=>'edit', $contactus['Contactus']['id']), array('escape' => false, 'title' => 'Edit'));
	$actions .= ' ' .$this->Html->link($this->Html->image('admin/icon_delete.gif'), array('action'=>'delete', $contactus['Contactus']['id']), array('escape' => false, 'title' => 'Delete'), sprintf(__('Are you sure you want to delete # %s?', true), $contactus['Contactus']['id']));
	

    $rows[] = array(
    $contactus['Contactus']['id'],
    $contactus['Contactus']['name'],
	$contactus['Contactus']['subject'],
	$contactus['Contactus']['emailaddress'],
	$contactus['Contactus']['created'],
    $actions,
    );
    }
    echo $html->tableCells($rows);
    ?>
</table>

<?php echo $this->element('common/pagination'); ?>

