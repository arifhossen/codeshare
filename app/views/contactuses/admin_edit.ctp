<script type="text/javascript">
	tinyMCE.init({
		theme : "advanced",
		mode : "textareas",
		convert_urls : false
	});
</script>
<div class="action_title"><span>Update Contact Information</span></div>
<div class="tabitem">
    <div class="common_from">
    <?php echo $this->Form->create("Contactus",array('enctype'=>"multipart/form-data")); echo $form->input('id');?>

        <div class="element">
            <div class="title">Name</div>
            <div class="value">
            <?php echo $form->input('name',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		 <div class="element">
            <div class="title">Email address</div>
            <div class="value">
            <?php echo $form->input('emailaddress',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		 <div class="element">
            <div class="title">Nature of business</div>
            <div class="value">
            <?php echo $form->input('natureofbusiness',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		 <div class="element">
            <div class="title">Telephone No</div>
            <div class="value">
            <?php echo $form->input('telephoneno',array('label'=>false,'div'=>false));?>
            </div>
        </div>
		
		 <div class="element">
            <div class="title">Subject</div>
            <div class="value">
            <?php echo $form->input('subject',array('label'=>false,'div'=>false));?>
            </div>
        </div>

        <div class="element">
            <div class="title">Details</div>
            <div class="value">
            <?php echo $form->input('details',array('label'=>false,'div'=>false,'type'=>'textarea'));?>

            </div>
        </div>
		
    </div>
</div>
<div class="button-container">
    <button value="signin" name="btnAction" type="submit"><span>Submit</span></button>
    or <?php echo $this->Html->link(__('Back to list', true),array('action' => 'index')); ?>
</div>
