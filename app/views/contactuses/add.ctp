<?php echo $this->Form->create('Contactus');?>
		<div class="ckeckout-banner">
			<h1>Contact Us</h1>
		</div>
		<div class="gift-img">
			<img src="<?php echo $this->webroot; ?>images/ribon.jpg" alt="#" />
		</div>
		<div class="contactus-form">
			<h1>PERSONAL INFORMATION:</h1>				
			<table>
			<tr> <td style="text-align:left;"> <span>Name</span></td></tr>
			<tr><td><?php echo $form->input('name',array('label'=>false,'div'=>false));?></td> </tr>
			<tr> <td style="text-align:left;"><span>Email Address</span></td></tr>
			<tr><td><?php echo $form->input('emailaddress',array('label'=>false,'div'=>false));?></td></tr>
			<tr> <td style="text-align:left;"><span>City</span></td></tr>
			<tr><td><?php echo $form->input('city',array('label'=>false,'div'=>false));?></td></tr>
			<tr> <td style="text-align:left;"><span>Country</span></td></tr>
			<tr><td><?php echo $form->input('country',array('label'=>false,'div'=>false));?></td></tr>
			<tr> <td style="text-align:left;"><span>Subject</span></td></tr>
			<tr><td style="text-align:left;"><?php echo $form->input('subject',array('label'=>false,'div'=>false));?></td></tr>
			<tr> <td style="text-align:left;"><span>Description</span></td></tr>
			<tr><td> <?php echo $form->input('details',array('type'=>'textarea','label'=>false,'div'=>false,'style'=>'height:120px;'));?></td></tr>
			</table>			
			<?php echo $form->end('submit.jpg'); ?>
		</div>
		
		<div class="rightup">
			<img src="<?php echo $this->webroot; ?>images/rightup-contactus.jpg" alt="" width="250"/>
		</div>
		
		<div class="clear"></div>		

<div class="contact-info">
				<h2>You can also contact with us to this address or numbers written below:</h2>
				<p>Address Office<br/>
				Momtaz Plaza(2nd Floor)<br/>
				House # 7, Road # 4, Dhanmondi R/A<br/>
				Dhaka-1205<br/>
				Tel: +880 2 9661171<br/>
				Fax: +880 2 8622371</p>
</div>

<div class="ckeckout-banner-footer">
		<img src="<?php echo $this->webroot;?>images/kechal2-contactus.jpg" alt="" width="695"  />
</div>