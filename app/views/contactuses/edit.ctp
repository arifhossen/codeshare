<div class="contactuses form">
<?php echo $this->Form->create('Contactus');?>
	<fieldset>
 		<legend><?php __('Edit Contactus'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('emailaddress');
		echo $this->Form->input('natureofbusiness');
		echo $this->Form->input('telephoneno');
		echo $this->Form->input('subject');
		echo $this->Form->input('details');
		echo $this->Form->input('delflg');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Contactus.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Contactus.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Contactuses', true), array('action' => 'index'));?></li>
	</ul>
</div>