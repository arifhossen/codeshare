<div>

  <?php
  

	if(isset($posts))
	{								   
	$serial = 1;
	

	foreach ($posts AS $data) 
	{
	
		$id = $data['Post']['id'];
		$title = $data['Post']['title'];
		$details = $data['Post']['details'];
		$tagged = $data['Category']['name'];
		
		if($serial %2 == 1)
		{
			$alter_row = 'first';
		}
		else
		{	
			$alter_row = 'second';		
		}
		
		echo "<div class='blog_title'>";
		
			
			echo $this->Html->link($title,array('controller'=>'posts','action'=>'details',$title));
			echo "<br><hr>";
		
		echo "</div>";
		
		echo "<div class='blog_details'>";
		
		
		    $find = array('[code]','[/code]','[codestep]','[/codestep]');				
		   $replacer = array("<div class='codehightlight'>",'</div>',"<div class='codestep'>",'</div>');
		   
		  
		   echo  $post_details = str_replace($find,$replacer, $details); 
		
			echo "<br>";
			
			if(!empty($data['PostTag']))
			{
				
				echo "Tags : ";
				foreach($data['PostTag'] as $key=>$tags)
				{
				   // pr($tags);
				    echo "<b>";
					echo $tags['Tag']['name'].', &nbsp;';
					echo "</b>";
				
				}
				
			}
			echo "<br><br>";
			echo "<div class='view_more'>";
				echo $this->Html->link('Leave Comments',array('controller'=>'posts','action'=>'details',$title)); 
			echo "</div>";
			
		echo "</div>";
		
		
		
	}
	
}		
  
  ?>
  
  
 <?php 
 	if(isset($posts) && sizeof($posts)>5): ?>
		 
  <div style="float:right;" id="paginator_control">	
	<?php echo $paginator->prev('Prev'); ?> 
<?php echo $paginator->numbers(array('separator'=>' ')); ?>
<?php echo $paginator->next('Next'); ?>
</div>
  
    <?php endif; ?>



</div>


