<div class="action_title"><span>Add New Module</span></div>
<div class="tabitem">
    <div class="common_from">
    <?php echo $this->Form->create("Sitemodule",array('enctype'=>"multipart/form-data"));?>
		 <div class="element">
            <div class="title">Module name</div>
            <div class="value">			
			<?php echo $form->input('modulename', array('label' => false)); ?>
            </div>
        </div>
		 <div class="element">
            <div class="title">Url name</div>
            <div class="value">
			<?php 	echo $this->Form->input('url',array('label'=>false));?>
            </div>
        </div>
		<div class="element">
            <div class="title">Image name</div>
            <div class="value">
			<?php 	echo $this->Form->input('image',array('label'=>false));?>
            </div>
        </div>
		<div class="element">
            <div class="title">IsActivee</div>
            <div class="value">
			<?php 	echo $this->Form->input('is_active',array('type'=>'checkbox','label'=>false));?>
            </div>
        </div>
    </div>
</div>
<div class="button-container">
    <button value="signin" name="btnAction" type="submit"><span>Submit</span></button>
    or <?php echo $this->Html->link(__('Back to list', true),array('action' => 'index')); ?>
</div>





			