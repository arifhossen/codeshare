<div id="action" style="text-align: right;">   <?php echo  $this->Html->link(__('Add new module', true), array('action' => 'add')); ?>
	</div>
	<div id="action" style="text-align: right;">   <?php echo  $this->Html->link(__('Module permission', true), array('action' => 'permission')); ?>
	</div>
	

<table cellpadding="0" cellspacing="1" class="listContainer">
    <?php
	$tableHeaders =  $html->tableHeaders(array(
    $paginator->sort('SLNo','Sitemodule.id'),
    $paginator->sort('Module Name','Sitemodule.modulename'),
	$paginator->sort('url','Sitemodule.url'),
	$paginator->sort('IsActive','Sitemodule.isactive'),	
    __('Actions', true),
    ));
    echo $tableHeaders;

		$rows = array();
		
		if(!empty($Sitemodules))
		{		
			foreach($Sitemodules as $sitemodules){			
				$actions  =  $this->Html->link($this->Html->image('admin/icon_edit.gif'), array('action'=>'edit', $sitemodules['Sitemodule']['id']), array('escape' => false, 'title' => 'Edit'));
			$actions .= '&nbsp;' .$this->Html->link($this->Html->image('admin/icon_delete.gif'), array('action'=>'delete', $sitemodules['Sitemodule']['id']), array('escape' => false, 'title' => 'Delete'), sprintf(__('Are you sure you want to delete # %s?', true), $sitemodules['Sitemodule']['id']));
			
				$rows[] = array(
				$sitemodules['Sitemodule']['id'],
				$sitemodules['Sitemodule']['modulename'],						
				$sitemodules['Sitemodule']['url'],	
				$sitemodules['Sitemodule']['is_active'],				
				$actions,
				);
		 	 }
		}
		else
		{
			echo "There is no no module available for admin panel.";
		}
		
    echo $html->tableCells($rows);
    ?>
</table>

<?php echo $this->element('common/pagination'); ?>



