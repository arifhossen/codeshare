<fieldset>
<legend>User module permission</legend>
						<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<th>SLNo</th>
							<th>Role</th>
							<th>Permission Modules</th>
														
							<th class="actions"><?php __('Actions');?></th>
						</tr>
						<?php
						$i = 0;
						foreach ($roleLists as $role):
							$class = null;
							if ($i++ % 2 == 0) {
								$class = ' class="altrow"';
							}
						?>
							<tr<?php echo $class;?> style="height:100px;">
								<td style="text-align:center;">
									<?php echo $i; ?>
								</td>
								<td>
									<?php echo $html->link(__($role['Role']['name'], true), array('controller'=> 'users', 'action'=>'admin_view', $role['Role']['id'])); ?>
								</td>								
								<td>
									<?php 									
									$module_id = $role['Role']['module_id'];
									$modulesExp = explode(",",$module_id );
									$mod_idsArray = array();
									
									foreach($modulesExp as $single) {	
										$mod_idsArray[$single] = $single;	
									}
									
									$i = 1;
									
	foreach($modules as $key => $value){
		$mod_id = $value['Sitemodule']['id'];
		//echo $mod_id = $mod_idsArray[$mod_id];
			$mod_name = $value['Sitemodule']['modulename'];	
			//echo $mod_name;
			if (array_key_exists($mod_id, $mod_idsArray)) {
				echo $i.' '.$mod_name;
				echo "<br>";
			}
			$i++;
	}									
	if($module_id == 0){
		echo "No module assigned";
	}								
									 ?>
								</td>
								<td align="center" class="actions">			
								</td>
							</tr>
							<?php endforeach; ?>
						</table>		
</fieldset>
<div class="manageModule">
<?php
	echo $form->create('Sitemodule',array('controller'=>'sitemodules','action'=>'admin_permission'));	
	echo $form->input('role_selected', array('options'=>$roleArrayList,'Selected'=>'Select'), array('label'=>'Select Role'));
	echo "<br><br><br>";
    echo "<br>";
	echo "Select Module for user permisison";
	echo "<br>";echo "<br>";
	foreach($modules as $key=>$value){
		$mod_id = $value['Sitemodule']['id'];
		$mod_name = $value['Sitemodule']['modulename'];
		echo $form->input('Module.'.$mod_name,array('type'=>'checkbox','value'=>$mod_id));	
		echo "<br />";
		echo "<br />";
	}
    echo $form->end('Submit');
?>
</div>